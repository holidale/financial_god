class LarkTaskEvent
  EVENT_TYPE_MAPPING = {
    description_changed: 1,
    collaborators_changed: 2,
    follower_changed: 3,
    due_changed: 4,
    completed: 5,
    canceled: 6,
    deleted: 7,
  }.freeze

  attr_accessor :params, :header, :event, :event_obj_type
  def initialize(_params)
    self.params = _params.with_indifferent_access
    self.header = self.params[:header]
    self.event = self.params[:event]
    self.event_obj_type = EVENT_TYPE_MAPPING.key(event[:obj_type])
  end

  def event_type
    @header[:event_type]
  end

  def task_id
    @event[:task_id]
  end

  def task
    @task ||= LarkTask.new.remote_data(task_id).dig("data", "task")
  end

  def feishu_task
    @feishu_task ||= FeishuTask.find_by(remote_id: task_id)
  end

  def workorder
    feishu_task&.workorder
  end

  def hook
    return if workorder.blank?
    return unless need_hook?

    RedisHelper.lpush("LarkEvent", { 
      event_for: "task",
      object_type: "ServiceSchedule", 
      object_id: feishu_task.workorder_id,
      event_type: event_obj_type,
      data: {
        due: task["due"],
        completed: task["complete_time"].to_i > 0,
      }
    })

    workorder.push_update_lark_task_msg(event_name)
  end

  def event_name
    {
      description_changed: "Due Changed",
      due_changed: "Due Changed",
      completed: "Completed",
    }.dig(event_obj_type)
  end

  def need_hook?
    (:completed == event_obj_type && !workorder.completed?) ||
      (%i[description_changed due_changed].include?(event_obj_type) && workorder_due_need_change?)
  end

  def workorder_due_need_change?
    due_info = task["due"].with_indifferent_access
    Time.zone = due_info[:timezone]
    due = Time.zone.at(due_info[:time].to_i).to_date
    workorder.due != due
  rescue => e
    #LarkMsg.enqueue :debug_helper, "LarkTaskEvent#workorder_due_need_change? error: #{e.message}, Time zone = #{due_info[:timezone]}"
    return false
  end
end