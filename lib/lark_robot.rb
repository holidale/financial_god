class LarkRobot
  Headers = {"Content-Type": "application/json"}

  attr_reader :receiver, :url, :name
  def initialize(receiver)
    @receiver = receiver
    @name = receiver.name
    @url = receiver.url
  end

  # {"Extra":null,"StatusCode":0,"StatusMessage":"success"}
  def hook_request(res)
    JSON.parse(res.body) rescue {}
  end

  def send_msg(msg)
    params = LarkMsg.assemble_msg(msg)
    params[:card] = params.delete(:content) if params[:msg_type] == 'interactive'
    res = hook_request Faraday.post(@url, params.to_json, Headers)
    FeishuMessage.hook_msg(
      "LarkRobot", receiver, params[:content] || params[:card]) if res["msg"] == "success" rescue nil
    res
  end

  def notify(msg)
    send("notify_#{name}", msg)
  end
end
