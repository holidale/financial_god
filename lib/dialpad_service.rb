module DialpadService
  extend self
  BASE_URL = 'https://dialpad.com/api/v2'

  def api_key
    return PublicConfigs.dialpad_api_key
  end

  def request_headers
    {
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{api_key}"
    }
  end


  def stats
    url = "#{BASE_URL}/stats"
    payload = {
      export_type: 'records',
      stat_type: 'texts',
      target_id: PublicConfigs.dialpad_target_id,
      target_type: PublicConfigs.dialpad_target_type,
      days_ago_start: 0,
      days_ago_end: 1,
      timezone: "UTC"
    }

    res = Faraday.post(url, payload.to_json, request_headers)
    return JSON.parse(res.body) rescue {}
  end

  def get_stats_result(id)
    url = "#{BASE_URL}/stats/#{id}"
    res = Faraday.get(url, nil, request_headers)
    return JSON.parse(res.body) rescue {}
  end

end