module ByteBrain
  extend self

  def response(text)
    Sidekiq.logger.info("ByteBrain response Text: #{text}")
    !text.present? and return
    text = text.to_s.strip
    is_hi?(text) and return help_content
    # Manually run inquiry check with the pattern !!!!
    if text.start_with?("!!!!")
      res = inquiry_handler(text)
    elsif text.start_with?("????")
      res = gpt_handler(text)
    elsif is_inquiry?(text)
        # This method might return in Array of string
      res = inquiry_handler(text)
    else
      # This method returns string
      # Normal GPT request
      res = gpt_handler(text)
    end
    res
  end

  def is_hi?(text)
    _t = text.to_s.downcase
    _t == "hi" || _t == "hello"
  end

  def is_m2m?(text)
    _t = text.to_s.downcase
    contain_short_words?(text, "m2m") || contain_short_words?(text, "month2month")
  end

  def is_yes?(text)
    contain_short_words?(text, "y") || contain_short_words?(text, "yes")
  end

  def contain_short_words?(text, w)
    arr = text.to_s.downcase.gsub(".", "").gsub("!", "").split(" ")
    text == w || (arr.size <= 5 && arr.include?(w))
  end

  def help_content
    "Hi, my name is Mia (Stands for Month2Month Intelligent Assistant). I am Month2Month AI assistant, part of Byte Brain project. I am designed to assist month2month employees to work more efficiently.

Here are something I can do for you:
1. Assist in responding external inquiries

Here are something I will be able to do in the future:
1. Answer any question in Wiki
2. Assist in responding customer service questions
3. Analyze company data

Simply just text me anything, and I will try my best to answer you."
  end

  def openai
    @openai ||= Openai.new
  end

  def is_inquiry?(text)
    append = "We are furnished housing service provider. I will give you a text. Tell me is this text customer rental inquiry? Customer might ask the availability or price about an address(may only include street name or only city name), sometimes they will provide desire move-in date. Only reply \"yes\" or \"no\". No comment, no explanation.\nText:\n"
    prompt = append + text
    res = openai.chat(prompt)
    is_yes?(res) rescue false
  end

  def gpt_handler(text)
    res = openai.chat(text)
  end

  # Based on the prompt, what we expect is GPT can extract the address and desired move-in move-out date
  # If the year is missing, auto append 2023. If anything is missing, output null
  # Separate the result with "$$$" and separate fields with "|"
  # In the following format:
  # $$$123 Lake Front|2023/07/01|2023/08/31$$$
  # $$$630 W Woodcrest|null|null$$$
  def inquiry_handler(text)
    append = "We are furnished housing service provider. I will give you a customer inquiry text message, it includes the address they are interested (may only include street name or only city name), and may include the desired date as well. Respond with the address, check-in date, check-out date from the text. Format the dates as yyyy/mm/dd. If there is no date, return null. If a date contains month, date but is missing the year, it is the year 2023. If any of the content can not be extracted, return null. Separate address, check-in date, check-out date with the \"|\" character. If extracted multiple answer, put them into different lines. Start the line with \"$$$\" and put \"$$$\" at the end of the line (answers in between $$$ signs). Do not explain, do not comment, do not add empty lines. Respond with the single line of this format. For example:\n
$$$123 Lake Front|2023/07/01|2023/08/31$$$\n$$$630 W Woodcrest|null|null$$$\nText:\n"
    prompt = append + text
    extracted_info = openai.chat(prompt)
    # If result is not expected, return the gpt result
    !(extracted_info.start_with?("$$$") && extracted_info.end_with?("$$$")) and return extracted_info
    # Separate lines based on the $$$ mark
    address_arr = separate_inquiry_response(extracted_info)
    !address_arr.present? and return "Cannot extract address from AI response: #{extracted_info}"
    response = []
    address_arr.each do |line|
      ans = single_inquiry_handler(line)
      response << ans
    end
    response
  end

  def separate_inquiry_response(extracted_info)
    start_marker = '\$\$\$'
    end_marker = '\$\$\$'
    extracted_info.scan(/#{start_marker}\s?(.*?)\s?#{end_marker}/m).flatten
  end

  def single_inquiry_handler(address_line)
    separator = "|"
    arr = address_line.split(separator) rescue []
    # Cannot parse
    !(arr.present? && arr.size == 3) and return ""
    # Cannot extract address from the string
    arr[0] == "null" and return ""
    check_in = arr[1].to_date rescue nil
    check_out = nil
    if check_in.present?
      check_out = arr[2].to_date rescue nil
    end
    params = {
      "address": arr[0],
      "check_in": check_in&.to_s,
      "check_out": check_out&.to_s
    }
    res =  PmsApi.request("/ai_bot_search", params)
    ans = res["content"] rescue nil
  end

end
