module CommandCaller
  extend self

  BASIC_RUBY = if Rails.env.development?
                 "ruby /Users/heli/m2m/"
               else
                 "/home/webuser/.rvm/rubies/ruby-3.0.0/bin/ruby /home/webuser/zmsun/ruby/"
               end
  BASIC_PYTHON = "/usr/bin/python3 /home/webuser/zmsun/python/"

  def call_ruby(script, *args)
    ps = "'" + args.join("' '") + "'"
    command = "#{BASIC_RUBY}/#{script} #{ps}"
    puts command
    res = `#{command}`
    JSON.parse(res)
  rescue => e
    "CommandCaller Error ----> #{e.message}"
    nil
  end

  def call_python(script, *args)
    ps = "'" + args.join("' '") + "'"
    command = "#{BASIC_PYTHON}/#{script} #{ps}"
    puts command
    `#{command}`
  rescue => e
    "CommandCaller Error ----> #{e.message}"
    nil
  end

  def copy_files(source_dir, destination_dir)
    `scp -P 20202 -r webuser@192.81.135.161:/opt/webapp/holidale/current#{source_dir} #{destination_dir}/`
  end

  def get_issue(key)
    call_ruby("jira.rb", "get_issue", key)
  end

  def get_comment(url)
    call_ruby("jira.rb", "get_comment", url)
  end
end
