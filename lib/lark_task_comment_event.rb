class LarkTaskCommentEvent
  EVENT_TYPE_MAPPING = {
    created: 1,
    replied: 2,
    updated: 3,
    deleted: 4,
  }.freeze

  attr_accessor :m2m, :params, :header, :event, :event_obj_type
  def initialize(_params)
    self.m2m = LarkApp.inner_tool
    self.params = _params.with_indifferent_access
    self.header = self.params[:header]
    self.event = self.params[:event]
    self.event_obj_type = EVENT_TYPE_MAPPING.key(event[:obj_type])
  end

  def hook
    return unless need_hook?
    return if feishu_task.blank?

    pms_note.update(content: note_content)
    feishu_task_comment.update(commentable: pms_note)
    workorder.push_update_note_lark_task_msg(pms_note, event_obj_type)
  end

  def need_hook?
    # stop hook that lark task comment synced from pms
    return false if :created == event_obj_type.to_sym && pms_note.persisted?

    %i[created updated].include?(event_obj_type.to_sym)
  end

  def pms_note
    @pms_note ||= feishu_task_comment.commentable ||
      Note.new(notable: feishu_task.workorder, creator: note_creator)
    @pms_note.notable.touch
    @pms_note
  end

  def note_content
    content = comment[:content]
    return content if pms_creator.present?

    "[#{creator_name}] #{content}"
  end

  def comment
    @comment ||= lark_task_comment.remote_data.dig("data", "comment").with_indifferent_access
  end

  def note_creator
    pms_creator || feishu_task&.workorder&.assignee
  end

  def creator_name
    creator&.name || creator_remote_data[:name]
  end

  def creator_remote_data
    @creator_remote_data ||= m2m.get_user_info(comment[:creator_id]).with_indifferent_access
  end

  def pms_creator
    creator&.pms_user
  end

  def creator
    @creator ||= FeishuObject.find_user_with(comment[:creator_id])
  end

  def lark_task_comment
    @lark_task_comment ||= LarkTaskComment.new(task_id: task_id, comment_id: comment_id)
  end

  def feishu_task
    @feishu_task ||= FeishuTask.find_by(remote_id: task_id)
  end

  def workorder
    feishu_task&.workorder
  end

  def feishu_task_comment
    @feishu_task_comment ||= FeishuTaskComment.find_or_initialize_by(lark_task_id: task_id, lark_comment_id: comment_id)
  end

  def event_type
    header[:event_type]
  end

  def task_id
    event[:task_id]
  end

  def comment_id
    event[:comment_id]
  end
end
