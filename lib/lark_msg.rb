class LarkMsg
  attr_reader :content, :msg_type
  def initialize(content, msg_type)
    @content = content
    @msg_type = msg_type || 'interactive'
  end

  class << self

    def need_like?(msg)
      !msg.present? and return
      msg.is_a?(String) && msg.to_s.downcase =~ /^we released|deal time\:|extension time\:|^woohoo|^new signed/  # && (rand(3) == 1)
    end

    def need_image?(msg)
      !msg.present? and return
      msg.is_a?(String) && msg.to_s.downcase =~ /^woohoo/
    end

    def assemble_post_msg(msg)
      msg = %i[zh_cn en_us].each_with_object({}) { |language, r| r[language] = msg }
      msg[:msg_type] = "post"
      msg
    end

    def assemble_msg(msg)
      msg = {header: false, elements: [msg]} if need_image?(msg)
      if msg.is_a?(Hash) 
        msg = msg.with_indifferent_access
        msg_type = msg.delete(:msg_type)
        %w[sticker post].include?(msg_type.to_s) and return {content: msg.to_json, msg_type: msg_type}
        return {content: card_msg(msg).to_json, msg_type: msg_type || 'interactive'}
      end
      {content: {text: msg}.to_json, msg_type: 'text'}
    end

    def card_msg(data)
      elements = data[:elements]&.compact&.map do |a|
        if a.is_a?(String)
          a == "divider" ? divider : monit_string(a)
        else
          a[:tag] ? a : {tag: "div", fields: fill_fields(a[:fields])}
        end
      end || [data.to_json]
      _header = {header: {title: {tag: "plain_text", content: data[:header]}, template: data[:template] || 'Orange'}}
      { 
        config: {wide_screen_mode: true}, 
        elements: elements
      }.merge(data[:header] ? _header : {})
    end

    def monit_string(a)
      a.to_s.downcase =~ /woohoo/ and return with_image_line(a)
      {tag: "div", text: {tag: "lark_md", content: a}}
    end

    def fill_fields(fields)
      fields&.map do |k, v|
        {
          is_short: true,
          text: {
            tag: "lark_md",
            content: "**#{k.to_s.titleize}: ** #{v}"
          }
        }
      end
    end

    def divider
      {tag: "hr"}
    end

    def link_to_pms(object, text = nil)
      object.nil? and return text || object || ""
      text ||= object.id
      case object.class.name
      when 'Booking'
        "[#{text}](#{PmsRecord::SITE}/bookings/#{object.id}/show_and_edit)"
      when 'Invoice'
        link_to_pms(object.booking, text)
      when 'House'
        "[#{text}](https://month2month.com/houses/#{object.id})"
      when 'Cost'
        "[#{text}](#{PmsRecord::SITE}/costs/#{object.id}/edit_cost)"
      when 'CostItem'
        link_to_pms(object.cost, text)
      when 'ServiceSchedule'
        "[#{text}](#{PmsRecord::SITE}/service_schedules/#{object.id})"
      end
    end

    def enqueue(type, body)
      RedisHelper.lpush("FeishuMsg", { from: "financial_god", type: type, body: body })
      body.is_a?(Hash) ? body[:elements] : body
    end

    # DEFAULT_IMG_KEY = "img_v2_14f0ab9f-6365-4861-b5e0-80d88516b80g"
    # DEFAULT_IMG_KEY = "img_v2_12790309-1a5a-4faa-894e-2d98abf506ag"
    DEFAULT_IMG_KEY = "img_v2_9c11ef6e-f087-4ae2-8ce5-413bd5e544fg"
    def with_image_line(words, img_key = DEFAULT_IMG_KEY)
      {
        tag: "div",
        text: { tag: "lark_md", content: words },
        extra: { tag: "img", img_key: img_key, alt: {tag: "plain_text", content: "Image"} }
      }
    end

    def tiny_image(words, img_key)
      {
        tag: "note",
        elements: [
          { tag: "img", img_key: img_key, alt: { tag: "plain_text", content: "Image"} },
          { tag: "plain_text", content: words }
        ]
      }
    end

    def h_msg(key, h)
      enqueue(key, h.simple_info.to_a.map{|aa| ["**#{aa[0]}**", aa[1]].join(": ")}) rescue nil
    end
  end
end
