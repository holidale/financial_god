class LarkChatMemberEvent
  attr_accessor :m2m, :params, :header, :event, :event_type

  def initialize(_params)
    self.m2m = LarkApp.inner_tool
    self.params = _params.with_indifferent_access
    self.header = self.params[:header]
    self.event = self.params[:event]
    self.event_type = self.header[:event_type]
  end

  def hook
    hook_add_member if add_member?
  end

  def add_member?
    "im.chat.member.user.added_v1" == event_type
  end

  def hook_add_member
    ids = added_member_open_ids
    return if ids.blank?

    local_chat.feishu_tasks.where("feishu_tasks.created_at > ?", 1.month.ago).find_each do |lark_task|
      LarkTask.new(task_id: lark_task.remote_id).add_followers(ids)
    end
  end

  def added_member_open_ids
    Array(params.dig(:event, :users)).map { |u| u.dig(:user_id, :open_id) }.compact
  end

  def chat
    @chat ||= LarkChat.new(chat_id: event[:chat_id])
  end

  def chat_info
    @chat_info ||= chat.remote_data.with_indifferent_access
  end

  def local_chat
    @local_chat ||= FeishuObject.find_user_with(chat_info[:name])
  end
end
