# frozen_string_literal: true

module M2mTool
  extend self

  def bool(value)
    ActiveModel::Type::Boolean.new.cast(value)
  end

  def parse_query_str(query_str)
    Rack::Utils.parse_query(query_str).with_indifferent_access
  end

  def round_of_ten(number)
    (number / 10.0).round * 10
  end

  def days_for_month(date)
    Date.new(date.year, date.month, -1).day
  end

  def generate_month_ranges(sdate, edate)
    return [[sdate, edate]] if sdate.strftime("%Y%m") == edate.strftime("%Y%m") # in the same month

    (sdate..edate).each_with_object({}) do |date, h|
      next if h.key?(date.strftime("%Y%m"))

      if date == sdate
        h[date.strftime("%Y%m")] = [sdate, sdate.end_of_month]
        next
      end

      if date.strftime("%Y%m") != edate.strftime("%Y%m")
        h[date.strftime("%Y%m")] = [date.beginning_of_month, date.end_of_month]
        next
      end

      if date == edate
        h[date.strftime("%Y%m")] = [edate.beginning_of_month, edate]
        next
      end
    end.values
  end

  def days_between(start_date, end_date, last: false)
    days = (end_date - start_date).to_i
    days += 1 unless last
    days
  end
end