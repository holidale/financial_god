class LarkChat

  ENDPONT = "https://open.feishu.cn/open-apis/im/v1/chats/"

  attr_accessor :m2m, :params, :chat_id, :event

  def initialize(**params)
    @m2m = LarkApp.inner_tool
    @params = params.with_indifferent_access
    @chat_id = self.params[:chat_id] || self.params.dig(:event, :chat_id)
    @event = self.params[:event]
  end

  def remote_data(_chat_id = chat_id)
    m2m.get_remote("#{LarkChat::ENDPONT}/#{_chat_id}").dig("data")
  end

  def all_members(member_id_type: "open_id")
    chat_id = params[:chat_id]
    items, page_token = [], nil

    loop do
      res = members(page_size: 100, page_token: page_token)
      if res["code"] == 0
        data = res["data"]
        items += Array(data["items"])
        page_token = data["page_token"]
        break(items) unless data["has_more"]
      end
    end
  end

  def members(member_id_type: "open_id", page_token: nil, page_size: 10)
    chat_id = params[:chat_id]

    query = {
      member_id_type: member_id_type,
      page_token: page_token,
      page_size: page_size,
    }.compact.to_query

    m2m.get_remote("#{ENDPONT}/#{chat_id}/members?#{query}")
  end

  def hook
    was_name = @event.dig(:before_change, :name)
    cur_name = @event.dig(:after_change, :name)

    if was_name && cur_name
      wo_group = FeishuObject.find_user_with(was_name)
      return if wo_group.blank?

      wo_group.alias_names = (wo_group.alias_names.to_s.split(",") << cur_name).join(",")
      wo_group.save
    end
  end

end