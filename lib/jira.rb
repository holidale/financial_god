module Jira
  extend self

  def get_tasks(sprint)
  end

  def link_task(task)
    task = JiraTask.find_by(key: task) if task.is_a?(String)
    tid = task.jira_id and return "[#{task.key} #{task.title}](https://applink.feishu.cn/client/mini_program/open?appId=cli_9c394c5cc1ff910b&mode=sidebar-semi&path=pages/issue/detail?id=#{tid})"
    link(task.key, task.title)
  end

  def link_issue(issue)
    issue = issue.with_indifferent_access
    link(issue[:key], issue.dig(:fields, :summary) || issue[:title])
  end

  def link(key, title, n = 80)
    "[#{key} #{title.truncate(n)}](https://holidale.atlassian.net/browse/#{key})"
  end

  def close_issue(key)
    CommandCaller.call_ruby("jira.rb", "close_issue", key)
    if jt = JiraTask.find_by(key: key)
      jt.update(online_at: Time.current) rescue nil
      if (jt.extra.dig("fields", "subtasks") || []).size > 0
        LarkMsg.enqueue(:sub_issues_to_deploy, "subtasks in #{key} Do Not forget to Deploy")
      end
    end
  end
end
