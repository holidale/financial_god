class LarkTask

  ENDPONT = "https://open.feishu.cn/open-apis/task/v1/"

  attr_accessor :m2m, :params, :task_id, :payload
  def initialize(**params)
    @m2m = LarkApp.inner_tool
    @params = params.with_indifferent_access
    @task_id = self.params[:task_id]
    @payload = self.params[:payload]
  end

  def remote_data(_task_id = task_id)
    m2m.get_remote("#{ENDPONT}/tasks/#{_task_id}")
  end

  def create
    m2m.post_remote("#{ENDPONT}/tasks?user_id_type=open_id", payload)
  end

  def update(_payload = payload)
    m2m.patch_remote("#{ENDPONT}/tasks/#{task_id}?user_id_type=open_id", _payload)
  end

  def complete
    m2m.post_remote("#{ENDPONT}/tasks/#{task_id}/complete")
  end

  def uncomplete
    m2m.post_remote("#{ENDPONT}/tasks/#{task_id}/uncomplete")
  end

  def add_followers(ids, user_id_type: "open_id")
    m2m.post_remote("#{ENDPONT}/tasks/#{task_id}/followers?user_id_type=#{user_id_type}", { id_list: ids })
  end
end
