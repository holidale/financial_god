class AiApp
  AppId = "cli_a40d551c97fa900d"
  AppSecret = "VI4t3XRD7x0wd1urq5fehhaHV3mjI6aK"
  REMOTE_FOLDER_TOKEN = "nodcnyZ5T4FIV3N0K9Y2kJqYsZg"

  Headers = {"Content-Type": "application/json; charset=utf-8"}
  FileDir = "#{Rails.root}/data/ai/"

  FeishuEndpoint = "https://open.feishu.cn/open-apis"
  InternalAuthUrl = FeishuEndpoint + "/auth/v3/tenant_access_token/internal"
  UserAuthUrl = FeishuEndpoint + "/authen/v1/access_token"
  UserInfoUrl = FeishuEndpoint + "/contact/v3/users"
  ChatInfoUrl = FeishuEndpoint + "/im/v1/chats"
  BasicMessageUrl = FeishuEndpoint + "/im/v1/messages"
  UserAlertMsgUrl = FeishuEndpoint + "/im/v1/messages"
  FileUrlInChat = FeishuEndpoint + "/im/v1/messages"
  UploadRemoteUrl = FeishuEndpoint + "/drive/v1/files/upload_all"
  UploadApprovalUrl = "https://www.feishu.cn/approval/openapi/v2/file/upload"

  attr_reader :app_id, :app_secret
  def initialize(app_id, app_secret)
    @app_id = app_id
    @app_secret = app_secret
  end

  def hook_request(res)
    body = res.body
    Sidekiq.logger.info(body)
    JSON.parse(body)&.with_indifferent_access rescue {}
  end

  def tenant_access_token
    _t = RedisHelper.get("#{app_id}:tenant_access_token").presence and return _t

    params = {app_id: app_id, app_secret: app_secret}
    res = hook_request(Faraday.post(InternalAuthUrl, params.to_json, Headers))
    _t = res["tenant_access_token"]
    RedisHelper.cache("#{app_id}:tenant_access_token", _t, res["expire"].to_i)
    _t
  end

  def admin_user_token
    get_user_access_token(FeishuObject.admin&.open_id)
  end

  def get_user_access_token(open_id)
    get_cached_user_info(open_id)['access_token'] || refresh_user_token(open_id)
  end

  def logined_user?(open_id)
    get_user_access_token(open_id).present?
  end


  def refresh_user_token(open_id)
    if _fresh_token = RedisHelper.get(open_id + "_refresh_token").presence
      user_info = post_remote("https://open.feishu.cn/open-apis/authen/v1/refresh_access_token", {
        grant_type: "refresh_token",
        refresh_token: _fresh_token,
      })["data"]
      cache_user_info(user_info)
      user_info[:access_token]
    end
  end

  def request_feishu_user_info(code)
    params = {grant_type: "authorization_code", code: code}
    res = hook_request(Faraday.post(UserAuthUrl, params.to_json, headers_with_token)).presence
    user_info = res[:data]
    cache_user_info(user_info)
  end

  def cache_user_info(user_info)
    ukey = user_info[:open_id]
    find_user(ukey).assign_on_extra(user_auth_info: user_info)
    RedisHelper.cache(ukey + "_refresh_token", user_info[:refresh_token], user_info[:refresh_expires_in].to_i)
    RedisHelper.cache(token_for_user(ukey), user_info, user_info[:expires_in].to_i)
    user_info
  end

  def find_user(open_id)
    FeishuObject.find_user_with(open_id)
  end

  def token_for_user(open_id)
    "#{app_id}:user_access_token:#{open_id}"
  end

  def get_cached_user_info(open_id)
    JSON.parse(RedisHelper.get(token_for_user(open_id)).presence || "{}")&.with_indifferent_access
  end

  def download_file_from_chat(file_msg)
    url = [FileUrlInChat, file_msg[:message_id], "resources", file_msg[:file_key]].join("/")
    ps = {type: file_msg[:type]}
    _fname = FileDir + file_msg[:file_key] + "--#{file_msg[:file_name] || 'tmp'}"
    File.open(_fname, "w") do |f|
      f.puts Faraday.get(url, ps, headers_with_token).body.force_encoding('utf-8')
    end
    _fname # file_msg[:file_key]
  end

  def upload_to_remote(file_msg, folder_token = "", request_type: "app")
    file_path = FileDir + file_msg[:file_key] + "--" + file_msg[:file_name]
    if !File.exist?(file_path)
      return Sidekiq.logger.info("Fail to Get file: #{file_path}")
    end
    command = %Q(curl --location --request POST '#{UploadRemoteUrl}'
      --header 'Authorization: Bearer #{request_type == 'app' ? tenant_access_token : admin_user_token}'
      --header 'Content-Type: multipart/form-data'
      --form 'file_name="#{file_msg[:file_name]}"'
      --form 'parent_type="explorer"'
      --form 'parent_node="#{folder_token}"'
      --form 'size="#{File.read(file_path).bytesize}"'
      --form 'file=@"#{file_path}"')
    puts command
    res = `#{command.gsub("\n"," ")}`
    JSON.parse(res) rescue {error: "upload file failed"}
  end

  def auth_header(type)
    at = type == "app" ? tenant_access_token : admin_user_token
    {"Authorization": "Bearer #{at}"}
  end

  def headers_with_token(type = "app")
    Headers.merge(auth_header(type))
  end

  def get_remote(url, ps = {}, request_type: "app")
    res = Faraday.get(url, ps, headers_with_token(request_type))
    hook_request(res)
  end

  def post_remote(url, ps = {}, request_type: "app")
    res = Faraday.post(url, ps.to_json, headers_with_token(request_type))
    hook_request(res)
  end

  def patch_remote(url, ps = {}, request_type: "app")
    res = Faraday.patch(url, ps.to_json, headers_with_token(request_type))
    hook_request(res)
  end

  def auth_url(redirect_uri = nil, state = 'state')
    redirect_uri ||= FINANCE_ENDPOINT + "/feishu_auth"
    redirect_uri = CGI.escape(redirect_uri)
    "#{FeishuEndpoint}/authen/v1/index?redirect_uri=#{redirect_uri}&app_id=#{app_id}&state=#{state}"
  end

  def get_user_info(openid)
    res = get_remote UserInfoUrl + "/#{openid}"
    res["data"]["user"] rescue {}
  end

  def get_chat_info(chat_id)
    res = get_remote ChatInfoUrl + "/#{chat_id}"
    res["data"] rescue {}
  end

  def send_single_msg_to_user(open_id, msg)
    send_single_msg_to("open_id", open_id, msg)
  end

  def send_single_msg_to_chat(chat_id, msg)
    send_single_msg_to("chat_id", chat_id, msg)
  end

  def send_single_msg_to(type, type_id, msg)
    params = {receive_id: type_id}.merge(LarkMsg.assemble_msg(msg))
    Sidekiq.logger.info(params)
    res = Faraday.post("#{BasicMessageUrl}?receive_id_type=#{type}", params.to_json, headers_with_token)
    hook_request(res)
  end

  def send_emoji(msg_id, emoji, request_type: "app")
    params = {reaction_type: {emoji_type: emoji.to_s.upcase}}
    Faraday.post("#{BasicMessageUrl}/#{msg_id}/reactions", params.to_json, headers_with_token(request_type))
  end

  def self.ai_tool
    @ai_tool ||= new(AppId, AppSecret)
  end

  def self.at_user(user)
    user == 'all' and return "<at user_id=\"all\">All</at>"
    if obj = FeishuObject.find_user_with(user)
      return "<at user_id=\"#{obj.open_id}\">#{obj.name}</at>"
    end
  end

  def monit_file_msg(file_msg, remote_folder_token = REMOTE_FOLDER_TOKEN)
    _fname = download_file_from_chat(file_msg)
    # res = upload_to_remote(file_msg, remote_folder_token, request_type: "user")
    return _fname
  end
end