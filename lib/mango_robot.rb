module MangoRobot
  extend self
  OBJECT_MAPPINGS = {
    "qn" => "quotation_new",
    "br" => "booking_revenue",
    "ci" => "cost_item",
    "cl" => "cleaner",
    "ct" => "cleaner",
    "qr" => "qb_sync_record",
    "ca" => "contract_amendment",
    "uf" => "utility_fee",
    "wo" => "service_schedule",
    "ar" => "approval",
    "po" => "cost",
    "h" => "house",
    "b" => "booking",
    "i" => "invoice",
    "u" => "user",
    "q" => "quotation",
    "c" => "cost",
    "w" => "service_schedule",
  }
  BASIC_KEYWORDS = OBJECT_MAPPINGS.values + OBJECT_MAPPINGS.keys
  PURE_COMMANDS = %w(
    tasks
    checkin_list 
    checkout_list 
    approval_summary 
    toggle_rca_period
    workorder_summary 
    major_issues
    guesties
    clusters
  )
  COMMAND_LIST = %w(
    fq
    fast_quote
    fs
    fast_search
    qb_get
    recent
    gpt
    dalle
    ahwo
    addwo
    ahi
    rcache
    rsadd
    rsrem
    rsmembers
    v
    vendor
    p
    plan
  ) + PURE_COMMANDS + BASIC_KEYWORDS

  def customized_command?(text)
    command, command_body = text.to_s.split("#", 2)
    pure_command?(text) ||
      (command_body.present? && !BASIC_KEYWORDS.include?(command.to_s.downcase)) # none internal commands
  end

  def pure_command?(text)
    PURE_COMMANDS.include?(text.to_s.downcase)
  end

  def is_help?(text)
    contain_short_words?(text, "help")
  end

  def is_hi?(text)
    contain_short_words?(text, "hi") || contain_short_words?(text, "hello")
  end

  def contain_short_words?(text, w)
    arr = text.to_s.downcase.split(" ")
    text == w || (arr.size <= 3 && arr.include?(w))
  end

  def response(text, robot_obj = nil, chat_type = nil)
    !text.present? and return
    text = text.to_s.strip
    is_help?(text) and return help_content
    is_hi?(text) and return hi_content
    text.size > 30 || customized_command?(text) and return monit_long_words(text, robot_obj, chat_type)
    if arr = text.downcase.scan(/^(#{BASIC_KEYWORDS.join("|")})[#\s]{0,}(.*)/).flatten.presence
      obj, obj_id = arr
      if OBJECT_MAPPINGS[obj] == 'house'
        _house = find_house(obj_id) and return resp_obj(_house)
      end
      return resp_obj(OBJECT_MAPPINGS[obj] || obj, obj_id)
    end
    text =~ /\,|\?|\// and return "Invalid Chars"
    _house = find_house(text) and return resp_obj(_house)
    _user = find_user(text) and return resp_obj(_user)
    chat_type != 'group' and return fetch_response_from_chatgpt(text)
  end

  def hi_content
    "Hi, What can I do for you?"
  end

  def help_content
    File.read("#{Rails.root}/config/bot_helper.txt")
  end

  def find_house(text)
    !text.presence and return
    # Accurate street number
    a = Location.where(locationable_id: House.active).
      where("address2 like ? or address like ?", "#{text} %", "#{text} %").first&.locationable
    # Fuzzy street number
    a ||= Location.where(locationable_id: House.active).
      where("address2 like ? or address like ?", "#{text}%", "#{text}%").first&.locationable
    # Search street name
    a ||= HouseTranslation.where("title like '%#{text}%'").first&.house
    # return
    a
  end

  def find_org(text)
    !text.presence and return
    text = (RedisHelper.get("FastQuote:Org:#{text}").presence || text)
    text_str = "#{text}%"
    Organization.where("name like ?", text_str).first
  end

  def find_user(text)
    !text.presence and return
    text_str = "#{text}%"
    User.where("username like ? or first_name like ? or last_name like ? or email like ?", 
        text_str, text_str, text_str, text_str).first
  end

  def find_house_check_dates(house, text)
    !text.presence and return default_check_dates(house)
    if text.include?("-")
      return parse_dates_from_range_text(text)
    else
      return parse_dates_from_days_text(house, text)
    end
  rescue => e
    LarkMsg.enqueue :debug_helper, "FQ#find_house_check_dates error: #{e.message}, text: #{text}"
    return default_check_dates(house)
  end

  def default_check_dates(house, covered = nil)
    start_date = house&.earliest_available_date_for_house || Date.current
    covered = covered.present? ? covered.to_i : 59
    end_date = start_date + covered
    return start_date, end_date
  end

  def parse_dates_from_range_text(text)
    start_date_str, end_date_str = text.split("-")

    current_year = Time.zone.today.year
    start_month, start_day = start_date_str.split("/").map(&:to_i)
    end_month, end_day = end_date_str.split("/").map(&:to_i)

    end_year = current_year
    end_year += 1 if end_month < start_month || (end_month == start_month && end_day < start_day)

    start_date = Date.new(current_year, start_month, start_day)
    end_date = Date.new(end_year, end_month, end_day)
    return start_date, end_date
  end

  def parse_dates_from_days_text(house, text)
    start_date = house&.earliest_available_date_for_house || Date.current
    covered    = text.present? ? text.to_i : 59
    end_date   = start_date + covered
    return start_date, end_date
  end

  # Low number in DB, can use '%%'
  def find_cleaner(text)
    !text.presence and return
    text_str = "%#{text}%"
    Cleaner.where("full_name like ? or phone like ? or email like ? or note like ?", 
        text_str, text_str, text_str, text_str).first
  end

  def resp_obj(obj, obj_id = nil)
    obj_id == 'summary' and return resp_objs_summary(obj)

    if !obj_id.nil?
      base = {id: obj_id.to_s =~ /^\d+$/ ? obj_id : 0}
      _model = obj.classify.constantize
      obj = _model.where(base).first || __send__("find_#{obj}", obj_id.to_s.gsub("'", "")) rescue nil
    end
    obj.nil? and return
    {
      header: "Found #{obj.class.name} Info ID##{obj.id}", template: "grey",
      elements: obj.basic_card_fields
    }
  end

  def resp_objs_summary(obj)
    base = []
    eles = case obj
    when 'house'
      base << "Count: #{House.active_native_listings.size}\n"
      base += House.active_native_listings.order("ae_user_id").map(&:robot_name_in_list)
    when 'invoice'
    when 'booking'
      base << "Count: #{Booking.booked.size}\n"
    when 'user'
      base << "Count: #{User.active.size}\n"
    end
    base.join("\n")
    # { header: "#{obj.classify} Summary", template: "green", elements: eles }
  end

  def monit_long_words(text, robot_obj = nil, chat_type = nil)
    command, command_body = text.to_s.split("#", 2)
    command_body.nil? && !pure_command?(text) and return read_long_words(text, robot_obj, chat_type)
    command = command.downcase
    !COMMAND_LIST.include?(command) and return 

    body = JSON.parse(command_body) rescue command_body
    res = __send__(command, body, robot_obj) rescue "Command Execute Failed: #{command}"
    res.is_a?(String) and return res
    if res.is_a?(Hash)
      res.try(:dig, :msg_type) == "post" and return res
    end
    return {header: "#{card_title_for_command(command)} Result", elements: res, template: 'green'} if res.present?
    "No result ~"
  end

  def fetch_response_from_chatgpt(text)
    res = gpt(text, nil)
    "#{res || 'None'} (ChatGPT)"
  end

  def card_title_for_command(command)
    c = command.to_s.downcase 
    c == 'fq' and return "Fast Quote"
    command.titleize
  end

  # Whole sentences comunications
  # Total answers reply
  def read_long_words(text, robot_obj = nil, chat_type = nil)
    chat_type != 'group' and return fetch_response_from_chatgpt(text)
  end

  def larkapp
    @larkapp ||= LarkApp.inner_tool
  end

  def openai
    @openai ||= Openai.new
  end

  def _request_remote_quote(params)
    larkapp.post_remote(PublicConfigs.end_points.quotation + "/rpc/quotations/api_json", params)
  end

  def parse_command_params(params)
    params.to_s.split(",").map(&:strip)
  end

  # Second params default indicate organizations
  #   if Second params input a number, it will be as covered days from start_date 
  def parse_quote_params(params, quote_type = 'all_inclusive_no_deposit')
    house_s, org_s, q_type, dates_s = parse_command_params(params)
    house = find_house(house_s)
    start_date, end_date = find_house_check_dates(house, dates_s)

    base = {
      check_in: start_date.to_s, 
      check_out: end_date.to_s,
    }
    params.is_a?(Hash) and return house, base.merge(params)

    org = find_org(org_s)
    return house, base.merge(
      house_id: house&.id, 
      quote_user_id: (org&.users&.last&.id || 0),
      org_id: (org&.id),
      quote_source: (org ? "bd" : "individual"),
      quote_type: quote_type,
      q_type: (q_type.nil? ? "all_in" : q_type)
    )
  end

  def qb_get(params, robot_obj = nil)
    arr = parse_command_params(params)
    obj = arr.first
    obj == 'rca' and return User.revenue_clearing_account_balance.to_s
    json = User.qb_api.get(obj.to_sym, ["DocNumber", arr.last.to_s])
    [{fields: json}]
  rescue => e
    "Error #{e.message}"
  end

  def checkin_list(params, robot_obj = nil)
    params ||= Date.current
    Booking.check_in_at(params).map(&:short_line_for_lark)
  end

  def toggle_rca_period(params, robot_obj = nil)
    if a = RobotTask.rca
      a.fresh_interval(a.interval == 10 ? 1000000 : 10)
      return "Period Changed -> #{a.interval}"
    end
    "No RCA"
  end

  def major_issues(params, robot_obj = nil)
    JiraTask.important_tasks_progress
  end

  def checkout_list(params, robot_obj = nil)
    params ||= Date.current
    Booking.check_out_at(params).map(&:short_line_for_lark)
  end

  def recent(params, robot_obj = nil)
    arr = parse_command_params(params)
    obj, limit = arr
    _model = OBJECT_MAPPINGS[obj] || obj
    _model.classify.constantize.last(limit || 2).flat_map(&:basic_card_fields)
  end

  def tasks(params, robot_obj)
    conditions, sprint = parse_command_params(params)
    sprint ||= "HWA Sprint #{Date.current.strftime("%Y-%m")}"
    arr = CommandCaller.call_ruby("jira.rb", "get_tasks_for_sprint", sprint) rescue []

    is_admin = robot_obj == FeishuObject.admin 
    arr = arr.select do |a| 
      sources = a['source'] || []
      from = is_admin ? [conditions] : robot_obj.matchable_names
      for_robot = from.include?(a['developer']) || (sources & from).present?

      base = a['status'] != 'Done' 
      (conditions == 'all' && is_admin) ? base : (base && for_robot)
    end
    arr.sort_by{|a| a['key']}.reverse.map do |a| 
      sources = (a['source'] || []).join(",")
      "#{a["key"]} #{a["title"]}【#{a["status"]}】[#{a['duedate'] || 'Not-Estimate'}]"
    end
  end

  def rcache(params, robot_obj)
    key, val = parse_command_params(params)
    RedisHelper.cache!(key, val)
  end

  def rsmembers(params, robot_obj)
    "List is -> #{RedisHelper.smembers(params)}"
  end

  %w(sadd srem).each do |a|
    define_method "r#{a}" do |params, robot_obj = nil|
      key, val = parse_command_params(params)
      RedisHelper.send(a, key, val)
      return "New List is -> #{RedisHelper.smembers(key)}"
    end
  end

  %w(separate cluster).each do |_method|
    define_method "#{_method}s" do |params, robot_obj = nil|
      House.where(id: RedisHelper.smembers("#{_method}_houses")).map do |a|
        "H##{a.id}, #{a.address2}, Active: #{a.active}"
      end.join("\n")
    end
  end
  alias :guesties :separates

  def parse_params_for_html(params)
    params = params.to_s
    html_present = false
    main_query, html_fq = params.split('#', 2)

    if html_fq&.downcase == 'html'
      html_present = true
    end

    [main_query, html_present]
  end

  def fast_quote(params, robot_obj)
    house, common_quote_params = nil, {}
    query_params, html_present = parse_params_for_html(params)

    res = %w(all_inclusive_no_deposit all_inclusive).map do |qt|
      house, _params = parse_quote_params(query_params, qt)
      unless common_quote_params.present?
        common_quote_params = _params.dup
        common_quote_params.delete(:quote_type)
      end
      house ||= House.find_by(id: _params["house_id"])
      data = _request_remote_quote(_params)
      !data[:success] and return data[:msg]
      data[:result]
    end

    copied_content = house.quote_copiable_content(
      all_inclusive_no_deposit: res[0], all_inclusive: res[1], quote_params: common_quote_params, html: html_present
    )
    robot_obj.notify(copied_content) rescue nil
    # resp_multy_content(common_info, card_data[0], card_data[1])
    nil
  rescue => e
    "Fast Quote Error #{e.message}"
  end
  alias :fq :fast_quote

  def fast_search(params, robot_obj)
    return nil if params.blank?
    houses_content = House.search_houses_by_address(params)
    return nil if houses_content.blank?
    robot_obj.notify(houses_content) rescue nil
    nil
  rescue => e
    "Fast Search Error #{e.message}"
  end
  alias :fs :fast_search

  def vendor_list(params, robot_obj)
    return nil if params.blank?
    RobotVendorListWorker.perform_async(params, robot_obj&.id)
    "Processing '#{params}'..."
  rescue => e
    "Vendor List Error #{e.message}"
  end
  alias :v :vendor_list

  def plan_of_work(params, robot_obj)
    return nil if params.blank?
    RobotPlanOfWorkWorker.perform_async(params, robot_obj&.id)
    "Processing '#{params}'..."
  rescue => e
    "Plan of work orders error #{e.message}"
  end
  alias :p :plan_of_work
  alias :plan :plan_of_work

  def resp_multy_content(*contents)
    arr = contents.map{|a| (a.is_a?(Array) ? a : [a]).dup << "divider"}.sum
    arr.pop and return arr
  end

  def gpt(params, robot_obj = nil)
    openai.completion_text(params)
  rescue => e
    FeishuObject.admin.notify("GPT error -> #{e.message}")
    "GPT Temporarily unavailable, can not answer your quotesion: #{params}"
  end

  def dalle(params, robot_obj = nil)
    openai.dalle(params)
  end

  # add workorder
  def addwo(params, robot_obj)
    add_workorder(params, robot_obj)
  end

  # add hoc inspection
  def ahi(params, robot_obj)
    add_workorder(params, robot_obj, service_name: "Ad Hoc Inspection")
  end

  # ahi#630 w woodcrest
  #   type: ahi, house: 630, creator: self, assignee: self
  # ahi#630 w woodcrest, roger
  #   type: ahi, house: 630, creator: self, assignee: roger
  # addwo#ahi, 630 w woodcrest, roger
  #   type: ahi, house: 630, creator: self, assignee: roger

  SERVICE_NAME_SHORT_MAPPING = {
    "ahi" => "Ad Hoc Inspection",
  }.with_indifferent_access.freeze

  def add_workorder(params, robot_obj, **options)
    texts = params.split(",").map { |i| i.squish }

    service_name = options[:service_name] || texts.shift
    service = Service.find_by_name(SERVICE_NAME_SHORT_MAPPING[service_name] || service_name) if service_name.present?

    house = texts.map { |text| find_house(text) }.compact.first
    return "Create Workorder Faild: can't find a house" if house.blank?

    engineer = FeishuObject.find_user_with("shuo")
    creator = robot_obj || engineer
    pms_creator = creator&.pms_user
    assignee = texts.map { |text| FeishuObject.find_user_with(text) }.compact.first
    pms_assignee = assignee&.pms_user
    service ||= texts.map { |text| Service.find_by_name(SERVICE_NAME_SHORT_MAPPING[text] || text) }.compact.first
    return "Create Workorder Faild: can't find a service" if service.blank?

    payload = {
      house_id: house.id,
      creator_id: pms_creator&.id,
      assignee_id: pms_assignee&.id || pms_creator&.id,
      service_id: service.id
    }

    workorder = create_pms_workorder(payload)

    workorder.create_lark_task(
      {
        skip_send_lark_msg: true,
        task_payload: workorder.lark_task_payload(collaborator_ids: [assignee&.open_id || creator.open_id], follower_ids: [creator.open_id, engineer.open_id].compact)
      }
    )

    # "Workorder Created! Task Url: #{workorder.lark_task_view_url}"
    workorder.create_lark_task_msg
  end

  def create_pms_workorder(payload)
    api_auth_token = "fg-api-#{SecureRandom.uuid}"
    RedisHelper.cache(api_auth_token, "internals#create_workorder")
    payload[:api_auth_token] = api_auth_token
    url = "#{PublicConfigs.end_points.pms}/internals/create_workorder"

    res = Faraday.post(url, payload.to_json, { content_type: "application/json" })

    res_body = JSON.parse(res.body).with_indifferent_access rescue {}

    ServiceSchedule.find(res_body.dig(:data, :id))
  end

  def parse_date(date)
    %w(%m/%d/%Y %d/%m/%Y %Y-%m-%d).map do |a| 
      if date = DateTime.strptime(date, a) rescue nil
        return date
      end
    end
    nil
  end

  def parse_amount(str)
    str.gsub(/\$|\*|\,/,"").to_d rescue nil
  end
end
