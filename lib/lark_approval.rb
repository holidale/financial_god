class LarkApproval

  ENDPONT = "https://open.feishu.cn/open-apis/approval/v4"
  CODE_MAPPERS = Approval.approval_codes.with_indifferent_access

  attr_accessor :m2m, :approval_code, :approval_app
  def initialize(approval_code)
    @m2m = LarkApp.inner_tool
    @approval_code = approval_code
    @approval_app = CODE_MAPPERS.invert[approval_code]
  end

  # detail.dig(:data, :form)
  def detail
    m2m.get_remote("#{ENDPONT}/approvals/#{approval_code}")
  end

  def records(start_at = 1.days.ago)
    m2m.get_remote("#{ENDPONT}/instances", {
      approval_code: approval_code, start_time: start_at.to_i * 1000, end_time: Time.now.to_i * 1000
    }).dig(:data, :instance_code_list) || []
  end

  def parse_to_pms(start_at = 1.days.ago)
    records(start_at).map {|_a| parse_approval(_a) }
  end

  def parse_approval(_a)
    appr = m2m.get_remote "#{ENDPONT}/instances/#{_a}"
    status = appr.dig('data', "status")
    status == "APPROVED" || not_reimbursement? and return parse_approval_to_db(_a, appr)
    "Get Approval #{_a} #{status}, Skipped"
  end

  def cancel(_code, lark_user_id)
    params = {
      approval_code: approval_code,
      instance_code: _code,
      user_id: lark_user_id,
      # notify_starter: true
    }
    m2m.post_remote("#{ENDPONT}/instances/cancel?user_id_type=user_id", params)
  end

  def is_po?
    approval_code == CODE_MAPPERS[:po]
  end

  def not_reimbursement?
    approval_code != CODE_MAPPERS[:reimbursement]
  end

  # params
  #  before 2023 one cost_item one approval, creator_id params is easy to test
  #  currently one cost one approval, creator_id params is easy to test
  def create(cost_id, creator_id = nil)
    ci = Cost.find(cost_id)
    ci.done? and return "PO##{cost_id} Done"
    is_rejected = ci.rejected?.dup
    ci.approval && !is_rejected && !ci.cancelled? and return "PO##{cost_id} has Already to Lark"

    params = ci.to_approval_form(creator_id)
    !params.presence and return "PO Skip to Lark: Amount 0"

    params.each do |pp|
      if pp[:name] && (_list = radio_list(pp[:name])).presence
        pp[:value] = _list[pp[:value]]
        pp[:value] = _list["None"] if pp[:name] == "AE" && pp[:value].blank?
      end
    end

    po_submitter = ci.po_submitter(creator_id)
    po = m2m.post_remote("#{ENDPONT}/instances", {
      approval_code: approval_code, open_id: po_submitter.open_id, form: params.to_json
    })
    if _code = po.dig('data', 'instance_code').presence
      res = parse_approval(_code)
      a = Approval.find_by(key: _code)
      a.update(cost_id: cost_id) if a
      pre_fix = is_rejected ? "Rejected " : ""
      LarkMsg.enqueue(:po_details, a.simple_info.to_a.map{|aa| ["**#{aa[0]}**", aa[1]].join(": ")}) rescue nil
      return "#{pre_fix}PO Approval##{a&.id} PO##{cost_id} Submitted from #{po_submitter&.name}"
    end
    "Create PO Approval Error #{cost_id} from #{po_submitter&.name}
     Params: #{params.to_json}
     Error: #{po['msg']}"
  end

  def parse_approval_to_db(_a, po)
    data = po["data"].slice(*%w(approval_code approval_name department_id
      end_time form open_id serial_number start_time status task_list timeline user_id uuid))
    Approval.init_from_lark(_a, data) rescue "Get Approval #{_a} Failed"
  end

  def definition
    @definition ||= m2m.get_remote("#{ENDPONT}/approvals/#{approval_code}")
  end

  def radio_list(name = "Property")
    form = definition.dig("data", "form")
    Hash[JSON.parse(form).find{|a| a.values.index(name)}["option"].map{|a| [a['text'], a['value']]}]
  rescue => e
    []
  end

  def remote_data(_a)
    m2m.get_remote "#{ENDPONT}/instances/#{_a}"
  end

  def self.reimbursement
    @reimbursement ||= new(CODE_MAPPERS[:reimbursement])
  end

  def self.po_test
    @po_test ||= new(CODE_MAPPERS[:po_test])
  end

  def self.help_desk
    @help_desk ||= new(CODE_MAPPERS[:help_desk])
  end

  def self.po
    !Approval.po_taking_effect? and return po_test
    @po ||= new(CODE_MAPPERS[:po])
  end
end
