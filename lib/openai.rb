class Openai

  def initialize
    @api_key = AppConfig.openai.app_secret
    @client = OpenAI::Client.new(access_token: @api_key)
  end

  # temperature controls the degree of randomness in the generated text.
  # A higher temperature will result in more creative and unpredictable text,
  # while a lower temperature will produce more predictable and conservative text.
  # The temperature parameter is a float value between 0 and 1,
  # with 0 meaning that the algorithm will always choose the most likely word,
  # and 1 meaning that the algorithm will choose words randomly according to their likelihood.
  #
  # max_tokens controls the length of the generated text,
  # measured in the number of tokens (words or punctuation marks) in the output.
  # The max_tokens parameter is an integer value that sets the maximum number of tokens
  # that the API will generate in response to a given prompt.
  def completion_text(input, max_tokens = 4000, temperature = 0.7, model = "gpt-4-1106-preview")
    response = @client.chat(
      parameters: {
        model: model,
        messages: [{ role: "user", content: input.encode("UTF-8") }],
        max_tokens: max_tokens,
        temperature: temperature
      })
    return response.dig("choices", 0, "message", "content")
  end
  alias :chat :completion_text

  # DALLE 2 will generate image based on your describe
  def dalle(input, size = "512x512")
    response = @client.images.generate(
      parameters: {
        prompt: input,
        size: size
      })
    return response.dig("data", 0, "url")
  end

  def whisper(file, model="whisper-1")
    response = client.translate(
      parameters: {
        model: model,
        file: File.open(file, "rb"),
      })
    return response.dig("text")
  end

end

