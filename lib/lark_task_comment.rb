class LarkTaskComment
  ENDPONT = "https://open.feishu.cn/open-apis/task/v1/"

  attr_accessor :m2m, :params, :task_id, :comment_id, :payload
  def initialize(**params)
    @m2m = LarkApp.inner_tool
    @params = params.with_indifferent_access
    @task_id = self.params[:task_id]
    @comment_id = self.params[:comment_id]
    @payload = self.params[:payload]
  end

  def create
    m2m.post_remote("#{ENDPONT}/tasks/#{task_id}/comments", payload)
  end

  def remote_data
    m2m.get_remote("#{ENDPONT}/tasks/#{task_id}/comments/#{comment_id}")
  end
end