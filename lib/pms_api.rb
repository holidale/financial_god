module PmsApi
  extend self

  Headers = {"Content-Type": "application/json; charset=utf-8"}

  def request(url, params)
    res = Faraday.get(PMS_ENDPOINT+url, params, headers_with_token)
    hook_request(res)
  end

  def headers_with_token
    Headers.merge({"x-sign": get_signature})
  end

  def get_signature
    current = Time.now.utc.to_i*1000
    secret = AppConfig.m2m_api.api_secret
    _to_be_hashed = secret + current.to_s
    hashed = Digest::SHA2.new(256).hexdigest(_to_be_hashed)
    signature = "#{hashed}&#{current}"
  end

  def hook_request(res)
    body = res.body
    JSON.parse(body)&.with_indifferent_access rescue {}
  end

end