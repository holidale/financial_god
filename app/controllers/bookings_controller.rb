class BookingsController < ApplicationController
  def show
    @booking = Booking.find(params[:id])
    @extensions = @booking.extensions
    @ecolors = rand_colors(@extensions.size + 1, opacity: 0.9)
  end

  def owner_revenue
    show
    render 'show'
  end
end
