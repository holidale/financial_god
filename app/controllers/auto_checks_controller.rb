class AutoChecksController < ApplicationController
  before_action :get_basic_data

  def extensions
    @filter_columns = %w(period)
    @extensions = ContractAmendment.active.includes(:booking).
      for_period_like(@period, "start_date").
      order("booking_id DESC")
    @invoices = Invoice.effective.where(booking_id: @extensions.map(&:booking_id)).extension
  end
end
