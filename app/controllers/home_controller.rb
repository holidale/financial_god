class HomeController < ApplicationController

  before_action :auth_user, only: [:index, :compare_journal_entries, :compare_invoices]
  before_action :verify_authenticity_token, only: [:index, :compare_journal_entries, :compare_invoices]
  before_action :get_basic_data
  before_action :get_finance_data
  before_action :base_compare_params, only: [:compare_invoices, :compare_journal_entries]

  def index
    @invoice_total = RedisHelper.get("Invoice:total_amounts")
    @invoice_cache = Invoice.global_cache
    @deposit_cache = Deposit.global_cache
  end

  def compare_journal_entries
    @pms_jes = JournalEntry.where(payment_id: @pms_invoice_ids)
    @qb_jes = QbObject.for_journal_entries.where(local_value: @pms_jes.map(&:journal_no).uniq)
  end

  def compare_invoices
    @qb_invoices = QbObject.for_invoice.where(local_value: @pms_invoice_ids)
  end

  def auth
    if current_user = User.auth_from_pms(params[:session_key])
      session[:user_id] = current_user.id
    end
    rpath = root_path
    rpath = session[:return_to] if session[:return_to] && !session[:return_to].in?(root_path) &&
      session[:return_to].to_s !~ /^\/auth/
    session[:return_to] = nil
    redirect_to rpath
  end

  def forbidden
    redirect_to PMS_ENDPOINT + "/new_financial_system", allow_other_host: true
  end

  def feishu_events
    params.permit!
    LarkEventWorker.perform_async(params.to_json)
    render json: {challenge: params[:challenge]}
  end

  def ai_events
    params.permit!
    AiEventWorker.perform_async(params.to_json)
    render json: {challenge: params[:challenge]}
  end

  def jira_events 
    params.permit!
    JiraIssuesWorker.perform_async(params.to_json)
    render json: {result: "succuess"}
  end

  def bitbucket_events
    params.permit!
    BitbucketWorker.perform_async(params.to_json)
    render json: {result: "succuess"}
  end

  def feishu_auth_from_external
    return to_feishu_auth_url(params[:source]) if !session[:lark_open_id].present? && !current_user

    lark_user = current_user&.feishu_user || FeishuObject.find_user_with(session[:lark_open_id])
    back_to_third_system(lark_user, params[:source])
  end

  def feishu_auth
    oid = session[:lark_open_id]
    fetch_lark_user(oid, params[:state]).presence and return

    if remote_code = params[:code]
      user_info = lark_app.request_feishu_user_info(remote_code) || {}
      oid = session[:lark_open_id] = user_info["open_id"]
    end
    oid.nil? || !lark_app.logined_user?(oid) and return to_feishu_auth_url
    fetch_lark_user(oid, params[:state])
  end

  def redirect_after_feishu_user_authed(feishu_user, state)
    back_to_third_system(@lark_user, state) if state != 'state'
  end

  def salesforce_hook
    Rails.logger.info(params.to_json)
    RedisHelper.lpush("salesforce_hooked_data", params.to_json)
  end

  def gmail_hook
    Rails.logger.info(params.to_json)
    case params["type"]
    when "services"
      msg_card = {
        header: "Service Gmail Received a New Email",
        template: "grey",
        elements: [
          { fields: { From: params["sender"], Time: params["datetime"], Subject: params["subject"], Link: params['url'] }}
        ]
      }
      LarkMsg.enqueue(:service_gmail_alarm, msg_card)
    else
    end
    render json: { result: "success" }
  end

  def big_screen_data
    json = RedisHelper.get_json("BigScreenData")
    json["unoccupied_list"] = json["unoccupied_list"].select{|a| a["active"]}.sort_by{|a| a["since"] || Date.current.to_s}
    json["orders_list"] = json["orders_list"].map do |a| 
      a["display_amount"] = PmsRecord.to_currency(a[:total_amount].to_f)
      a
    end
    %w(rental expense occ adr revpar revpar_bar cpor cpar).each do |a|
      val = a == 'occ' ? json[a].to_f * 100 : json[a].to_f
      json[a] = [{value: val}]
    end
    ws = json.delete("workorders")
    %w(overdue ongoing total finished).each do |a|
      json["workorders_#{a}"] = [{value: ws[a]}]
    end

    render json: json[params[:key]] || json
  end

  def rocket_chats
    Rails.logger.info(params.to_json)
    # msg = "#{params["user_name"]}: #{params["text"]}"
    msgs = params.dig("messages").presence || []
    if msg = msgs.first
      uname = msg.dig("u", "name") ||  msg.dig("u", "username")
      content = msg.dig("msg")
      LarkMsg.enqueue(:rocket_chats, header: 'New RocketChat Msg', elements: ["#{uname}: #{content}"])
    end
    render json: {result: "succuess"}
  end

  def jira_dashboard
    @tasks = JiraTask.current_month
    @tasks = JiraTask.last(100) if Rails.env.development?
    render layout: false
  end

  private

  def back_to_third_system(lark_user, source)
    url = URI(source)
    lark_token = SecureRandom.uuid
    RedisHelper.cache!("LarkToken-#{lark_token}", lark_user&.pms_user&.id) rescue nil

    origin_site = [url.scheme, [url.host, url.port].join(":")].join("://")
    callback = origin_site + "/auth_from_lark"
    callback = "#{callback}?lark_token=#{lark_token}&source=#{url}"
    redirect_to callback, allow_other_host: true
  end

  def fetch_lark_user(oid, state)
    if lark_user_info = lark_app.get_cached_user_info(oid).presence
      @lark_user = FeishuObject.find_user_with lark_user_info[:open_id]
      return redirect_after_feishu_user_authed(@lark_user, state) if @lark_user
    end
  end

  def to_feishu_auth_url(state = 'state', redirect_uri = nil)
    redirect_to(lark_app.auth_url(redirect_uri, state), allow_other_host: true)
  end

  def base_compare_params
    @filter_columns = %w(period)
    # @period_column = "finance_create_at"
    @base_invoices = Invoice.compared_period_in(@period)
    @pms_invoice_ids = @base_invoices.map(&:id)
  end

  def lark_app
    @lark_app ||= LarkApp.inner_tool
  end
end
