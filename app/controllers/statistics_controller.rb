class StatisticsController < ApplicationController

  before_action :get_basic_data
  before_action :get_finance_data, except: [:daily_incomes_overview]
  before_action :no_periods, only: [:houses_overview, :house_detail, :bookings_overview]

  def invoices_overview
    @finance_data = @finance_data.call.for_data_source(@data_source)
  end

  def journal_entries_overview
    @filter_columns = %w(period income_accounts)
    @revenues = filter_accounts JournalEntry.for_period_like(@period, "journal_date")
  end

  def deposits_overview
    @filter_columns = %w(period)
    @finance_data = Deposit.for_period_like(@period)
  end

  def daily_incomes_overview
    @filter_columns = %w(period)
    @finance_data = DailyIncome.for_period_like(@period, "day")
  end

  def houses_overview
    @houses = House.active_native_listings.sort_by(&:bookings_count).reverse
  end

  def bookings_overview
  end

  def ar_overdue_summary
    @filter_columns = %w(period)
    if @is_for_user = params[:source] == "user"
      @objs = User.where(id: AccountRecord.unpaid_from_invoice.individuals.map(&:responser_id))
    else
      @objs = Organization.all
    end
    @data = Hash[@objs.map do |org|
      [org, org.invoices_summary(@period)]
    end]
  end

  def ar_overdue_detail
    @filter_columns = %w(period)
    if @is_for_user = params[:source] == "user"
      @obj = User.find_by(id: params[:obj_id])
    else
      @obj = Organization.find_by(id: params[:obj_id])
    end
    @invoices = @obj.invoices_summary(@period)
  end

  def house_detail
    @house = House.find(params[:id])
    @rental_infos = @house.cached_rental_info
    @bookings = @house.booked_bookings.includes(:user, :creator, :contract_amendments).recent
  end

  private

  def no_periods
    @filter_columns = []
  end
end
