class Admin::FeishuObjectsController < AwesomeAdminController
  def index
    self.collections = paginate_objs(FeishuObject.user_and_groups, per: 100)
  end

  def config_tasks
    get_base_obj 
  end

  def update
    params[:feishu_object] = {task_ids: []} if !params[:feishu_object]
    params.permit!
    super
  end
end
