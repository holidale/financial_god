class Admin::RobotsController < AwesomeAdminController
  def config_tasks
    @robot = Robot.find(params[:id])
  end

  def update
    params[:robot] = {task_ids: []} if !params[:robot]
    params.permit!
    super
  end
end
