class ApplicationController < ActionController::Base
  before_action :auth_user
  helper_method :current_user, :is_financial?
  include SharedHelper if defined?(SharedHelper)
  include ColorsHelper

  if Rails.env.production?
    rescue_from Exception, with: lambda { |exception|
      proj_file = exception.backtrace.select{|a| a.to_s =~ /releases/}
      LarkMsg.enqueue(:financial_system_error, proj_file.unshift(exception.message).join("\n"))
      # render_error 500 and return
    }
  end

  def get_basic_data
    parse_period
    @finance_type = params[:finance_type] ||= "Invoice"
    @data_source = params[:data_source] ||= "PMS"
    @company = params[:company] ||= "holidale"
    @income_accounts = params[:income_accounts] ||= RENTAL_INCOME_ACCOUNT

    @filter_columns ||= AccountRecord::BasicSearchColumns
  end

  def parse_period
    @period = params[:period] || session[:period] || Time.current.strftime("%Y-%m")
    @period = Time.current.strftime("%Y-%m") if @period.to_s.size < 4
    session[:period] = @period

    @start_date, @end_date = @period.to_s.split(",").map(&:to_date) rescue []
  end

  private

  def auth_user
    session[:return_to] = request.path
    !current_user and return redirect_to(forbidden_path, allow_other_host: true)
  end

  def auth_internal_api_token
    return redirect_to(forbidden_path, allow_other_host: true) unless PublicConfigs.internal_api_token == params[:auth_token]
  end

  def is_tool?
    request.host == 'tools.month2month.com'
  end

  def is_financial?
    !is_tool?
  end

  def current_user=(_current_user)
    @current_user ||= _current_user
  end

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
    if @current_user&.pms_logout?
      session[:user_id] = nil
      return
    end
    @current_user
  end

  def filter_accounts(collection)
    collection.select {|j| @income_accounts.split(",").include?(j.account) }
  end

  def get_finance_data
    @period_column ||= Dateable::DEFAULT_DATE_COLUMN
    @finance_data = -> {AccountRecord.for_period_like(@period, @period_column)
      .for_company(@company)
      .for_finance_type(@finance_type)
    }
  end

end
