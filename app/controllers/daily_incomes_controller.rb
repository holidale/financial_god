class DailyIncomesController < ApplicationController
  before_action :get_basic_data
  before_action :get_finance_data, except: [:month_overview]

  def month_overview
    @filter_columns = %W(period income_accounts)
    @finance_data = DailyIncome.for_account(@income_accounts).for_period_like(@period, "day")
    @journal_entries = DailyIncome.journal_entries_from_ids(@finance_data.flat_map(&:je_ids).uniq)
    @total_covered = @journal_entries.map{|j| j.covered_in_period(@period)}.sum
    group_by_sales
  end

  private

  def group_by_sales
    @sales = User.where(id: @journal_entries.map(&:sales_id).uniq)
    @sales_data = @sales.map do |s| 
      amount = @journal_entries.select{|j| j.sales_id == s.id }.map do |j|
        j.amount_in_period(@period)
      end.sum.round(2)
      {sales: s, amount: amount}
    end.sort_by{|a| a[:amount]}.reverse
  end
end
