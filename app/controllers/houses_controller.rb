class HousesController < ApplicationController
  skip_before_action :auth_user
  before_action :auth_internal_api_token

  def coordinate
    house = House.find(params[:id])
    location = house&.location

    render json: { code: 200, result: "success", latitude: location&.latitude, longitude: location&.longitude, airbnb_id: house&.latest_airbnb_id}
  end

end
