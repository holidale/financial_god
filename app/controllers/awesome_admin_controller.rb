class AwesomeAdminController < ApplicationController
  before_action :get_base_obj, only: [:edit, :update, :destroy, :show]
  before_action :auth_admin

  def index
    self.collections = paginate_objs(klass)
  end

  def new
    self.single_obj = klass.new
  end

  def edit; end
  def show; end

  def create
    self.single_obj = klass.new(params[obj_str])
    to_or_not_to?(single_obj)
  end

  def update
    single_obj.update(params[obj_str]) if params[obj_str]
    back_to_list 
  end

  def destroy
    back_to_list(single_obj.destroy)
  end

  private
  def get_base_obj
    self.single_obj = klass.find(params[:id])
  end

  def paginate_objs(objs, per: 100)
    objs = objs.is_a?(Array) ? Kaminari.paginate_array(objs) : objs
    objs.page(params[:page]).per(per).order("id DESC")
  end

  def back_to_list(condition = true, msg = nil)
    return render(:edit) unless condition
    flash[condition ? :info : :danger] =
      condition ? "Operation completed" : single_obj.errors.full_messages.join("\n")
    flash[:info] = msg if msg
    redirect_to(action: :index)
  end

  def to_or_not_to?(obj)
    obj.save ? back_to_list("OK") : render(:new)
  end

  def auth_admin
    redirect_to root_path if !current_user.is_admin?
    params.permit!
  end
end
