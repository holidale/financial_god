class InvoicesController < ApplicationController
  def unpaid
    @unpaid = Invoice.unpaid.due_over_2_years
  end
end
