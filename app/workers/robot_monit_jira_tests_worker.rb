class RobotMonitJiraTestsWorker < RobotBase
  def perform(**args)
    # data = CommandCaller.call_ruby("jira.rb", "wait_to_test_list")
    data = JiraTask.testing.flat_map {|a| a.msg_for_cases.presence }.compact
    check_and_enqueue_data(args, data)
  end
end
