class RobotMonitFixExceptionDataWorker < RobotBase
  def perform(**args)
    ss = %w(partially_paid paid refunded partially_refunded)
    data = Invoice.recent_updated_in_minutes(60).map do |i|
      msg = ''
      if i.status.in?(ss) && i.paid_amount.to_d != i.total_receivable.to_d
        # msg += "I##{i.id} Paid Status #{i.status}: #{i.paid_amount.to_d}/#{i.total_receivable.to_d}"
        i.quiet_update_finance_values
        msg += "\n I##{i.id} Balance Due Still Wrong: Due -> #{i.balance_due}" if i.paid? && i.balance_due.to_f > 0
      end
      if ar = i.account_record
        if ar.status != i.status
          msg += "\n I##{i.id} Status Wrong"
          ar.update(status: i.status) 
          msg += "\n #{ar.status == i.status ? "Corrected" : "Still Wrong After Update"}"
        end
      end

      if rps = i.received_payments.select{|a| a.qbid.presence}.group_by(&:qbid).select{|k,v|v.size > 1}.presence
        msg += "\n RP repeated for I##{i.id}"
        rps.map do |k,v|
          v.last.notify_pms_data_update("clean_destroy")
        end
      end

      if (rps1 = i.received_payments.presence) && (rps1.size > 1) && (rps1.uniq.size < rps1.size)
        msg += "\n Alert!!!  Repeated payment_received_payments for #{i.id}"
      end

      msg
    end.select(&:present?).compact
    AccountRecord.where(status: 'voided').destroy_all
    check_and_enqueue_data(args, data)
  end
end
