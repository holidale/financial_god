class RobotMonitPoWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    data = Cost.recent_created_in_minutes(args[:interval]).map do |c|
      res, msg = c.can_to_lark? 
      res ? c.to_lark_approval : msg
    end

    data += Cost.recent_updated_in_minutes(args[:interval]).map do |c|
      next if (c.updated_at - c.created_at).abs < 5
      res, msg = c.can_to_lark? 
      if res
        c.can_resubmit? ? c.to_lark_approval : "PO##{c.id} Updated But Not to Resubmit"
      else
        msg
      end
    end

    # TEMP SOLUTION: check lost every 24 hours (Should push to queue POs needed to be pushed to lark approval instead)
    Cost.recent_created_in_minutes(60 * 24).holidale.effective_purchase_order.map do |a|
      if a.approval.nil? && a.can_to_lark?
        a.to_lark_approval
        data += "Lost PO##{a.id} to Lark"
      end
      a.hook_lark if a.approval&.approved?
    end if (Time.current.min <= 1)
    check_and_enqueue_data(args, data.compact)
  end
end
