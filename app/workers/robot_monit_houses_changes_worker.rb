class RobotMonitHousesChangesWorker < RobotBase
  def perform(**args)
    last_hids = JSON.parse(RedisHelper.get("ListingHouseIds")) rescue []
    hids = House.active_native_listings.map(&:id).uniq
    data = []
    if last_hids != hids
      RedisHelper.cache!("ListingHouseIds", hids)
      data << "New Signed --> #{hids - last_hids}" if last_hids.present? && (hids - last_hids).present?
      data << "House Lost --> #{last_hids - hids}" if (last_hids - hids).present?
      data << "Current Listings Count: #{hids.size} ---> #{hids}"
    end
    check_and_enqueue_data(args, data)
  end
end
