class RobotMonitDeployPmsWorker < RobotBase
  def perform(**args)
    deployed_data = JSON.parse(get_deployed_data).presence rescue nil
    spark_msgs = check_spark_deploy_data || [] rescue []
    new_pms_msgs = check_new_pms_deploy_data || [] rescue []
    check_and_enqueue_data(args, deployed_data)
  end

  def get_deployed_data
    new_version = `ssh -p 20202 m2m "ls /opt/webapp/holidale/revisions.log -l"`
    last_version = RedisHelper.get("PMS_deploy_version")
    last_version == new_version and return
    RedisHelper.cache!("PMS_deploy_version", new_version)

    tasks_command = "/home/webuser/.rvm/rubies/ruby-2.5.8/bin/ruby /home/webuser/logs/deploy_checker.rb"
    `ssh -p 20202 m2m "#{tasks_command}"`
  rescue => e
    LarkMsg.enqueue(:deploy_data_error_from_pms, "PMS deployed_data error #{e.message}")
    nil
  end

  def check_spark_deploy_data
    check_react_project(:spark)
  end

  def check_new_pms_deploy_data
    check_react_project(:pms_frontend)
  end

  MAPPINGS = {
    spark: {server: 'spark', path: "/opt/webapp/spark"},
    pms_frontend: {server: 'm2m', path: "/opt/webapp/m2m_pms_frontend" },
  }
  def check_react_project(proj)
    config = MAPPINGS.dig(proj) || {}
    new_version = `ssh -p 20202 webuser@#{config[:server]} 'ls -l #{config[:path]}/run'`

    key = "#{proj.to_s.titleize}_deploy_version"
    last_version = RedisHelper.get(key)
    last_version == new_version and return
    RedisHelper.cache!(key, new_version)

    dir = new_version.split("->").last.to_s.strip
    last_commit = RedisHelper.get("#{key}:last_commit")
    new_version_commits = `#{send("#{proj}_command", "#{dir} #{last_commit}")}`.split("\n")
    new_commit = new_version_commits.shift.to_s.split(" ").last
    RedisHelper.cache!("#{key}:last_commit", new_commit)

    content = new_version_commits.map{|a| a.split(" ", 2).last}.select{|a| a.size > 11}
    if msgs = content.map{|a| parse_commit_msg(a) }.presence
      _lark_msgs = msgs.map{|a| a.values.join("/") rescue a.to_s}
      _lark_msgs.unshift("**#{proj.to_s.titleize} Released**")
      LarkMsg.enqueue(:"#{proj}_released", header: false, elements: _lark_msgs) if _lark_msgs.size < 20
      msgs.map{|a| Jira.close_issue a[:key]}
    end
    msgs 
  rescue => e
    LarkMsg.enqueue(:"deploy_data_error_from_#{proj}", "#{proj.to_s.titleize} deployed_data error #{e.message}")
    nil
  end

  def spark_command(ps = "")
    "ssh -p 20202 webuser@spark 'sh /home/webuser/zmsun/deploy_check.sh #{ps}'"
  end

  def pms_frontend_command(ps = "")
    "ssh -p 20202 webuser@m2m 'sh /home/webuser/zmsun/deploy_new_pms.sh #{ps}'"
  end

  def parse_commit_msg(_msg)
    msg = _msg.split("Merged in ").last.to_s
    type, msg = msg.split("/")
    _, key, msg = msg.split(/([A-Z]+\-\d+)/, 2)
    msg = msg.split("-").map(&:presence).compact.join(" ").split("(").first.strip.humanize
    {type: type.titleize, key: key, msg: msg}
  rescue => e
    _msg
  end
end
