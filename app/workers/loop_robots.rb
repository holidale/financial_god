class LoopRobots
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    RobotTask.all.select(&:can_run_at?).map(&:run!)
  end
end
