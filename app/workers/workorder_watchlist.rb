class WorkorderWatchlist
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    overdue_workorders_message_card = generate_message_content

    FeishuObject.where(outer_msg_keys: 'workorder_watchlist').each do |lark_chat|
      receive_id = extract_receive_id(lark_chat)
      next unless receive_id

      msg_id = fetch_message_id(receive_id)
      if update_existing_message?(msg_id)
        update_message(msg_id, overdue_workorders_message_card)
      else
        send_new_message(lark_chat, overdue_workorders_message_card, receive_id)
      end
    end
  end

  private

  def extract_receive_id(lark_chat)
    if lark_chat.user?
      lark_chat.open_id
    elsif lark_chat.group_chat? || lark_chat.p2p_chat?
      lark_chat.chat_id
    end
  end

  def fetch_message_id(receive_id)
    RedisHelper.get("Watchlist: receive_id #{receive_id}") rescue nil
  end

  def update_existing_message?(msg_id)
    # Lark Message Card has edit limit of 20 times
    msg_edit_times = RedisHelper.get("Watchlist: msg_id #{msg_id} use times") rescue nil
    msg_id.present? && msg_edit_times.present? && msg_edit_times.to_i < 20
  end

  def update_message(msg_id, message_card)
    lark_app.patch_remote("#{LarkApp::BasicMessageUrl}/#{msg_id}", message_card)
    increment_msg_edit_times(msg_id)
  end

  def increment_msg_edit_times(msg_id)
    RedisHelper.incr("Watchlist: msg_id #{msg_id} edit times")
  end

  def send_new_message(lark_chat, message_card, receive_id)
    msg_res = if lark_chat.user?
                lark_app.send_single_msg_to_user(receive_id, message_card)
              elsif lark_chat.group_chat? || lark_chat.p2p_chat?
                lark_app.send_single_msg_to_chat(receive_id, message_card)
              end

    msg_id = msg_res&.dig("data", "message_id")

    if msg_id
      RedisHelper.cache("Watchlist: receive_id #{receive_id}", msg_id, 6.hours)
      RedisHelper.cache("Watchlist: msg_id #{msg_id} edit times", 0, 6.hours)
    end
  end

  def generate_message_content
    message_elements = generate_message_elements
    header = generate_message_header
    {
      "config": {},
      "i18n_elements": {
        "en_us": message_elements
      },
      "i18n_header": {
        "en_us": header
      }
    }.to_json
  end

  def generate_message_elements
    ServiceSchedule.current_overdue.includes_tables.sort_by(&:due).map do |wo|
      {
        "tag": "markdown",
        "content": "[#{wo.service&.name}](#{INTERNAL_SITE}/workorder/option/#{wo.id}) at #{wo&.full_address} needs addressing as of #{wo.due}.",
        "text_align": "left",
        "text_size": "normal"
      }
    end.presence || default_message_element
  end

  def default_message_element
    [{
       "tag": "markdown",
       "content": "No overdue work orders at this time.",
       "text_align": "left",
       "text_size": "normal"
     }]
  end

  def generate_message_header
    {
      "title": {
        "tag": "plain_text",
        "content": "Workorder Watchlist"
      },
      "subtitle": {
        "tag": "plain_text",
        "content": "Workorders to Address"
      },
      "template": "violet"
    }
  end
end