class RobotMonitPmsStatusWorker < RobotBase
  Services = {
    # "SparkNewWeb" => "https://new.month2month.com/",
    "Month2MonthSite" => "https://month2month.com/",
    "InternalWeb5" => "https://internal.month2month.com/",
    "HolidaleWeb" => "https://holidale.com/",
    "WhiteLabelSite" => "http://45.33.53.229/",
    "ElasticSearch" => "http://es-service:9220",
    "ChatAdmin" => "https://chat.month2month.com/",
    "M2MSearch" => "https://m2msearch.month2month.com/",
  }

  def perform(**args)
    data = Services.map do |s, u|
      res = monit_service(s, u)
      errors = nil
      errors = res[:data]&.last if res.is_a?(Hash)
      errors || monit_response(s, res)
    end.compact
    if data.empty? && Time.current.min % 5 == 0
      LarkMsg.enqueue(:heart_beat_all_services_ok, "#{Services.keys.join(", ")} Are All OK")
    end
    check_and_enqueue_data(args, data)
  end

  def monit_service(service_name, url)
    _res = rescue_remote_request(service_name) {Faraday.get(url) } rescue nil
    _res.is_a?(Hash) and return _res
    # _res.nil? and return {alert: true, data: ["Request Service #{service_name} timeout in 10 seconds"]}
    _res&.status != 200 and return {alert: true, data: ["#{service_name} is Down, status is #{_res&.status || 'Error Response'}"]}
    _res
  end
  
  def monit_response(s, res)
    if s == 'InternalWeb5'
      doc = Nokogiri::HTML res.body
      if assets_res = Faraday.get(doc.css(".header-logo img")[0].attr("src")) rescue nil
        assets_res.status != 200 and return {alert: true, data: ["Assets in Web5 is Down, status is #{status}"]}
      end
    else
      #TODO
    end
    nil
  end
end
