class RobotMonitNearCheckInBookingWorker < RobotBase
  def perform(**args)
    data = Booking.near_check_in.map(&:id)
    data = "No Check-In Houses on tomorrow" if data.empty?
    check_and_enqueue_data(args, data)
  end
end
