class RobotWorkorderWatchlistWorker < RobotBase


  INTERNAL_NEW_PMS_SITE = 'https://internal.month2month.com/new_pms'

  def perform(**args)
    overdue_workorders_message_card = generate_message_content

    FeishuObject.where("outer_msg_keys like ?", '%workorder_watchlist%').each do |lark_chat|
      receive_id = extract_receive_id(lark_chat)
      next unless receive_id

      msg_id = fetch_message_id(receive_id)
      if msg_id && update_existing_message?(msg_id)
        update_message(msg_id, overdue_workorders_message_card)
      else
        send_new_message(lark_chat, overdue_workorders_message_card, receive_id)
      end
    end

    {alert: false}

  end

  private

  # Retrieves configurations for watchlist filtering.
  #
  # - `watchlistRegion` is expected to be an array of valid house_region strings.
  #   Example: ['Orange County', 'Austin and Nearby', 'Los Angeles and Nearby']
  #
  # - `watchlistTimeRange` is expected to be a hash with two keys: 'start' and 'end',
  #   each containing DateTime objects as values.
  #   Example: {
  #              'start' => 'Mon, 01 Jul 2024 00:00:00 +0000',
  #              'end' => 'Mon, 15 Jul 2024 00:00:00 +0000'
  #            }
  #
  # Redis keys are expected to follow the pattern 'Watchlist:{config_key}'.
  #
  # Note:
  # - The `watchlist:{config_key}` entries are manually set in the console as needed.
  # - These configurations are expected to persist indefinitely in the cache.
  def fetch_configurations
    config_keys = %w[watchlistRegion watchlistTimeRange]
    configurations = {}
    config_keys.each do |key|
      configurations[key.to_sym] = RedisHelper.get_json("Watchlist:#{key}") rescue nil
    end
    configurations
  end

  def extract_receive_id(lark_chat)
    if lark_chat.user?
      lark_chat.open_id
    elsif lark_chat.group_chat? || lark_chat.p2p_chat?
      lark_chat.chat_id
    end
  end

  def fetch_message_id(receive_id)
    RedisHelper.get("Watchlist: receive_id #{receive_id}") rescue nil
  end

  def update_existing_message?(msg_id)
    # Lark Message Card has edit limit of 20 times
    msg_edit_times = RedisHelper.get("Watchlist: msg_id #{msg_id} edit times") rescue nil
    msg_id.present? && msg_edit_times.present? && msg_edit_times.to_i < 20
  end

  def update_message(msg_id, message_card)
    lark_app.patch_remote("#{LarkApp::BasicMessageUrl}/#{msg_id}", LarkMsg.assemble_msg(message_card))
    increment_msg_edit_times(msg_id)
  end

  def increment_msg_edit_times(msg_id)
    RedisHelper.incr("Watchlist: msg_id #{msg_id} edit times")
  end

  def send_new_message(lark_chat, message_card, receive_id)
    msg_res = if lark_chat.user?
                lark_app.send_single_msg_to_user(receive_id, message_card)
              elsif lark_chat.group_chat? || lark_chat.p2p_chat?
                lark_app.send_single_msg_to_chat(receive_id, message_card)
              end

    msg_id = msg_res&.dig("data", "message_id")

    if msg_id
      RedisHelper.cache("Watchlist: receive_id #{receive_id}", msg_id, 6.hours)
      RedisHelper.cache("Watchlist: msg_id #{msg_id} edit times", 0, 6.hours)
    end
  end

  def generate_message_content
    message_elements = generate_message_elements
    header = generate_message_header
    {
      elements: message_elements,
      header: header,
      template: "violet"
    }
  end

  def parse_time_range(time_range_config)
    start_time = DateTime.parse(time_range_config['start']) rescue nil
    end_time = DateTime.parse(time_range_config['end']) rescue nil
    start_time..end_time
  end

  def apply_configurations_to_scope(scope, configurations)
    if configurations[:watchlistTimeRange].present?
      time_range = parse_time_range(configurations[:watchlistTimeRange])
      scope = scope.where(created_at: time_range) if time_range
    end

    scope
  end

  def fetch_and_filter_workorders(scope, configurations)
    workorders = scope.includes(:house)

    if configurations[:watchlistRegion].present?
      desired_regions = configurations[:watchlistRegion]
      workorders = workorders.select do |wo|
        house_region = wo.house&.house_region
        house_region && (house_region & desired_regions).any?
      end
    end

    workorders
  end

  def generate_message_elements(limit_per_group = 20)
    configurations = fetch_configurations
    workorders_scope = ServiceSchedule.need_addressing
    workorders_scope = apply_configurations_to_scope(workorders_scope, configurations)

    workorders = fetch_and_filter_workorders(workorders_scope, configurations)

    message_elements = workorders.group_by { |wo| wo.house&.house_region&.first }.flat_map do |region, workorders|
      regional_workorders = ["Region: #{region}"] +
        [workorders.sort_by { |wo| [wo.service&.name || '', wo.notification_date_time || Time.at(0)] }
                   .first(limit_per_group)
                   .map do |wo|
          "[#{wo.service&.name}, #{wo.house&.full_address}, notified at #{wo.notification_date_time&.strftime('%m/%d/%Y %H:%M:%S')}](#{INTERNAL_NEW_PMS_SITE}/workorder/option/#{wo.id})"
        end.join("\n")] +
        ['divider']
    end

    message_elements&.pop if message_elements&.last == 'divider' # Remove trailing divider if present
    message_elements.presence || default_message_element
  end

  def default_message_element
    ["No work orders to address at this time."]
  end

  def generate_message_header
    "Workorder Watchlist (Last Updated at #{DateTime.current.strftime("%m/%d/%Y %H:%M")})"
  end

  def lark_app
    LarkApp.inner_tool
  end
end