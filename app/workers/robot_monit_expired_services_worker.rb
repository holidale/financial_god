class RobotMonitExpiredServicesWorker < RobotBase
  ServicesSheetToken = "shtcnLNwVS6zuy4GsyJpUa1gdcb"
  def perform(**args)
    list = LarkApp.inner_tool.read_sheet_values(ServicesSheetToken, "A4:I100", "7ad087")
    expired = list.dig(:data, :valueRange, :values).map do |a| 
      next if a[6].to_s.downcase == 'auto'
      d = a[2]["formattedText"].to_date rescue nil
      msg = [a[8], a.first.first["text"]].join(": ") rescue nil
      if d && ((c = Date.current - d) > -30 ) 
        expired_msg = c > 0 ? "Expired #{c.to_i} Days" : "Will Expire in #{c.to_i.abs} Days on #{d}"
        [c, "Services #{expired_msg} for #{msg}"]
      else
        nil
      end
    end.compact.sort_by(&:first).map(&:last)
    # expired = ["No Expired Services Today"] if expired.empty?
    check_and_enqueue_data(args, expired)
  end
end
