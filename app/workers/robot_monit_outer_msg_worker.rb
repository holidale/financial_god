class RobotMonitOuterMsgWorker < RobotBase
  def perform(**args)
    msg_from_outer = RedisHelper.looped_flows("FeishuMsg").presence
    check_and_enqueue_data(args, msg_from_outer)
  end
end
