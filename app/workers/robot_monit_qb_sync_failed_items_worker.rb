class RobotMonitQbSyncFailedItemsWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    if (qs = QbSyncRecord.qb_fail.recent_synced_in_minutes(args[:interval])).present?
      qs.each do |q| 
        data = {qb_sync_record_id: q.id, task_id: args[:task_id]}
        RobotTask.enqueue(data)
      end
      return {alert: true}
    end
    {alert: false}
  end
end
