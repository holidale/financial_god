class RobotMonitSqlRelatedWorker < RobotBase
  ServersFile = Rails.env.production? ? "/home/webuser/zmsun/servers" : ""

  LIMIT = 10 # 20% # disk line
  MEMO_LIMIT = 90 # 90% # memory line # Easy to high, usually 90 -> 97
  def perform(**args)
    servers = File.read(ServersFile).split("\n") rescue []
    data = servers.map do |line|
      port, server_name = line.split(" ")
      s = `ssh -p #{port} webuser@#{server_name} 'df -l'`
      _, total, used, unused, _ = s.split("\n").sort_by{|a| a.split(" ")[1].to_i}.last.to_s.split(" ")
      rate = ((unused.to_i / total.to_f) * 100).round(2)
      rate <= LIMIT ? "Server #{server_name} Low Space #{rate}%" : nil # below 20%
    end.compact

    data += servers.map do |line|
      port, server_name = line.split(" ")
      s = `ssh -p #{port} webuser@#{server_name} 'vmstat -s'`
      total, used = s.split("\n")[0,2].map(&:to_i)
      rate = ((used.to_i / total.to_f) * 100).round(2)
      rate >= MEMO_LIMIT ? "Server #{server_name} High Memory Used #{rate}%" : nil # higer than 90%
    end.compact

    check_and_enqueue_data(args, data)
  end
end
