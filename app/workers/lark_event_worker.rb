class LarkEventWorker < EventBase
  def perform(params)
    params = super(params)
    hook_callback(params[:event]) if params[:type] == "event_callback"
    hook_task_callback(params) if event_from_task?(params)
    hook_chat_callback(params) if event_from_chat?(params)
    hook_chat_member_callback(params) if event_from_chat_member?(params)
    case params[:schema]
    when "1.0"
    when "2.0"
      parse_new_event(params[:event])
      FeishuObject.parse_event(params)
    end
  end

  def event_from_task?(params)
    params.dig(:header, :event_type)&.include?("task")
  end

  def event_from_chat?(params)
    params.dig(:header, :event_type).to_s.include?("im.chat.updated_v1")
  end

  def event_from_chat_member?(params)
    params.dig(:header, :event_type).to_s.include?("im.chat.member")
  end

  def hook_task_callback(params)
    event_type = params.dig(:header, :event_type)
    Sidekiq.logger.info("Lark Task Event Type: #{event_type}")

    case event_type
    when "task.task.updated_v1"
      LarkTaskEvent.new(params).hook
    when "task.task.comment.updated_v1"
      LarkTaskCommentEvent.new(params).hook
    end
  end

  def hook_chat_callback(params)
    Sidekiq.logger.info("Lark Chat Hook: #{params}")

    LarkChatEvent.new(params).hook
  end

  def hook_chat_member_callback(params)
    LarkChatMemberEvent.new(params).hook
  end

  def hook_callback(event)
    ic = event[:instance_code]
    LarkApproval.new(event[:approval_code]).parse_approval(ic) rescue nil
    operator = FeishuObject.find_json_in_cache(event[:open_id]) || {}
    a = Approval.find_by(key: ic, status: 'approved')
    a ||= Approval.where(key: ic).last

    case event[:type]
    when 'approval_task'
      # LarkMsg.enqueue(:approval_task, "Approval##{a&.id} #{operator[:name]} #{event["status"]}")
    when 'approval_instance'
      case event[:approval_code] 
      when Approval.approval_codes['po']
        a.cost_id ||= a.form[3]["value"][/option\/\d+/].split("/").last if a.cost.nil? rescue nil
        if a&.approved? && a.approval_name.to_s !~ /测试|test/
          a.cost&.hook_lark
          a&.check_comments
        end
        cc = LarkMsg.link_to_pms(a.cost) if a.cost
        msg = "PO Approval##{a&.id} From Callback:\n #{cc}, Status: #{event["status"]}"
        LarkMsg.enqueue(:po_approvals, header: false, elements: [msg])
      else
        hook_normal_approval(event)
      end
    end
  end

  def hook_normal_approval(event)
    ac, ic = event[:approval_code], event[:instance_code]
    type = Approval.approval_codes.invert[ac]
    type == 'reimbursement' && event['status'] != "APPROVED" and return 

    a = LarkApproval.new(ac).parse_approval(ic)
    msg = a.is_a?(String) ? a : "#{type.titleize} Approvals##{a.id} from #{a.user_name}: #{a.status}"
    LarkMsg.enqueue(:"#{type}_approvals", "Got Approval #{ic} From Callback:\n #{msg}")
  end

  def parse_new_event(event)
    msg = event[:message]
    !msg.present? and return

    msg_content = JSON.parse(msg[:content]).with_indifferent_access rescue {}
    case msg[:message_type]
    when "file", "image"
      file_msg = {file_key: msg_content[:file_key] || msg_content[:image_key], 
                  file_name: msg_content[:file_name] || "ImageMsg.png", 
                  message_id: msg[:message_id], type: msg[:message_type]}
      fname, _ = LarkApp.inner_tool.monit_file_msg(file_msg)
      if event[:message][:chat_type] == "p2p" && msg[:message_type] == "image"
        read_words_from_image_and_notify(fname, event)
      end
    when "text"
      response_text(event, msg_content["text"].to_s.split(/@_user_\d+/).last&.strip)
      LarkApp.inner_tool.like_and_react_msg(msg[:message_id], request_type: 'user') if LarkMsg.need_like?(msg_content["text"])
      parent_id = event.dig(:message, :parent_id) and return hook_reply_msg(parent_id, msg_content) rescue nil
    when "audio"
      file_msg = {file_key: msg_content[:file_key] || msg_content[:image_key],
                  file_name: msg_content[:file_name] || "audio.mp3",
                  message_id: msg[:message_id], type: msg[:message_type]}
      fname, _ = LarkApp.inner_tool.monit_file_msg(file_msg)
      # Apply OPENAI Whisper API Turn voice into text
    end
  end

  def hook_reply_msg(parent_id, msg)
    msg_content = msg["text"] || "No Content"
    if fm = FeishuMessage.find_by(message_id: parent_id)
      if hdesk_key = fm.content.to_s.scan(/HDESK-\d+/).last
        CommandCaller.call_ruby("jira.rb","create_comment", hdesk_key, msg_content)
      end
    end
  end

  def response_text(event, text)
    obj = FeishuObject.find_user_with(event.dig("sender", "sender_id", "open_id"))
    chat_type = event.dig(:message, :chat_type)
    resp = MangoRobot.response(text, obj, chat_type)
    !resp.present? and return

    case chat_type
    when 'group'
        ms = event.dig(:message, :mentions) || []
        at_robot = ms.find{|a| a[:name] == "month2month"}.presence
        obj = at_robot ? FeishuObject.find_by(key: event[:message][:chat_id]) : nil
    end
    # Disable FastGPT in group and Not config chatgpt as default answer bot
    obj = nil if !obj&.request_chatgpt_when_empty? && resp.to_s =~ /\(ChatGPT\)/
    obj&.notify(resp) if !no_need_alert?(text, resp)
  end

  def no_need_alert?(text, resp)
    text.to_s =~ /^fast_quote|^fq|^fast_search|^fs/ && resp.to_s.downcase =~ /no result/
  end

  def read_words_from_image_and_notify(file_name, event)
    data = LarkApp.inner_tool.ocr(file_name) rescue nil
    words = data["data"]["text_list"].presence rescue nil
    words.nil? and return 

    Sidekiq.logger.info("Words from #{file_name} ----> #{words.to_json}")
    open_id = event[:sender][:sender_id][:open_id]
    monit_words(words, open_id)
  end

  IGNORE_WORDS = %w(holidale month2month)
  def monit_words(arr, open_id)
    useful = arr.map(&:downcase).select {|a| a =~ /[a-z]+/}.flat_map{|a|a.split(" ")}.sort_by(&:size).reverse
    clean_words = useful.select {|a| a.size > 4 }.uniq - IGNORE_WORDS
    parsed_data = monit_amounts(arr)
    clean_words.map do |a| 
      a = a.strip
      parsed_data += parse_objects_from_word(a)
      # parsed_data += parse_qas_from_word(a)
    end
    final_reply_with_all_results(arr, parsed_data, open_id)
  end

  def final_reply_with_all_results(original_data, parsed_data, open_id)
    eles = analysis_data(parsed_data)
    eles += parsed_data.map {|a| object_to_msg(a) }
    msg = eles.present? ? {header: "Possibly Data from This Image", elements: eles.uniq} : original_data.to_json
    FeishuObject.find_user_with(open_id).notify(msg)
  end

  # obj
  #   May be a User, 
  #   May be a Org, 
  #   May be a Date, 
  #   May be a BigDecimal
  def object_to_msg(obj)
    ot = obj.class.name
    ot == 'User' and return "[Guest ID##{obj.id}-#{obj.full_name}](#{PmsRecord::SITE}/users/#{obj.id})"
    ot == 'Organization' and return "[Org ID##{obj.id}-#{obj.name}](#{PmsRecord::SITE}/organizations/#{obj.id})"
    ot == "BigDecimal" and return "Amount Info: #{ApplicationRecord.to_currency(obj)}"
    %w(Date DateTime).include?(ot) and return "Date Info: #{obj.to_date.strftime("%m/%d/%Y")}"
  end

  def monit_amounts(arr)
    amounts = arr.select{|a| a =~ /^\$.*?[\,\.\d]+/}.map{|a| MangoRobot.parse_amount(a) }.compact
  end

  # Find Answers for Quotations
  def parse_qas_from_word(a)
    []
  end

  # Found More info from data
  def analysis_data(data)
    []
  end

  def parse_objects_from_word(a)
    eles = []
    eles += User.where("first_name = ? or last_name = ?", a, a)
    eles << Organization.find_by(name: a)
    if a.size == 10 && (date = MangoRobot.parse_date(a))
      eles << date
    end
    eles.compact
  end
end
