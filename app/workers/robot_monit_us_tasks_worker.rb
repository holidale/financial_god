class RobotMonitUsTasksWorker < RobotBase
  BASIC_HOUR = 7
  def perform(**args)
    PmsRecord.holiday? and return

    if Time.current.min == 0
      send_important_tasks_progress(admin: true) if Time.current.hour == BASIC_HOUR - 2 # preview
      send_important_tasks_progress if Time.current.hour == BASIC_HOUR # normal
      send_summary_tasks if Time.current.hour == BASIC_HOUR - 3
    end
  end

  def send_important_tasks_progress(admin: false)
    msg = JiraTask.important_tasks_progress
    admin and return FeishuObject.admin.notify(header: false, elements: msg)
    LarkMsg.enqueue(:important_tasks_progress, header: false, elements: msg)
  end

  def send_summary_tasks
  end
end
