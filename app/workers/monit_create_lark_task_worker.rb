class MonitCreateLarkTaskWorker
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def perform
    RedisHelper.looped_flows("CreateLarkTask").each do |data|
      handle_single_data("create", data)
    end

    RedisHelper.looped_flows("UpdateLarkTask").each do |data|
      handle_single_data("update", data)
    end
  end

  private

  def handle_single_data(type, data)
    Sidekiq.logger.info("#{type.titleize}LarkTask data: #{data}")

    data = JSON.parse(data).with_indifferent_access
    workorder = find_workorder(data)

    workorder.create_lark_task(data) if "create" == type && workorder.feishu_task.blank?
    workorder.update_lark_task(data) if "update" == type && workorder.feishu_task.present?
  end

  def find_workorder(data)
    data[:taskable_type].constantize.find(data[:taskable_id])
  end
end