class RobotMonitTopSummaryWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    data = ServiceSchedule.summary
    data.unshift("**Workorder Summary**")
    # data.push("**Lark PO Summary**")
    # data += Approval.summary
    check_and_enqueue_data(args, data)
  end
end
