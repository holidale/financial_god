class RobotMonitOwnerRevenuePdfWorker < RobotBase
  def perform(**args)
    check_and_enqueue_data(args, get_latest_pdfs)
  end

  def get_latest_pdfs
    key = "PMS_lastest_owner_revenue_pdfs"
    version_check = "ls #{PMS_DEPLOY_PATH}/data/investor_earning_report/ -l |wc"

    invork_pms_with(key, version_check) do
      tasks_command = "find #{PMS_DEPLOY_PATH}/data/investor_earning_report/ -type f -newermt '-10 minutes'"
      invork_pms(tasks_command).split("\n").presence
    end
  rescue => e
    Sidekiq.logger.info("get get_latest_pdfs error #{e.message}")
    nil
  end
end
