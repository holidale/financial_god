class RobotVendorListWorker < EventBase
  def perform(params, obj_id)
    obj = FeishuObject.find(Integer(obj_id, exception: false))
    !obj.present? and return
    region, service = parse_region(params)
    send_response(region, service, obj)
  end

  def parse_region(params)
    openai = Openai.new
    mapping = get_city_mapping
    list = ContractorCategory.all.map(&:name).uniq.compact.join(', ')
    text = "I have a contractor service coverage city and region mapping list(city => region):\n#{mapping}\n\nI also have a contractor service type list:\n#{list}\n\nNow I will give you a text, coming from a person who needs to find contractors. The text might include a location (a city or a region) and a service type. Please detect and return me, which region(key \"region\") & what service(key \"service\") does it include. Please return in the format of json. Example: {\"region\": \"Orange County\", \"service\": \"Cleaner\"} If the city is not in the map, please detect the region based on your knowledge. If you don't know what region, return \"Orange County\". Only return the region name inside the mapping. If you don't know what service type does it mean, return \"All\". Only return the service type name inside the list. Do not explain, do not comment, do not add empty lines. Respond from the first letter, do not add anything.\n\nText:\n#{params}"
    res = openai.completion_text(text)
    return nil if !res.present?
    hash = JSON.parse(res) rescue nil
    return nil if !hash.present?
    region_name = hash["region"]
    service_name = hash["service"]
    region = Region.find_by_name(region_name) rescue nil
    service = ContractorCategory.find_by_name(service_name) rescue nil
    service = nil if service_name == "All"
    return region, service
  end

  def send_response(region, service, obj)
    !region.present? and return
    notification_text = "Pulling #{region&.name}"
    notification_text += " #{service&.name}"
    notification_text += " Vendor Data..."
    obj&.notify(notification_text)
    msg = cleaner_msg_card(region, service)
    obj&.notify(msg)
    msg2 = region_history_msg_card(region, service)
    obj&.notify(msg2)
  end

  def get_city_mapping
    houses = House.active_native_listings
    houses.map do |hh|
      if hh.house_region&.first
        "#{hh.city} => #{hh.house_region&.first}"
      else
        nil
      end
    end.uniq.compact.join(', ')
  end

  def cleaner_msg_card(region, service)
    cleaners = Cleaner.by_coverage_region(region.id).active_cleaners
    cleaners = cleaners.by_contractor_category(service.id) if service.present?
    msg = cleaners.map do |c|
      text = "#{c.average_review}★ #{c.full_name}"
      text += "(#{c.contractor_categories.map(&:name).join(", ")})" if c.profession.present?
      text += " #{c.phone}"
      text += " ,#{c.email}" if c.email.present?
      text
      # #{c.contractor_categories.map(&:name).join(", ")}
    end.uniq.compact
    header = "#{region.name}#{service.present? ? (" " + service&.name) : ""} Vendor List"
    {
      header: header, template: 'Orange',
      elements: msg
    }
  end

  def region_history_msg_card(region, service)
    houses = House.house_for_region(region, purge: false)
    if service.present?
      sid = service.services.ids
      wo = ServiceSchedule.where(house_id: houses.map(&:id)).where(service_id: sid).has_cleaner.order(created_at: :desc).limit(20)
    else
      wo = ServiceSchedule.where(house_id: houses.map(&:id)).has_cleaner.order(created_at: :desc).limit(20)
    end
    msg = wo.map do |w|
      "#{w.created_at.strftime("%m/%d/%Y")} (#{w.service.name}) #{w.house.address2} - #{w.cleaner.full_name}"
    end.uniq.compact
    msg << "Try 'cl#' cmd to get the vendor information, for example cl#rosalia"
    {
      header: "#{region.name}#{service.present? ? (" " + service&.name) : ""} Work Order History", template: 'Green',
      elements: msg
    }
  end

end
