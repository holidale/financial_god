class DialpadRecordWorker < RobotBase
  def perform(**args)
    res = DialpadService.stats
    return if res.blank? || res["request_id"].blank?
    res = DialpadService.get_stats_result(res["request_id"])

    if res["status"] != "completed"
      sleep(10)
      res = DialpadService.get_stats_result(res["request_id"])
    end

    return if res["status"] != "completed" || res["download_url"].blank?
    DialpadRecord.parse_remote_csv_and_save(res["download_url"])
  end

end
