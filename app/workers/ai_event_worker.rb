class AiEventWorker < EventBase
  def perform(params)
    # Todo Timeout processing
    params = super(params)
    case params[:schema]
    when "1.0"
    when "2.0"
      parse_ai_event(params[:event])
      FeishuObject.parse_event(params)
    end
  end

  def parse_ai_event(event)
    msg = event[:message]
    !msg.present? and return

    msg_content = JSON.parse(msg[:content])&.with_indifferent_access rescue {}
    case msg[:message_type]
    when "text"
      response_text(event, msg_content["text"].to_s)
    when "file", "image"
      # File not supported
    when "audio"
      file_msg = {file_key: msg_content[:file_key],
                  file_name: msg_content[:file_name] || "audio.mp3",
                  message_id: msg[:message_id], type: msg[:message_type]}
      # fname = AiApp.ai_tool.monit_file_msg(file_msg)
      # id = event.dig("sender", "sender_id", "open_id")
      # send_ai_message(id, fname)
      # # Apply OPENAI Whisper API Turn voice into text
    end
  end

  def response_text(event, text)
    id = event.dig("sender", "sender_id", "open_id")
    !id.present? and return
    Sidekiq.logger.info("AiEventWorker response_text Text: #{text}")
    resp = ByteBrain.response(text)
    !resp.present? and return
    response_handler(id, resp)
  end

  def ai_app
    @ai_app ||= AiApp.ai_tool
  end

  def send_ai_message(id, msg)
    !msg.present? and return

    msg_res = ai_app.send_single_msg_to_user(id, msg)
    msg_id = msg_res.dig("data", "message_id")
    FeishuMessage.hook_msg("AiApp", self, msg, msg_id) rescue nil
  end

  def response_handler(id, resp)
    if resp.is_a?(Array)
      resp.each do |line|
        send_ai_message(id, line)
      end
    else
      send_ai_message(id, resp)
    end
  end

end
