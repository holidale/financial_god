class EventBase
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def perform(params)
    params = JSON.parse(params) if params.is_a?(String)
    params = params.with_indifferent_access
    Sidekiq.logger.info "#" * 80
    # params is too big
    if !%w(BitbucketWorker JiraIssuesWorker).include?(self.class.name)
      Sidekiq.logger.info params.to_json
    end
    params
  end
end
