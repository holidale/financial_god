class RobotMonitSalesforceDataWorker < RobotBase
  def perform(**args)
    s = CommandCaller.call_python("salesforce.py", "get_booking_revenues")
    # "a028c00000fcQD6AAM,501198,33293.75
    # or
    # "a028c00000fcQD6AAM,501198
    data = []
    s.split("\n").each do |line|
      housing_request_id, booking_id = line.split(",")
      UpdateHousingRequestRevenueWorker.perform_async(booking_id, housing_request_id)
      data << "Monit None Revenue HousingRequest: #{housing_request_id},#{booking_id}"
    end
    check_and_enqueue_data(args, data)
  end
end
