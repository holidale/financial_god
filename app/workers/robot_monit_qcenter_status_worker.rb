class RobotMonitQcenterStatusWorker < RobotBase
  def perform(**args)
    res = rescue_remote_request("Quotation Center") do
      demo = {"check_in": "2025-06-06", "check_out": "2025-06-16", "house_id": "194"}
      Faraday.post("#{PublicConfigs.end_points.quotation}/rpc/quotations/api_json", demo)
    end
    res.is_a?(Hash) and return res
    res.status != 200 and return {alert: true, data: ["Quotation Center is Down, status is #{status}"]}

    data = JSON.parse(res.body) rescue {}
    data["success"] and return {alert: false}
    {alert: true, data: ["Response Data Wrong from Quotation Center, body is #{data}"]}
  end
end
