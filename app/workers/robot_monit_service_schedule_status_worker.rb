class RobotMonitServiceScheduleStatusWorker < RobotBase
  def perform(**args)
    ServiceSchedule.recent_updated_in_minutes(args[:interval]).map do |s|
      begin
        s.parse_track_times
      rescue => e
        LarkMsg.enqueue(:monit_service_schedule_status_fail, "WO ID##{s.id}, Monit Audit Excepstion #{e.message}")
      end
    end
  end
end
