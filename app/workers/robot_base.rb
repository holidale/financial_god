class RobotBase
  include Sidekiq::Worker
  sidekiq_options retry: false

  def formatted_args(args)
    Sidekiq.logger.info("Args from worker #{self.class.name}, #{args}")
    args.with_indifferent_access
  end

  def perform(**args)
    # do things for sub classes
  end

  def invork_pms(tasks_command)
    `ssh -p 20202 m2m "#{tasks_command}"`
  end

  def invork_pms_with(key, version_check, &block)
    new_version = invork_pms(version_check)
    last_version = RedisHelper.get(key)
    last_version == new_version and return
    RedisHelper.cache!(key, new_version)
    block.call
  end

  def check_and_enqueue_data(args, data = [])
    args = formatted_args(args)
    data = JSON.parse(data) if data.is_a?(String) rescue data
    if data.present?
      data = data.compact if data.is_a?(Array)
      _data = {task_id: args[:task_id], data: data}
      RobotTask.enqueue(_data)
      return {alert: true}
    end
    {alert: false}
  end

  def rescue_remote_request(service_name, &block)
    Timeout.timeout(30) do 
      begin
        block.call
      rescue Faraday::ConnectionFailed => e
        return {alert: true, data: ["#{service_name} is Totally Down #{e.message}"]}
      end
    end
  rescue => e
    {alert: true, data: ["#{service_name} Error: #{e.message}"]}
  end
end
