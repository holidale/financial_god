class JiraIssuesWorker < EventBase
  def perform(params)
    params = super
    timestamp = params[:timestamp]
    user = params.dig(:user, :displayName)
    issue = params[:issue]
    changelogs = params.dig(:changelog, :items) || []
    !issue.present? and return

    key = issue[:key]
    fields = issue[:fields] || {}
    it = fields[:issuetype][:name]
    task_link = Jira.link(key, fields[:summary])
    monit_test_case(issue) if it == 'TestCase'

    if "comment_created" == params[:webhookEvent]
      if params.dig("issue","key").to_s =~ /^HDESK/
        comment = CommandCaller.get_comment(params["comment"]["self"])
        if comment_body = comment["body"].presence rescue nil
          _title = Jira.link(params.dig("issue", "key"), params.dig("issue", "fields", "summary"))
          if issue = CommandCaller.get_issue(params.dig("issue", "key")).presence
            if submitter = issue.dig("fields")["customfield_10911"].first
              msgs = [_title, comment_body]
              FeishuObject.find_user_with(submitter)&.notify(header: "New Help Desk Updates", elements: msgs)
            end
          end
        end
      end
    end

    task_link =~ /Test|MARKET|ADMIN|LISTING|OPERA|DESK/ and return # Black List
    task_link !~ /HWA|SPRK|HM|FIN/ and return  # White List
    it.in? %w(TestCase Sub-task Story) and return

    enqueue_task(issue)

    case params[:webhookEvent]
    when "jira:issue_created"
      _card_msg(:jira_issue_create, "Task New(#{it}): #{task_link}") # if it != "Sub-task"
    when "jira:issue_updated"
      changelogs.each do |item|
        if item[:field] = "Status"
          test_cases_number = (fields[:inwardIssue] || []).select{|a| a[:key].to_s =~ /^TC-/ }.size
          if item[:toString].to_s == "Waiting for test"
            _card_msg(:jira_issue_to_test, "Ready to Test: #{task_link}")
          elsif item[:toString].to_s.downcase =~ /code review/
            _card_msg(:jira_issue_to_pr, "Ready to PR: #{task_link}")
          end
        end
      end
    end
  end

  def _card_msg(type, msg)
    msg = msg.is_a?(Array) ? msg : [msg]
    LarkMsg.enqueue(type, {header: false, elements: msg})
  end

  def monit_test_case(issue)
    status = issue.dig(:fields, :status, :name)
    links = issue.dig(:fields, :issuelinks)&.map{|a| a.dig(:outwardIssue, :key)}&.compact || []
    links += issue.dig(:fields, :issuelinks)&.map{|a| a.dig(:inwardIssue, :key)}&.compact || []
    links.each do |task|
      if jt = JiraTask.find_by(key: task)
        if status == 'Failed'
          data = ["Test Case Failed: ", Jira.link_issue(issue)]
          aname = issue.dig(:fields, :assignee, :displayName) || jt.assignee_name
          FeishuObject.find_user_with(aname).notify(header: false, elements: data) rescue nil
        elsif status == "DevFixed"
          data = ["Test Case Fixed: ", Jira.link_issue(issue)]
          FeishuObject.find_user_with("dan").notify(header: false, elements: data) rescue nil
        end
        enqueue_task(CommandCaller.get_issue(jt.key))
        _card_msg(:test_cases_status_for_task, jt.msg_for_cases) if jt.reload.cases_status_summary
      end
    end
  end

  def enqueue_task(issue)
    # issue["key"].to_s =~ /HWA/ && issue["key"][/\d+/].to_i < 6000 and return
    JiraTask.check_task(issue) if issue.present? rescue nil
  end
end
