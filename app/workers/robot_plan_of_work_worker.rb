class RobotPlanOfWorkWorker < EventBase

  def perform(params, obj_id)
    obj = FeishuObject.find(Integer(obj_id, exception: false))
    !obj.present? and return
    type, place = parse_region(params)
    send_response(type, place, obj)
  end

  def parse_region(params)
    openai = Openai.new
    mapping = home_address_mapping
    text = "I have a list of house id, house address and related region (id|address|region):\n#{mapping}\n\nNow I will give you a text, which might include a home address or a city or a region name. Please detect and return me, if this text is related to a specific address or just a general region (key: \"type\", value: \"address\" or \"region\"), and what is the place (key: \"place\", value: the id of the house or region name(please map the city to the region)). Please find the most closest specific address, the priority of the street name is higher than the street number. Direction like \"W\" or \"N\" (west, north) can be ignore. Please return in the format of json. \nExample1: {\"type\": \"address\", \"place\": \"41339\"}\nExample2: {\"type\": \"region\", \"place\": \"Orange County\"}\nIf you don't know what region, return \"region\" and \"Orange County\". Only return the address id or the region name inside the mapping. Do not explain, do not comment, do not add empty lines. Respond from the first letter, do not add anything.\n\nText:\n#{params}"
    res = openai.completion_text(text)
    return nil if !res.present?
    hash = JSON.parse(res) rescue nil
    return nil if !hash.present?
    type = hash["type"] rescue nil
    place = hash["place"] rescue nil
    return type, place
  end

  def send_response(type, place, obj)
    (type.nil? || place.nil? || obj.nil?) and return
    msg = []
    case type
    when "address"
      house = House.find(place) rescue nil
      return if house.nil?
      wos = ServiceSchedule.not_finished.where(house_id: house.id).where("due >= ? AND due <= ?", Date.current, Date.current + 2.weeks).order("due")
      msg = plan_of_work_msg_card(house.address2, wos)
    when "region"
      region = Region.find_by_name(place) rescue nil
      return if region.nil?
      houses = House.house_for_region(region, purge: false)
      wos = ServiceSchedule.not_finished.where(house_id: houses.map(&:id)).where("due >= ? AND due <= ?", Date.current, Date.current + 2.weeks).order("due")
      msg = plan_of_work_msg_card(region.name, wos)
    end
    obj&.notify(msg)
  end

  def plan_of_work_msg_card(place_name, wos)
    return nil if wos.nil?
    msg = wos.map do |wo|
      "#{wo.due.strftime("%Y-%m-%d")}: #{wo.service&.name}, #{wo.house.full_address_us_format}"
    end.uniq.compact
    header = "#{place_name} Plan of Work for the Next Two Weeks"
    {
      header: header, template: 'Green',
      elements: msg
    }
  end

  def home_address_mapping
    houses = House.active_native_listings
    houses.map do |hh|
      if hh.house_region&.first
        "#{hh.id}|#{hh.full_address_us_format}|#{hh.house_region&.first}"
      else
        nil
      end
    end.uniq.compact.join(', ')
  end
end

