class RobotMonitApprovalsWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    start = args[:interval].to_i.minutes.ago
    data = LarkApproval.reimbursement.parse_to_pms(start).map do |a|
      a.is_a?(String) ? a : a.msg_body
    end
    check_and_enqueue_data(args, data)
  end
end
