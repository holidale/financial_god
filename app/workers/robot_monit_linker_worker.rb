class RobotMonitLinkerWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    CallLog.recent_updated_in_minutes(args[:interval]).map do |c|
      LarkMsg.h_msg(:linker_call_log, s.lark_msg)
    end
    ShortMessageLog.recent_updated_in_minutes(args[:interval]).map do |s|
      LarkMsg.h_msg(:linker_sms_log, s.lark_msg)
    end
    # check_and_enqueue_data(args, data.compact)
  end
end
