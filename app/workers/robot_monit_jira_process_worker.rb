class RobotMonitJiraProcessWorker < RobotBase
  def perform(**args)
    unfinished_status = ["Open", "In Development", "To Do"]
    data = []

    test_tasks = JiraTask.where(status: ["Waiting for test"])
    data += category_tasks(test_tasks, "Issues In Test", "test_due")

    overdue_tasks = JiraTask.where("due_date <= ? ", Date.current).where(status: unfinished_status)
    data += category_tasks(overdue_tasks, "Overdue Tasks", "due_date")

    prs_tasks = JiraTask.where(status: ["PR Code Review"])
    data += category_tasks(prs_tasks, "Pull Requests to Submit/Merge")

    # data += month_summary 
    check_and_enqueue_data(args, data)
  end

  def category_tasks(tasks, title, due = nil)
    data = []
    data << (tasks.present? ? "**#{title}**" : "**#{title}(No)**")
    tasks.each do |a| 
      due_desc = due ? " (Due: #{a.send(due)&.to_date || '待评估'})" : ""
      data << "【#{a.short_assignee}】#{a.link} #{due_desc}"
    end
    data << "divider"
    data
  end

  def month_summary(tasks = [])
    arr = CommandCaller.call_ruby("jira.rb", "majors")
    brr = arr.select{|a| (a["sprint"] + a["labels"]).any?{|b| b =~ /#{Date.current.to_s[0,7]}/}}
    data = ["** Major Tasks Progress in #{Date.current.strftime("%Y-%m")}**"]
    data + brr.map do |a| 
      s = JiraTask::ChineseStatus[a['status']] || a['status']
      due = a['testdue'] || a['duedate'] || '待评估'
      ["【#{s}】", Jira.link_issue(a), "(#{due})"]# .join(" ") 
    end.sort_by(&:last).map{|a| a.join(" ")}
  end

  def notify_developers_unfinished_tasks(sprint, tasks)
    tasks.group_by{|a| a["developer"]}.each do |developer, ts|
      eles = ts.map{|a| Jira.link(a['key'], a['title'])}
      if u = FeishuObject.find_user_with(developer)
        u.notify(header: "Todos in sprint #{sprint}", elements: eles)
      end
    end
  end
end
