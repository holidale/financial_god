class RobotMonitApisWorker < RobotBase
  MonitedApiServices = {
    "auth_from_spark" => "https://internal.month2month.com/auth_from_spark/1",
  }

  def perform(**args)
    monit_res = MonitedApiServices.map do |s, u|
      res = Faraday.get(u)
      monit_response(s, res)
    end.compact
    monit_res.present? and return {alert: true, data: monit_res}

    {alert: false}
  end
  
  def monit_response(s, res)
    case s
    when 'auth_from_spark'
      res.body != 'forbidden' and return "API #{s} response error #{res.body}"
    end
    nil
  end
end
