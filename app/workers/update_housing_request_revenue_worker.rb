class UpdateHousingRequestRevenueWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(booking_id, hid = nil)
    if hid ||= CommandCaller.call_python("salesforce.py", "get_housing_request_by_booking_id", booking_id).strip.presence
      if b = Booking.find_by(id: booking_id)
        revenue = b.total_shareable_revenue
        CommandCaller.call_python("salesforce.py", "update_booking_revenue", hid, revenue) if revenue.to_f > 0
        LarkMsg.enqueue(:salesforce_hq_update, header: false, elements: [
          "HousingRequest Revenue Updated: #{hid},#{booking_id},#{revenue}"
        ])
      end
    end
  end
end

