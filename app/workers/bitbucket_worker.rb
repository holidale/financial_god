class BitbucketWorker < EventBase
  def perform(params)
    params = super
    arr = ["Proj: #{params.dig(:repository, :name)}"]
    arr << "User: #{params.dig(:actor, :display_name)}"
    if pr = params[:pullrequest].presence
      Sidekiq.logger.info("Monit Pull Request: #{pr[:state]}")
      arr.unshift("Pull Request: #{pr[:state]}")
      arr << "Title: #{pr[:title]}"
      msg = arr.join("\n")
      LarkMsg.enqueue(:bitbucket_events, msg)
    end

    if params[:push].presence
      params.dig(:push, :changes)&.map do |change|
        old, new = change[:old], change[:new]
        if new && new[:type] == "branch" 
          target = new[:target]
          if target[:message].to_s !~ /^Merged in/
            arr << "Branch: #{new.dig(:name)}"
            arr << "Message: #{target[:message]}"
            msg = arr.join("\n")
            LarkMsg.enqueue(:bitbucket_events, msg)
          end
        end
      end
    end
  end
end
