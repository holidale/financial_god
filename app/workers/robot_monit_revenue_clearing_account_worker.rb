class RobotMonitRevenueClearingAccountWorker < RobotBase
  def perform(**args)
    rt = RobotTask.rca
    a = User.revenue_clearing_account_balance
    if a.to_f == 0 
      rt.fresh_interval(10)
      RedisHelper.looped_flows("DELETED_QB_entities")
      return {alert: false}
    end
    rt.fresh_interval(rt.interval.to_i * 2)
    b = Time.zone.at RedisHelper.get("clearing_account_monitor_last_ok").to_i
    c = User.qb_user.get_entiries_updated_from_last_rca_ok
    # c.to_s.split(",").size > 10 and return {alert: false}  # Big possible syncing
    {alert: true, data: [a,b,c]}
  end
end
