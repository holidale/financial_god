class RobotMonitSalesDataWorker < RobotBase
  def perform(**args)
    key = "#{Date.current.beginning_of_month}_revpar_house_list_ids"
    houses = House.where(id: RedisHelper.get_json(key).presence || [])
    data = []
    data += Booking.where(id: args[:bs]).map(&:data_for_robot_msg_content) if args[:bs].present?
    data += ContractAmendment.where(id: args[:bs]).map(&:data_for_robot_msg_content) if args[:cs].present?

    check_and_enqueue_data(args, data)
  end
end
