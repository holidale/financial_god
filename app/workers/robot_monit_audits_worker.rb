class RobotMonitAuditsWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    audits = Audit.includes(:user).recent_created_in_minutes(args[:interval].to_i)
    monit_audits(audits)
    danger_audits = audits.select(&:is_danger?).map(&:danger_msg)

    danger_audits += other_db_data
    check_and_enqueue_data(args, danger_audits)
  end

  def other_db_data
    Feedback.recent_created_in_minutes(args[:interval].to_i).map(&:lark_msg)
  end

  def monit_audits(audits)
    audits.map(&:monit!)
  rescue => e
    LarkMsg.enqueue(:monit_audits_fail, "Monit Audit Excepstion #{e.message}")
  end
end
