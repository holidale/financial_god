class RobotMonitNewInvoicesWorker < RobotBase
  def perform(**args)
    args = formatted_args(args)
    if (is = Invoice.recent_created_in_minutes(args[:interval])).present?
      is.group_by(&:booking_id).uniq.each do |b, _is|
        if Booking.find_by(id: b)&.status == 'booked'
          data = {booking_id: b, invoice_ids: _is.map(&:id).join(","), task_id: args[:task_id]}
          RobotTask.enqueue(data)
        end
      end
      return {alert: true}
    end
    {alert: false}
  end
end
