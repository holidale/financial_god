class MonitLarkTaskStatusWorker
  include Sidekiq::Worker
  sidekiq_options retry: 1

  # keep this worker, currently not used
  # lark task completed event sometimes not push to our system, so add monit worker to update workorder status
  def perform
    FeishuTask.find_each do |task|
      next if task.workorder.blank? || task.workorder.completed?

      task.push_completed_event_to_pms
    end
  end
end