class RobotForGetBookedBookingsWorker < RobotBase
  MODULES_NEED_MONIT = %w(Booking ContractAmendment HostContract)

  def perform(**args)
    args = formatted_args(args)

    bids = []
    bs = Booking.recent_booked_in_minutes(args[:interval]).to_a.select(&:can_broadcast?)
    bids += bs.map(&:id)
    data = bs.map{|b| {booking_id: b.id} }
    reasons = bs.map{|b| b.create_msg }

    cs = ContractAmendment.recent_updated_in_minutes(args[:interval]).to_a.select(&:can_broadcast?)
    bids += cs.map(&:booking_id)
    data += cs.map{|c| {contract_amendment_id: c.id}}
    reasons += cs.map{|c| c.create_msg }

    audits = Audit.where(auditable_type: MODULES_NEED_MONIT).recent_created_in_minutes(args[:interval].to_i)
    reasons += audits.map {|audit| monit_audit(audit) }
    reasons.compact!
    Sidekiq.logger.info("Revpar Changed Reason-> #{reasons}")
    reasons.map{|r| RedisHelper.lpush("revpar_changed", r) } if reasons.present?

    bids.uniq.map{|bid| UpdateHousingRequestRevenueWorker.perform_async(bid) }
    check_and_enqueue_data(args, data)
  end

  def monit_audit(audit)
    _type = audit.auditable_type.tableize.singularize
    return __send__("monit_#{_type}", audit) if audit.auditable_type.in?(MODULES_NEED_MONIT)
  rescue => e
    LarkMsg.enqueue(:monit_audits_fail, "Monit Revpar Changed Audit #{e.message}")
  end

  def monit_booking(audit)
    _changes = audit.audited_changes
    factors = []
    case audit.action
    when 'update'
      %w(departure_date arrival_date status check_in check_out).each do |a|
        next unless (cs = _changes[a]).is_a?(Array)

        case a
        when "status"
          next unless (%w(booked owner_returned out_of_order).include? cs[0])
        when "check_in", "check_out"
          next unless (Booking.long_time_blocked_status.include? audit.auditable.status)
        end

        factors << "#{ a.titleize } Changed from #{cs[0]} to #{cs[1]}"
      end
    when 'create'
      if (%w(owner_returned out_of_order).include? _changes['status']) && overlap_with_current_month?(audit.auditable.real_check_in_date, audit.auditable.real_check_out_date)
          factors << "#{ _changes['status'].titleize } from #{audit.auditable.real_check_in_date.strftime("%m/%d/%Y")} to #{audit.auditable.real_check_out_date.strftime("%m/%d/%Y")}"
      end
    end

    return "Booking ##{audit.auditable.id} #{factors.join(',')},#{audit.auditable.house.desc_in_revpar}" if factors.present?
  rescue => e
    LarkMsg.enqueue(:monit_audits_fail, "Monit Booking Revpar Changed Audit #{e.message}")
  end

  def monit_contract_amendment(audit)
    _changes = audit.audited_changes
    return unless audit.action == 'update'
    return unless (cs = _changes["status"]).is_a?(Array)

    # monit voided extension
    if (audit.auditable.extra[:lark_broadcast].present? rescue nil) && cs.last == 'voided'
      return "Extension Deleted for Booking ##{audit.auditable.booking_id}"
    end

    return unless cs[0] == 'active'

    return "Extension Status Changed from #{cs[0]} to #{cs[1]} For Booking ##{audit.auditable.booking.id},#{audit.auditable.booking.house.desc_in_revpar}"
  rescue => e
    LarkMsg.enqueue(:monit_audits_fail, "Monit Extension Revpar Changed Audit #{e.message}")
  end

  def monit_host_contract(audit)
    reason = audit.monit! and return reason
  end

  def overlap_with_current_month?(s, e)
    true
  #   return false if (s.blank? || e.blank?)
  #   dates = [s.to_date, e.to_date].sort
  #   ((dates[0]..dates[1]).to_a & (Date.current.beginning_of_month..Date.current.end_of_month).to_a).present?
  # rescue
  #   false
  end
end
