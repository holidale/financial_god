class PaymentReceivedPayment < PmsRecord
  self.table_name = "payments_received_payments"
  belongs_to :invoice, foreign_key: :payment_id
  belongs_to :received_payment

  def paid_on
    received_payment&.received_date
  end
end
