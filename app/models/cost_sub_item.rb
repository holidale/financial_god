class CostSubItem < PmsRecord
  belongs_to :cost
  # audited associated_with: :cost
  # has_associated_audits
  belongs_to :cost_item
  belongs_to :cost_sub_item
end
