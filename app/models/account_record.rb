class AccountRecord < ApplicationRecord
  include Dateable
  include AccountingCalculable
  include ExtraObjectable

  has_many :deposits

  enum data_source: %w(PMS QB)
  enum company: %w(holidale greenland)
  # enum status: Invoice::PMS_STATUS

  scope :for_company, ->(company = "holidale"){where(company: company)}
  scope :for_org, ->(org){
    rt = "Organization"
    rt = "QbCustomer" if org.id == 0
    where(responser_type: rt, responser_id: org.id)
  }
  scope :individuals, -> {where(responser_type: "User")}
  scope :for_individual, ->(user){individuals.where(responser_id: user.id)}
  scope :for_data_source, ->(data_source = "PMS"){where(data_source: data_source)}
  scope :for_finance_type, ->(finance_type = "Invoice"){where(finance_type: finance_type)}
  scope :invoices, ->{for_finance_type.where(record_source_type: "Payment")}
  scope :unpaid, ->{where("received_amount < initial_amount")}
  scope :unpaid_from_invoice, ->{where(status: Invoice.unpaid_status)}

  # dateable_columns :finance_ensure_at
  BasicSearchColumns = %w(period data_source company)

  def amount
    (initial_amount * rate_for_initial_amount).round(2)
  end

  def unpaid_all?
    amount > received_amount
  end

  def invoice?
    finance_type == "Invoice" 
  end

  def invoice_id
    !invoice? and return

    record_source_type == "Payment" and return record_source_id
    if record_source_type == "QbObject"
      extra[:qb_local_value].presence || QbObject.find_by(id: record_source_id)&.local_value
    end
  end

  def invoice
    Invoice.find_by(id: invoice_id)
  end

  def self.sync_status
    where(status: nil).each do |a|
      puts "Sync Status: #{a.invoice_id}"
      a.update_columns(status: a.invoice&.status) if a.invoice?
    end
  end
end
