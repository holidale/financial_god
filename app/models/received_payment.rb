class ReceivedPayment < PmsRecord
  belongs_to :booking

  has_many :payment_received_payments
  has_many :invoices, through: :payment_received_payments, foreign_key: :payment_id

  has_many :credit_memo_received_payments
  has_many :credit_memos, through: :credit_memo_received_payments
end
