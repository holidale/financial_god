class Note < PmsRecord
  belongs_to :notable, polymorphic: true
  belongs_to :creator, class_name: :User, foreign_key: :creator_id, optional: true
  has_many :scanned_files, as: :relatable, dependent: :destroy

  VALID_SHOW_TO_CATEGORIES = %w(guest owner all)

  scope :show_to_guest, -> {
    where(show_to: %w(guest all)).order('created_at DESC')
  }

  scope :show_to_owner, -> {
    where(show_to: %w(owner all)).order('created_at DESC')
  }

  scope :show_to_all, -> {
    where(show_to: "all").order('created_at DESC')
  }

  def show_to_guest?
    %w(guest all).include? self.show_to
  end

  def show_to_owner?
    %w(owner all).include? self.show_to
  end

  def show_to_all?
    self.show_to == "all"
  end
end
