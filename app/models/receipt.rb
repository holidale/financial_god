class Receipt < PmsRecord
  belongs_to :invoice, foreign_key: :payment_id
  has_many :booking_items, dependent: :destroy

  STATUS = %w(pending paid)
  enum category: { uncategorized: 0, item: 1, date_change: 2, booking: 3 }

  validates :old_check_in, :old_check_out, :new_check_in, :new_check_out, presence: true, if: :record_date?
  validates :category, :payment_id, presence: true

  scope :receipts_for_date_change, -> { where(category: 2) }
  scope :receipts_for_booking, -> { where(category: 3) }
  scope :receipts_for_item, -> { where(category: 1) }
  scope :receipts_for_payment_item, -> { joins(booking_items: :item).distinct.where("receipts.category = ? AND items.category = ?", 1, 0) }
  scope :receipts_for_product_item, -> { joins(booking_items: :item).distinct.where("receipts.category = ? AND items.category = ?", 1, 1) }
  scope :receipts_for_deposit_item, -> { joins(booking_items: :item).distinct.where("receipts.category = ? AND items.category = ?", 1, 2) }

  def check_out_booking_items (ids)
    if self.item?
      BookingItem.where(id: ids).each { |n| n.update_attributes(receipt_id: self.id, checked_out: true) }
    end
  end

  def description
    booking_items.try(:first).try(:name)
  end

  def value
    booking_items.try(:first).try(:value)
  end

  private
    def record_date?
      ["date_change", "booking"].include? self.category
    end
end
