class Payment < PmsRecord
  self.table_name = "payments"

  def principal_amount
    self.amount - (self.refunded_amount || 0)
  end
end
