class RobotTask < ApplicationRecord
  include RobotMsgHandler
  has_and_belongs_to_many :robots, join_table: :robots_robots_tasks
  has_and_belongs_to_many :feishu_objects, join_table: :feishu_objects_robot_tasks
  has_many :robot_task_records
  scope :current_need_run, -> {where(interval: 1)}
  scope :worker, -> (w) {where(worker: worker)}

  def current_need_run?
    interval.to_i == 1
  end

  def can_run_at?(time = Time.current)
    interval.to_i == 0 and return run_at && (run_at.strftime("%H:%M") == time.strftime("%H:%M"))
    current_need_run? || (time.to_i / 60) % interval.to_i == 0
  end

  def interval
    # only use the latest deployed notes
    super || 10
  end

  def robot_params
    {task_id: id, interval: interval}
  end

  def run!
    (robots + feishu_objects).empty? and return

    res = worker.constantize.new.perform(**robot_params).with_indifferent_access
    !res[:alert] and return
    !res[:data].present? and return loop_task_flows
    notify_robots res[:skip_template] ? res : parse_results(res[:data])
  rescue => e
    Rails.logger.info("Run Task: #{id} error #{e.message}")
  end

  def loop_task_flows
    while task_body = RedisHelper.rpop_json("RobotTask:#{id}").presence
      handle_worker_msg(task_body)
    end
  end

  def need_skip?(msg)
    RedisHelper.smembers("LarkRedundantMsg").any? {|a| msg.to_s =~ /#{a}/ }
  end

  def notify_robots(msg, notify_objects = nil)
    need_skip?(msg) and return
    notify_objects = robots + feishu_objects if notify_objects.nil?
    notify_objects.map {|a| a.notify(msg)}
    Sidekiq.logger.info("Robot Msg -> " + "*" * 30)
    Sidekiq.logger.info(msg)
  end

  def parse_results(res)
    _res = res.dup
    return parse_hash_results(res) if template.to_s !~ /\#\{\}/
    template.to_s.gsub(/\#\{\}/){|a| _res.shift }
  rescue => e
    res.join("\n")
  end

  def parse_hash_results(res)
    template.to_s.gsub(/\#{.*?}/) do |a| 
      sub_data = res[a.gsub(/[\{\}\#]+/, "")]
      if sub_data.is_a?(Array)
        content = []
        sub_data.each_with_index do |desc, i|
          content << "  #{i + 1}. #{desc}"
        end
        content.join("\n  ")
      else
        sub_data
      end
    end
  end

  def fresh_interval(i)
    update(interval: i)
  end

  class << self
    def enqueue(task_body)
      RedisHelper.lpush("RobotTask:#{task_body[:task_id]}", task_body)
    end

    def select_values(tag, obj)
      {worker: workers}
    end

    def workers
      `ls #{Rails.root}/app/workers |grep worker|grep robot`.split("\n").map do |a| 
        a.split(".rb").first.titleize.gsub(" ", "")
      end
    end

    def check_new_worker
      workers.map do |worker|
        rt = RobotTask.where(worker: worker).first_or_create
        rt.name ||= worker
        rt.interval = 1 if rt.new_record?
        rt.save
        if admin = FeishuObject.find_user_with("zhimeng")
          admin.tasks = (admin.tasks + [rt]).uniq
        end
        worker
      end
    end

    def current
      current_need_run.to_a + where("robot_tasks.interval > 0").to_a.select do |a| 
        a.can_run_at?(Time.current)
      end
    end

    def rca
      @rca ||= find_by(worker: 'RobotMonitRevenueClearingAccountWorker')
    end
  end
end
