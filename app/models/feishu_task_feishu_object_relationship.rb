class FeishuTaskFeishuObjectRelationship < ApplicationRecord
  belongs_to :feishu_task
  belongs_to :feishu_object
end
