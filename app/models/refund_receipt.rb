class RefundReceipt < PmsRecord
  belongs_to :booking
  belongs_to :invoice, foreign_key: :payment_id
  has_many :deduct_refunds, dependent: :destroy
  has_one :credit_memo, dependent: :destroy
end
