# Note:
#   Before 2023 One Approval One CostItem (v1)
#   Currently One Approval One Cost
class Approval < ApplicationRecord
  include ExtraObjectable

  # belongs_to :user, foreign_key: :pms_user_id
  # belongs_to :cost_item
  serialize :form, Array
  serialize :timeline, Array
  serialize :task_list, Array
  before_save :parse_data

  enum approval_code: {
    reimbursement: "9CC59E65-41ED-47F5-8C7F-BEFC585DDBE2",
    feature_request: "15AAB973-A786-4B60-88DA-A7CA50B8AF03",
    po_test: "468885A7-2BAF-44FD-89BF-1C0FA6992CA0",
    po: "8604465C-C203-4DEE-A89E-9281C715F5FA",
    help_desk: "D93C1C68-FDD1-455F-A52A-BDFB5BEDC2C9"
  }

  %w(pending rejected approved).each do |s|
    scope s, -> {where(status: s.upcase)}
  end

  def self.po_taking_effect?
    Date.current >= APPROVAL_V2_START
  end

  def cost_item
    @cost_item ||= CostItem.find_by(id: cost_item_id)
  end

  def cost
    @cost ||= Cost.find_by(id: cost_id)
  end

  def user
    @user ||= User.find_by(id: pms_user_id)
  end

  def feishu_user
    @feishu_user ||= FeishuObject.find_user_with(open_id)
  end

  def approved?
    status == 'APPROVED'
  end

  def rejected?
    status == "REJECTED"
  end

  def cancelled?
    status == "CANCELED"
  end

  def user_name
    user&.full_name || feishu_user&.name
  end

  def start_at
    Time.zone.at(start_time.to_i / 1000) rescue start_time
  end

  def end_at
    Time.zone.at(end_time.to_i / 1000) rescue end_time
  end

  def last_operator_open_id
    (timeline.select{|a| a["open_id"].presence}.last || {})["open_id"]
  end

  def last_operator
    (FeishuObject.find_json_in_cache(last_operator_open_id) || {})["name"]
  end

  def parse_data
    self.pms_user_id = feishu_user&.user_id
    # self.status = "PART_APPROVED" if status == 'APPROVED' && last_operator.to_s.downcase !~ /#{last_node_name}/
  end

  def attachments
    form.select{|a| a["name"].to_s.downcase =~ /atta/ }.flat_map{|a| a["value"] }
  end

  def msg_body
    {id: id, user_name: user_name, status: status}
  end

  def qb_data
    usd = (form_field('$ Reimbursement（USD)').presence || form_field('$ Amount（USD)')).to_f
    {
      vendor: form_field("Vendor").presence || user_name,
      account: form_field("Account"),
      cost_item_id: cost_item_id,
      cost_id: cost_id,
      amount: usd,
    }
  end

  def form
    super.map(&:with_indifferent_access) rescue super
  end

  def form_field(name)
    a = form.find{|a| a[:name] == name}
    a ? a[:value] : ""
  end

  def simple_info
    form.map{|a| [a["name"], a["value"]]}.to_h.select{|k,v| v.is_a?(String)}.merge(
      "Submiter" => feishu_user.name
    )
  end

  def final_approver
    FeishuObject.find_user_with(task_list.select{|a| a["status"] == 'APPROVED'}.last["open_id"]).pms_user
  rescue => e
    nil
  end

  DEPARTMENT_DESK = {
    "Admin" => 10100,
    "Engineering" => 10101,
    "Accounting" => 10102,
    "Sales" => 10103,
    "HR" => 10104,
    "Marketing" => 10105,
    "Homeowner Signup" => 10106,
    "Operation" => 10107,
    "Product" => 10108,
    "Revenue" => 10109,
    "BI" => 10110,
    "CEO" => 10111,
    "Other" => 10112,
    "I don't know" => 10113,
  }
  def create_feature
    extra[:issue].present? || !approved? and return
    assign_on_extra(issue: "Pending")
    self.save
    sources = form.find{|a| a["name"] == "Submitter"}["open_ids"].map do |a| 
      FeishuObject.find_user_with(a)&.matchable_names&.first
    end.compact rescue nil
    sources ||= [FeishuObject.find_user_with(timeline.first["open_id"])&.matchable_names&.first]
    title = form_field("Request title").to_s.gsub("'","")
    desc = form_field("Request description").to_s.gsub("'","")
    feature_request? && form_field("Platform") != "PMS" and return
    _fields = {
      issuetype: {id: 10136},
      project: {id: 10221},
      customfield_10911: sources,
      customfield_10922: {id: (DEPARTMENT_DESK[form_field("Related Department")] || 10112).to_s}
    }
    _fields = {
      issuetype: {id: 4},
      customfield_10911: sources,
      project: {id: 10000},
      assignee: {id: "6191ee75744c4d00692fbcce"},
    } if feature_request?

    data = {
      fields: {
        summary: title,
        **_fields,
        description: {
          type: "doc",
          version: 1, 
          content: [
            {type: "paragraph", content: [{text: desc, type:"text"}]}
          ]
        }
      }
    }

    if issue = CommandCaller.call_ruby("jira.rb", "create_issue", data.to_json).presence
      _lark_content = help_desk? ? desc : title
      feishu_user.notify("Your Request Recorded:\n #{Jira.link(issue["key"], _lark_content)}") if issue['key']
      assign_on_extra(issue: issue["key"])
      self.save
      download_files.map{|f| CommandCaller.call_ruby("jira.rb", "upload_attachment", issue["key"], f)}
    end
  end

  FileDir = "#{Rails.root}/data/approvals/"
  def download_files
    files = []
    form_field("Attachment").each_with_index do |url, index|
      _fname = FileDir + "#{id}_#{index}.#{url.to_s.split(".").last}"
      _fname = FileDir + "#{id}_#{index}.png" if url.to_s =~ /authcode/
      File.open(_fname, "w") do |f| 
        f.puts Faraday.get(url).body.force_encoding('utf-8')
      end
      files << _fname
    end
    files
  end

  def cancel
    lark_user_id = form[0]["value"].first || FeishuObject.admin.feishu_user_id
    LarkApproval.send(approval_code).cancel(key, lark_user_id) rescue nil
  end

  def last_node_name
  end

  def process
    timeline&.map do |a|
      {
        start_at: Time.zone.at(a['create_time'].to_i / 1000).strftime("%m/%d/%Y %H:%M"),
        status: a['type'],
        feishu_user: FeishuObject.find_json_in_cache(a['open_id']) || {},
        comment: a['comment']
      }
    end || [] rescue []
  end

  def sub_status_content(split = "<br>")
    process.map do |h|
      base = "#{h[:start_at]}: #{h[:status]} #{h.dig(:feishu_user, :name)}"
      base += "#{split}    " + h[:comment].split("\n").join("#{split}    ") if h[:comment]
      base
    end.join(split).html_safe
  end

  def remote_data
    LarkApproval.send(approval_code).remote_data(key)
  end

  def check_comments
    cs = timeline&.select{|a| a[:comment].presence}
    cs.nil? and return

    first_comment = cs.shift
    first_comment.nil? and return

    LarkMsg.enqueue(:comments_on_approval, header: "Approval##{id} PO##{cost_id} comments", 
                    elements: cs.map(&:to_json)) rescue nil

    creator_id = FeishuObject.find_user_with(first_comment['open_id'])&.pms_user&.id
    wo = comment_workorder || create_comment_workorder(creator_id, first_comment["comment"])
    origin_notes = comment_workorder.notes
    cs.each_with_index do |comment, index|
      if origin_note = origin_notes[index]
        origin_note.update(content: comment)
      else
        origin_notes << comment_workorder.notes.create(creator_id: creator_id, content: comment)
      end
    end
    origin_notes
  end

  def comment_workorder
    ServiceSchedule.find_by(id: extra[:comment_wo_id])
  end

  def create_comment_workorder(creator_id, content)
    sid = Service.find_by(name: 'Others')&.id
    wo = ServiceSchedule.find_by(id: extra[:comment_wo_id])
    wo and return wo

    payload = {
      creator_id: creator_id, note: content,
      house_id: cost&.cost_items&.first&.house_id, service_id: sid,
      assignee_id: cost&.cost_items&.first&.house&.ae_user_id, 
    }
    wo = MangoRobot.create_pms_workorder(payload)
    assign_on_extra(comment_wo_id: wo.id)
    self.save(validate: false)
    wo.create_lark_task_related
    wo
  end

  class << self
    def init_from_lark(key, data)
      data = data.with_indifferent_access
      data[:form] = JSON.parse(data[:form])
      obj = where(key: key).first_or_create
      obj.update(data)
      obj.create_feature if obj.feature_request? || obj.help_desk?
      obj
    end

    def summary
      arr = ["PO Total Pending: #{count_with_amount po.pending}"]
      arr << "PO Total Approved: #{count_with_amount po.approved}"
      arr << "PO Yesterday Submitted: #{count_with_amount po.pending.created_on_yesterday}"
      arr << "PO Yesterday Approved: #{count_with_amount po.approved.updated_on_yesterday}"
      arr
    end

    def count_with_amount(arr)
      amount = PmsRecord.to_currency arr.map{|a| a.qb_data[:amount]}.sum
      "#{arr.count}(#{amount})"
    end

  end
end
