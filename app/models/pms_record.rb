class PmsRecord < ApplicationRecord
  SITE = PublicConfigs.end_points.pms

  self.abstract_class = true
  establish_connection(:from_pms)
  include Auditable

  scope :recent, -> {order("id DESC")}

  def self.get_work_days(from, to)
    _all = from.to_date.upto(to.to_date).to_a
    _all.select{|a| [6, 0].exclude?(a.wday) }.map(&:to_s) - PmsRecord.holidays
  end
end
