class Deposit < ApplicationRecord
  include Dateable
  include GlobalCacheable
  belongs_to :account_record

  def amount
    initial_amount
  end
  
  def done?
    refunded_amount >= initial_amount
  end

  def status_str
    received_amount == 0 and return "Not Received"
    received_amount < amount and return "Partially Received"
    refunded_amount < received_amount and return "Pending Refund"
  end
end
