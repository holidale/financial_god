class Service < PmsRecord
  has_many :service_schedules, dependent: :destroy
  has_many :recurring_work_orders, dependent: :destroy
  enum category: { property_inspection: 0, quick_inspection: 1, single_task: 2, check_out_inspection: 3,
                   check_in_inspection: 4, cleaning_inspection: 5, cleaning: 6 }

  CHECK_INSPECTION_CATEGORIES = %w{check_out_inspection cleaning}
  INSPECTION_CATEGORIES = %w{property_inspection check_in_inspection check_out_inspection quick_inspection cleaning_inspection cleaning}
  WORK_ORDER_CATEGORIES = categories.keys - INSPECTION_CATEGORIES

  scope :inspections, -> { where(category: INSPECTION_CATEGORIES.map { |c| categories[c.to_sym] }) }
  scope :check_inspections, -> { where(category: CHECK_INSPECTION_CATEGORIES.map { |c| categories[c.to_sym] }) }
  scope :work_orders, -> { where(category: WORK_ORDER_CATEGORIES.map { |c| categories[c.to_sym] }) }
  scope :maintenance_only, -> {
    where(category: WORK_ORDER_CATEGORIES.map { |c| categories[c.to_sym] }).where.not("name LIKE ? OR name LIKE ?", "%arden%", "%ecurring%")
  }
end
