class Cost < PmsRecord
  include Dateable
  include Approvalable

  belongs_to :user
  # belongs_to :landlord_statement
  # belongs_to :service_schedule
  # belongs_to :cleaner
  has_one :purchase_order, class_name: "Cost"
  has_many :invoices, foreign_key: :payment_id
  has_many :cost_items, dependent: :destroy
  has_many :cost_sub_items, dependent: :destroy
  has_many :items, through: :cost_items
  has_many :approvals

  enum category: {
    purchase_order: 'purchase_order',
    invoice: 'invoice'
  }

  VALID_STATUSES = %w(pending pending_reimbursement approved paid bill_verification)
  enum status: VALID_STATUSES.map { |s| [s.to_sym, s] }.to_h

  scope :holidale, -> { where(business_entity: "holidale") }
  scope :greenland, -> { where(business_entity: "greenland") }
  scope :effective_invoice, -> { invoice.where("landlord_expense > ?", 0) }
  scope :effective_purchase_order, -> { purchase_order.where("holidale_expense > ?", 0) }

  def cleaner
    @cleaner ||= Cleaner.find_by(id: cleaner_id)
  end

  def has_workorder?
    workorders_in_item.present?
  end

  def workorders_in_item
    cost_items.map(&:service_schedule)
  end

  def for_holidale?
    business_entity == 'holidale'
  end

  def opposite_po
    Cost.find_by(cost_id: id)
  end

  def all_costs
    Cost.where(id: [id, opposite_po&.id, cost_id, opposite_po&.cost_id])
  end

  def finished?
    approved? || paid?
  end

  def single_invoice?
    all_costs.size == 1 && invoice? 
  end

  def can_to_lark?
    finished? and return false, "PO##{id} Already #{status}"

    _costs = all_costs
    Rails.env.production? && created_at.to_date < APPROVAL_V2_START and return false, "PO##{id} is old data"
    # Four records and both POs vendors are Greenland, can not to Lark
    _costs.size == 4 && _costs.map{|a| a.cleaner_id}.compact == [432, 432] and return false, "Greenland PO##{id} not to Lark"

    proper_status = pending_reimbursement? || pending?
    proper_status && (single_invoice? || (holidale_po? && holidale_expense.to_f != 0))
  end

  def holidale_po?
    purchase_order? && for_holidale? 
  end
end
