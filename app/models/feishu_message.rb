class FeishuMessage < ApplicationRecord
  def self.hook_msg(sender, obj_receiver, msg, msg_id = nil)
    msg = msg.to_json if !msg.is_a?(String)
    create(
      sender_type: sender, content: msg, message_id: msg_id,
      receivable_type: obj_receiver.class.name, receivable_id: obj_receiver.id, 
    )
  rescue => e

  end
end
