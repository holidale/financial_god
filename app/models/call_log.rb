class CallLog < PmsRecord
  include ExtraObjectable
  has_many :workorder_messages, primary_key: :sid, foreign_key: :sid

  def lark_msg
     {
      id: id,
      sid: sid,
      from: from,
      to: to,
      direction: direction,
      call_status: call_status,
      call_duration: call_duration,
      date: start_time,
      work_orders: workorder_messages.map{|wom| wom.workorder.id}
    }
  end
end
