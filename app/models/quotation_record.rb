class QuotationRecord < ApplicationRecord
  self.table_name = "#{PublicConfigs.qcenter_db_name}.quotation_records"
  include ExtraObjectable
  belongs_to :quotation, class_name: "QuotationNew"
  # belongs_to :income_account
  #
  # validates :income_account, presence: true

  scope :top_level, -> {where(parent_id: 0)}

  def amount
    override.to_d > 0 ? override : super
  end

  def final_tax_rate
    tax.to_f / amount.to_f
  end
  #
  # def display_name
  #   super || income_account&.name
  # end

  def taxable_amount
    extra[:pure_amount] || amount
  end

  def fee_key
    extra[:fee_key]# || income_account&.key
  end

  def amount_with_tax
    amount.to_d + tax.to_d
  end
  #
  # def api_json(round: 2)
  #   if amount.to_f > 0
  #     {
  #       key: fee_key, 
  #       title: display_name, 
  #       amount: _amount = amount_with_tax.round(round), 
  #       origin_amount: amount.round(round), 
  #       tax: tax.round(round), 
  #     }
  #   end
  # end
end
