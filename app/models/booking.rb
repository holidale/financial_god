class Booking < PmsRecord
  include Dateable
  include QuotationShareable
  include ExtraObjectable

  has_many :invoices
  has_many :contract_amendments
  has_many :booking_revenues
  has_one :agency_labor_event, dependent: :destroy

  belongs_to :house
  belongs_to :organization
  belongs_to :user
  # has_one :quotation
  belongs_to :creator, class_name: "User", foreign_key: :creator_id
  belongs_to :referrer, class_name: "User", foreign_key: :referrer_id

  STATUS = %w(draft inquiry booked long_term_booked rejected canceled cancelled_by_traveller blocked owner_returned temp_blocked imported_booking blocked_pending_payment canceled_imported_booking pending billing_error)

  (STATUS - %w(booked)).each do |s|
    scope s, -> { where(status: s) }
  end

  scope :booked, -> { where(status: Booking.real_booked) }
  scope :pure_booked, -> { where(status: 'booked') }
  scope :near_check_in, ->(n = 1) { 
    pure_booked.where("check_in = ? or arrival_date = ?", Date.current + n, Date.current + n) 
  }
  scope :current, -> { 
    pure_booked.where("COALESCE(bookings.departure_date, bookings.check_out) >= ? and COALESCE(bookings.arrival_date, bookings.check_in) <= ?", Date.current, Date.current) 
  }
  scope :future, -> { 
    pure_booked.where("COALESCE(bookings.departure_date, bookings.check_out) > ?", Date.current) 
  }
  scope :check_in_at, ->(date = Date.current) { pure_booked.where("check_in = ? or arrival_date = ?", date, date) }
  scope :check_out_at, ->(date = Date.current) { pure_booked.where("check_out = ? or departure_date = ?", date, date) }
  scope :recent_booked_in_minutes, ->(n) {booked.where(changed_to_booked_date: n.minutes.ago..Time.current)}

  def total_coverd_days
    covered_days + extension_covered_days
  end

  def covered_days
    (check_out - check_in).to_i + delt_cover
  end

  def covers_desc
    "#{covered_days}(#{pay_by_days ? "Days" : "Nights"})"
  end

  def delt_cover
    pay_by_days ? 1 : 0
  end

  def real_check_in_date
    [check_in, arrival_date].compact.max
  end

  def real_check_out_date
    [check_out, departure_date].compact.min
  end

  def real_check_in
    arrival_date || check_in
  end

  def real_check_out
    departure_date || check_out
  end

  def final_locked_day
    real_check_out_date + turnover_block_day - (self.pay_by_days ? 0 : 1) if check_out
  end

  def extension_covered_days
    contract_amendments.select(&:active?).sort_by(&:start_date).map do |c|
      (c.end_date.to_date - c.start_date.to_date).to_i + delt_cover
    end.sum
  end

  def booked?
    Booking.real_booked.include?(status)
  end

  def data_for_robot_msg
    {
      deal_time: created_at.strftime("%m/%d"),
      address: [house.address2, house.city.to_s.titleize].join(","),
      guest_name: user&.full_name || "",
      lease_term: lease_term,
      total_amount: Booking.to_currency(broadcast_total),
      quotation_version: quotation_version,
      booking_source: booking_source,
      referrer: get_referrer&.full_name,
    }
  end

  def data_for_robot_msg_content
    data = data_for_robot_msg.slice(
      :deal_time, :address, :guest_name, :lease_term, :total_amount, :quotation_version,
      :booking_source
    )
    arr = data.map do |k, v|
      [k.to_s.titleize, v].join(": ")
    end
    arr.unshift("**New Booking: #{LarkMsg.link_to_pms(self)}**")
  end

  def lease_term
    [check_in, check_out].map{|a| a.strftime("%m/%d/%Y")}.join("-")
  end

  def real_lease_term
    [real_check_in, real_check_out].map{|a| a.strftime("%m/%d/%Y")}.join("-")
  end

  def has_referrer?
    get_referrer.present?
  end

  def can_broadcast?
    true
    # has_referrer? && broadcast_total.to_f > 0
  end

  def broadcast_total
    !booked? and return 0
    n = shareable_revenue(source: 'quotation')
    if n <= 0
      LarkMsg.enqueue(:fixed_revenue_error, "B##{id} New Booked, But Fixed revenue Error") if !no_need_booking_revenue_house?
      n = sharable_revenue_with_landlord.to_f
    end
    n.to_f > 0 ? n : invoices.sum(&:amount)
  end

  def shareable_revenue(source: 'all')
    n = quotation.extra.dig(:breakdown_list_snapshot, :lists, :total).last[:total] rescue 0
    n = quotation.extra.dig(:revenue_details, :shareable_revenue) || 0 if n <= 0 rescue 0
    source == 'quotation' || n.to_f > 0 and return n.to_f
    sharable_revenue_with_landlord.to_f
  end

  def total_shareable_revenue
    key = "booking_revenue_#{id}_#{updated_at.to_i}_#{invoices.order("updated_at").last&.updated_at.to_i}"
    n = $redis.get(key).split("valuef\v").last.split(":").first.to_f rescue 0
    n.to_f > 0 and return 0
    shareable_revenue(source: 'quotation') +
      contract_amendments.active.sum(&:shareable_revenue)
  end

  def no_need_booking_revenue_house?
    contract_type.in? %w(fix_income long_term_lease)
  end

  def org
    organization || user&.organization
  end
  alias :booking_organization :org

  def org_name
    org&.name
  end

  def get_referrer
    referrer || creator
  end

  def quote_objects
    old_quotation? and return ([quotation] + extensions).compact
    ([quotation] + extensions.map(&:quotation_new)).compact
  end

  def quote_objects_json
    quote_objects.map do |a|
      covers = (end_date - start_date) + delt_cover
      {start_date: a.start_date, end_date: a.end_date, covers: covers}
    end
  end

  def extensions
    contract_amendments.need_revenues
  end

  def all_extension_invoices
    edate = real_check_out_date
    invoices.from_extension.effective.select do |p|
      (p.start_date && p.start_date < edate) || (p.end_date == edate)
    end
  end

  def send_negotiated_invoice
    org&.fee_policies&.first&.send_negotiated_invoice
  end

  def is_individual_guest?
    org.nil? || org&.fee_policies.empty? || org&.fee_policies&.first&.use_guest_quotation_form
  end

  def from_org?
    !is_individual_guest?
  end

  ['Airbnb', 'VRBO', 'TripAdvisor', 'Holidale', 'Holidale Individual', 'booking_dot_com', 'WeIrvine', 'Offline',
    'trulia', 'hotpads', 'Ctrip', 'holidale_miniapp', 'miniapp', 'HA Integration', 'chinese_in_la', 'expedia', 'tujia',    'mls', 'flipkey', 'red_awning', 'facebook', 'rentals_united', 'Month2Month BD', 'zillow']

  def from_internal?
    ["Holidale", "Month2Month BD", "Holidale Individual", 'holidale_miniapp'].include?(booking_source)
  end

  def from_external?
    !from_external?
  end

  def short_line_for_lark
    "【H##{house_id} #{house&.address2}】[B##{id}](#{PmsRecord::SITE}/bookings/#{id}/show_and_edit)"
  end

  def future_days
    check_in < Date.current ? (real_check_out_date - Date.current + gap_days) : covered_days
  end

  def gap_days
    pay_by_days ? 1 : 0
  end

  def total_revenue_kpi_export
    _invoices = invoices.select{ |p| !%w(bad_debt voided).include?(p.status) && !p.utility? && p.transaction_type != 'deposit' }
    _invoices.reject!{|iv| iv.transaction_type == 'extension' && iv.start_date > real_check_out_date}
    _invoices.map {|p| (%w(booking date_change item).include? p.receipt.try(:category)) ? p.amount : 0}.sum
  end

  def self.long_time_blocked_status
    %w(blocked owner_returned out_of_order)
  end

  def self.real_booked
    %w(booked long_term_booked)
  end

  def self.airbnb_ar_aging
    pure_booked.where(booking_source: 'airbnb').where("created_at > ?", '2021-01-01 00:00:00').each do |b|
      is = b.invoices
      if is.any?{|i| i.status.in?(["pending", "partially_paid"])}
      end
    end
  end

  def create_msg
    "New Booking ##{id} from #{real_check_in_date.strftime("%m/%d/%Y")} to #{real_check_out_date.strftime("%m/%d/%Y")},House #{house.short_address}"
  end

end
