class Channel < PmsRecord
  has_many :channel_listings
  has_many :houses, through: :channel_listings
end
