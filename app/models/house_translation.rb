class HouseTranslation < PmsRecord
  COLUMNS = %i(title description)
  belongs_to :house
end 
