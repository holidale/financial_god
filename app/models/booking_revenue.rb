class BookingRevenue < PmsRecord
  NEED_COVERT_TO_30_DAYS_MONTHS = [1, 2, 3, 5, 7, 8, 10, 12].freeze
  include ExtraObjectable

  belongs_to :revenueable, polymorphic: true
  enum revenue_type: %w(month day)
  enum status: %w(not_sent sent)

  belongs_to :booking
  belongs_to :house

  scope :in_period, -> (period) {where(period: period).order("period,start_date")}
  scope :version, -> (version) {where(version: version).order("period,start_date")}
  scope :already_sent, -> {where(arel_table[:send_at].lt(Time.current))}
  scope :with_booked_booking, -> {joins(:booking).where("bookings.status": %w(booked long_term_booked))}

  def override_details
    extra[:override_details] || {}
  end

  def landlord_override_enabled
    override_details[:enabled]
  end

  def version
    landlord_override_enabled and return 1 # use old override, ensure go version 1 process
    super
  end

  def owner_revenue
    landlord_override_enabled and return override_revenue
    super
  end

  def override_revenue
    _daily = override_details[:rate_type] == 'daily' ? override_details[:rate] : override_details[:rate].to_f / 30
    _daily * real_covered_days
  end

  def real_owner_revenue
    !before_check_in? && (old_version? || before_check_out?) and return owner_revenue || 0
    (revenueable.daily_owner_revenue * real_covered_days).round(FixedRevenueable::REVENUE_ROUND_FRONTENT)
  end

  def old_version?
    version.to_i <= 1 || booking_id.nil? || end_date.nil?
  end

  def before_check_in?
    start_date && (start_date < booking.real_check_in_date)
  end

  def before_check_out?
    end_date && (end_date <= booking.real_check_out_date)
  end

  def real_start_date
    [start_date, booking.real_check_in_date].compact.max
  end

  def real_covered_days
    old_version? || before_check_in? and return dynamic_covered_days
    before_check_out? and return covered_days || dynamic_covered_days
    n = (booking.real_check_out_date - real_start_date).to_i + booking.gap_days
    n.to_f > 0 ? n : 0
  end

  def dynamic_covered_days
    booking and return booking.get_duration_of_per_year_month[period.to_date]
    (period.end_of_month - period.beginning_of_month  + 1).to_i
  rescue => e
    0
  end

  def revenueable
    super rescue nil
  end

  def daily_owner_revenue
    revenueable.daily_owner_revenue rescue 0
  end

  def new_rule?
    period.present? && period.to_date > "2023-05-01".to_date
  end

  def dynamic_daily_revenue
    return revenueable.daily_revenue.to_f unless new_rule?

    _revenueable = revenueable

    if _revenueable.is_a?(QuotationNew)
      if new_rule?
        return _revenueable.new_daily_revenue
      else
        return _revenueable.daily_revenue
      end
    end

    _revenueable.compute_daily_owner_revenue(new_rule: true)
    _revenueable.daily_revenue.to_f
  end

  SHORT_TYPES = {
    "Quotation" => "Q",
    "QuotationNew" => "QN",
    "ContractAmendment" => "CA",
  }
  def short_type
    SHORT_TYPES[revenueable_type] || revenueable_type
  end

  def lark_attrs
    {
      "rtype" => "#{short_type}##{revenueable_id}",
      "period" => "#{start_date.to_s[5,5]}/#{end_date.to_s[5,5]}",
      "revenue" => "#{owner_revenue.round(2)}(#{daily_owner_revenue.round(2).to_s} x #{covered_days})",
      "ratio" => revenue_ratio,
    }
  end
end
