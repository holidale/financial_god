class ScannedFile < PmsRecord
  include Dateable
  belongs_to :relatable, polymorphic: true
  enum category: {
    id_front: 10,
    id_back: 11,
    air_tickets: 100,
  }

  def dir_path
    "/opt/webapp/financial_god/shared/private/scanned_file/related_file"
  end

  # Get local first
  # If has no file, download from S3
  def private_url
    _file = -> {Dir.glob("#{dir_path}/#{id}*").first}
    file = _file.call and return file
    CommandCaller.copy_files("/private/uploads/scanned_file/related_file/#{id}", dir_path)
    file = _file.call and return file
    download_remote_s3_file
  end

  def download_remote_s3_file
    if cached_url = RedisHelper.get("ScannedFile_#{id}_url").presence
      file = cached_url.split("/").last.split("?").first
      _fname = dir_path + "/#{id}_#{file}"
      File.open(_fname, "w") do |f| 
        f.puts Faraday.get(cached_url).body.force_encoding('utf-8')
      end
      return _fname
    end
  end
end
