class CostItem < PmsRecord
  include Dateable

  belongs_to :cost
  belongs_to :house
  belongs_to :booking
  belongs_to :financial_account
  belongs_to :service_schedule
  # belongs_to :cost_item
  has_many :scanned_files, as: :relatable, dependent: :destroy
  has_many :cost_sub_items, dependent: :destroy
  has_one :cost_item
  has_many :approvals

  enum customer: {
    landlord: "landlord",
    holidale: "holidale",
    tenant: "tenant",
    vendor: "vendor",
    greenland: "greenland",
    landlord_deposit: "landlord_deposit",
    landlord_payment: "landlord_payment"
  }

  def siblings 
    cost&.cost_items - [self]
  end

  def can_to_lark?
    cost&.can_to_lark?
  end
end
