class Invoice < PmsRecord
  self.table_name = "payments"
  include GlobalCacheable
  include ExtraObjectable
  include Dateable
  include OldLogicInvoice

  serialize :finance_values, Hash

  belongs_to :booking
  belongs_to :cost
  has_one :receipt, dependent: :destroy, foreign_key: :payment_id
  has_many :booking_items, through: :receipt
  has_many :payment_received_payments, foreign_key: :payment_id
  has_and_belongs_to_many :received_payments, join_table: :payments_received_payments, foreign_key: :payment_id
  has_many :refund_receipts, dependent: :destroy
  has_many :deduct_refunds, dependent: :destroy
  has_many :credit_memo_payments, foreign_key: :payment_id
  has_many :credit_memos, :through => :credit_memo_payments

  DYNAMIC_FINACE_VALUES = %i[received_amount paid_amount refund_amount credit_amount credit_amount_of_discount]

  PMS_STATUS = %w(pending partially_paid paid refunded partially_refunded collecting in_dispute bad_debt voided)
  PMS_STATUS.each do |s|
    scope s, -> {where(status: s)}
    define_method "#{s}?" do
      status == s
    end
  end

  PMS_TRANTYPES = %w(deposit extension)
  PMS_TRANTYPES.each do |s|
    scope s, -> {where(transaction_type: s)}
  end

  scope :effective, -> {where.not(status: %w(voided bad_debt))}
  scope :compared_period_in, -> (period) {
    s = "#{period}%"
    joins(:booking).effective.where("payments.created_at like ? or payments.updated_at like ?", s, s)
      .where("bookings.status in ('booked', 'long_term_booked')")
  }
  scope :unpaid, ->{where(status: unpaid_status)}
  scope :due_over_2_years, -> {where("due_date > '#{2.years.ago}' and due_date < '#{Time.current}'").
                               order("due_date DESC")}
  scope :from_booking, -> { joins(:receipt).where("receipts.category = ?", 3).where("payments.transaction_type = ? OR payments.transaction_type = ? ", "hybrid", "full") }
  scope :from_extension, -> { joins(:receipt).where("receipts.category = ?", 2) }
  scope :from_additional, -> { joins(booking_items: :item).where("items.category = ?", 0) }

  def unpaid?
    Invoice.unpaid_status.include?(status)
  end

  def account_record
    AccountRecord.invoices.find_by(record_source_id: id)
  end

  def total_receivable
    total_receivable_without_late_fee + late_fee
  end

  def total_receivable_without_late_fee
    apply_transaction_fee ? total_receivable_without_transaction_fee + transaction_fee_by_customer.to_f : total_receivable_without_transaction_fee
  end

  def total_receivable_without_transaction_fee
    amount + (deposit || 0)
  end

  def late_fee
    (total_receivable_without_late_fee * late_fee_ratio).round(2) rescue 0
  end

  def amount_with_late_fee
    amount + late_fee
  end

  def balance_due
    total_receivable - paid_amount
  end

  DYNAMIC_FINACE_VALUES.each do |value|
    define_method(value) do
      finance_values[value] || 0
    end
  end

  def service_term
    _s, _e = start_date, end_date
    [_s, _e].compact.map{|a| a.strftime("%m/%d/%Y")}.join("-")
  end

  def covers
    (end_date - start_date) + 1 rescue 1
  end

  def memo
    service_term
  end

  def effective?
    !%w(bad_debt voided).index(status) # && booking&.effective?
  end

  def invoice_breakdown_hash
    JSON.parse(invoice_breakdown) rescue {}
  end

  def lark_attrs
    {
      id: "I##{id}",
      total_receivable: total_receivable, 
      status: status, 
      due: "Due: #{balance_due}"
    }
  end

  class << self
    def unpaid_status
      %w(pending partially_paid collecting)
    end
  end
end
