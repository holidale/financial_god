module QuotationShareable
  extend ActiveSupport::Concern

  included do
    has_one :quotation, dependent: :destroy
    has_many :quotation_news, dependent: :destroy, class_name: "QuotationNew"

    scope :from_new_quotes, -> {where(quotation_version: 2)}
    scope :from_old_quotes, -> {where(quotation_version: 1)}

    def quotation
      old_quotation? ? super : quotation_news.first
    end

    def old_quotation?
      quotation_version.to_i <= 1
    end

    def new_quotation?
      !old_quotation?
    end
  end
end
