# frozen_string_literal: true

module AgencyLaborCommonConcern
  extend ActiveSupport::Concern

  included do
    enum agency_labor_type: {
      disabled: 0,
      ratio_per_day: 10,
      flat_rate_per_day: 20,
      flat_rate_per_booking: 21
    }, _prefix: :agency_labor
  end

  def total_agency_labor(quotation)
    return 0 if agency_labor_disabled?
    return agency_labor_value if agency_labor_flat_rate_per_booking?
    return total_agency_labor_with_flat_rate_per_day(quotation) if agency_labor_flat_rate_per_day?

    total_agency_labor_with_ratio_per_day(quotation)
  end

  def total_agency_labor_with_flat_rate_per_day(quotation)
    agency_labor_value * quotation.los
  end

  def total_agency_labor_with_ratio_per_day(quotation)
    agency_labor_amount = quotation.agency_labor_base_rent * agency_labor_percentage
    agency_labor_daily = agency_labor_amount / quotation.los

    return agency_labor_amount if agency_labor_no_cap? || agency_labor_daily < agency_labor_daily_cap

    agency_labor_daily_cap * quotation.los
  end

  def agency_labor_percentage
    agency_labor_value / 100.0
  end

  def agency_labor_daily_cap
    agency_labor_monthly_cap / 30.0
  end

  def agency_labor_flat_rate?
    agency_labor_flat_rate_per_day? || agency_labor_flat_rate_per_booking?
  end

  def agency_labor_no_cap?
    agency_labor_monthly_cap.zero?
  end
end