module Analysisable
  def sold_rate
    # key = "#{id}_vacancy_rate"
    # a = RedisHelper.get(key)
    # a and return a.to_f
    #
    rate = sold_days / avaliable_days.to_f
    RedisHelper.cache(key, rate, 1.days)
  end

  def sold_days
    @sold_days ||= bookings.pure_booked.map(&:covered_days).sum -
      bookings.pure_booked.future.map(&:future_days).sum
  end

  def none_avaliable_days
    @none_avaliable_days ||= bookings.owner_returned.map(&:covered_days).sum -
      bookings.owner_returned.future.map(&:future_days).sum
  end

  def avaliable_days
    @avaliable_days and return @avaliable_days
    first_date = contracts.first.effect_date
    start_date = [first_date, bookings.order("check_in").first&.check_in || first_date].min
    end_date = [contracts.last.expire_date, Date.current].min

    @avaliable_days = (end_date - start_date + 1 - none_avaliable_days).to_i
  end

  def self.percent(f)
    (f.to_f * 100).round(2).to_s + "%"
  end
end
