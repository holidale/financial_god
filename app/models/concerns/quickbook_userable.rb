module QuickbookUserable
  REALM_IDS = AppConfig.quickbook.realm_ids
  REALM_IDS_INVERT = REALM_IDS&.invert || {}

  extend ActiveSupport::Concern

  included do
    class << self
      def qb_user
        find_by(id: RedisHelper.get("current_qb_user_id")) ||
          find_by(email: AppConfig.qb_auto_sync_account)
      end

      def qb_api
        qb_user.qb_api
      end

      def revenue_clearing_account_balance
        amount = qb_user.revenue_clearing_account_balance
        RedisHelper.cache!("clearing_account_monitor_last_ok", Time.now.to_i) if amount == 0
        amount
      end
    end

    def revenue_clearing_account_balance
      qb_api.get(:account, QB_CLEARING_ACCOUNT_VALUE)["CurrentBalance"] rescue "Error"
    end
  end

  def set_qb_session(realm_id, new_session)
    new_session[:realm_id] = realm_id
    _qb_session = new_session.with_indifferent_access

    expires_in = _qb_session.delete(:expires_in) || 1.hours
    x_refresh_token_expires_in = _qb_session.delete(:x_refresh_token_expires_in) || 100.days

    company = REALM_IDS_INVERT[realm_id.to_i].to_sym rescue realm_id
    RedisHelper.cache!("current_qb_user_id", id)
    RedisHelper.cache(long_term_session_key(company), _qb_session, x_refresh_token_expires_in)
    RedisHelper.cache(qb_session_key(company), _qb_session, expires_in)
  end

  def clear_qb_session(company = :all)
    cs = [company]
    cs = AppConfig.quickbook.realm_ids.keys if company == :all
    cs.map do |c| 
      RedisHelper.del(qb_session_key(c)) 
      RedisHelper.del(long_term_session_key(c))
    end
  end

  def qb_session_key(company = :holidale, env = Rails.env)
    env = "production_web5" if Rails.env.production? # use web5's qb session
    "qb_session:short_term:#{id}:#{env}:#{company}"
  end

  def long_term_session_key(company = :holidale, env = Rails.env)
    env = "production_web5" if Rails.env.production?
    "qb_session:long_term:#{id}:#{env}:#{company}"
  end

  def qb_session(company = :holidale)
    RedisHelper.get_json(qb_session_key(company)).presence || refresh_qb_session(company)
  end

  def long_term_qb_session(company = :holidale)
    RedisHelper.get_json(long_term_session_key(company)).presence
  end

  def qb_api(company = :holidale)
    _qb_session = qb_session(company)
    !_qb_session.presence and return
    @qb_api ||= QboApi.new(access_token: _qb_session[:access_token], realm_id: _qb_session[:realm_id])
  end

  def refresh_qb_session(company = :holidale)
    _lsession = long_term_qb_session(company)
    !_lsession.presence and return

    _env = env_production? ? "production" : "sandbox"
    client = IntuitOAuth::Client.new(AppConfig.quickbook.client_id, AppConfig.quickbook.client_secret, 'redirectUrl', _env)
    new_token = client.token.refresh_tokens(_lsession[:refresh_token])
    set_qb_session(_lsession[:realm_id], JSON.parse(new_token.body.to_json))
  # rescue => e
  #   nil
  end

  def get_list_from_qb(qb_entity, start_at, end_at = Time.current)
    !qb_api.presence and return []

    start_at = start_at.round.iso8601(0)
    end_at = end_at.round.iso8601(0)
    range = "Metadata.LastUpdatedTime>='#{start_at}' And Metadata.LastUpdatedTime<'#{end_at}'"
    qb_api.query("select * from #{qb_entity} Where #{range} Order By Metadata.LastUpdatedTime") || []
  rescue => e
    Rails.logger.info("get_list_from_qb ERROR: #{e.message}")
    # sometimes return auth error even if there always has a right QB session
    []
  end

  MONIT_ENTITIES = %w(Invoice JournalEntry)
  def get_entiries_updated_from_last_rca_ok
    last_ok_time = Time.zone.at RedisHelper.get("clearing_account_monitor_last_ok").to_i
    last_ok_time = 1.days.ago if (Time.current - last_ok_time) > 24 * 3600

    _updated = MONIT_ENTITIES.map {|a| get_list_from_qb(a, last_ok_time).map{|a| a["DocNumber"]}}.sum
    _deleted = get_entiries_deleted_from_last_rca_ok
    _deleted.empty? and return _updated.join(",")
    _updated.join(",") + "\nRecent Deleted Entities: " + _deleted.join(",")
  end

  def get_entiries_deleted_from_last_rca_ok
    arr = []
    while entity = RedisHelper.rpop("DELETED_QB_entities")
      delete_entity = JSON.parse(entity) rescue {}
      arr << delete_entity['local_value'] if MONIT_ENTITIES.index(delete_entity['remote_type'])
    end
    arr
  end
end
