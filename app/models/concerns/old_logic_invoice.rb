module OldLogicInvoice

  def receipt_category
    receipt&.category
  end
  
  def get_item_type_of_invoice
    @item_type ||= (receipt_category.try(:titleize) || "Booking")
  end

  def invoice_breakdown
    JSON.parse(super).with_indifferent_access rescue {}
  end

  def utility?
    is_utility || is_utility_cost
  end

  def is_utility
    utility_description.present? || invoice_breakdown[:utility] and return true
    is_final_utility || is_pre_charged_utility
  end

  def is_final_utility
    get_item_type_of_invoice == "Item" && self&.booking_items&.first&.name == "Utility"
  end


  def is_pre_charged_utility
    is_utility_deposit || is_utility_cost
  end

  # used to check if invoice is for additional utility deposit (especially when sending monthly utility usage statement)
  def is_utility_deposit
    get_item_type_of_invoice == "Item" && self&.booking_items&.first&.name == "Additional Utility Deposit"
  end

  # used to check if invoice is for utility cost(pre-payment of utility) (especially when sending monthly utility usage statement)
  def is_utility_cost
    get_item_type_of_invoice == "Item" && self&.booking_items&.first&.name == "Utility Cost"
  end

  def is_damage
    get_item_type_of_invoice == "Item" && self.cost_id.present?
  end

  # used to check if invoice is for utility deposit
  def utility_deposit
    get_item_type_of_invoice == "Item" && self&.booking_items&.first&.name == "Utility Deposit"
  end

  def invoice_date
    booking&.org_name == "Plus Relocation" and return booking.created_at.to_date

    is_utility || is_utility_deposit || is_damage and return created_at.to_date
    start_date || booking&.check_in
  end

  def dynamic_received_amount(with_round: true)
    a = self.payment_received_payments.sum(:amount) rescue 0
    with_round ? a.round(2) : a
  end

  # returns sum of amount that received_payments and credit_memos applied on the invoice
  def dynamic_paid_amount
    (dynamic_received_amount(with_round: false) + dynamic_credit_amount(with_round: false)).round(2)
  end

  # returns total refund amount refunded from the invoice
  def dynamic_refund_amount
    (self.refund_receipts.sum(:amount) rescue 0).round(2)
  end

  # returns total credit_amount applied on the invoice
  def dynamic_credit_amount(with_round: true)
    a = self.credit_memo_payments.sum(:amount) rescue 0
    with_round ? a.round(2) : a
  end

  def dynamic_credit_amount_of_discount(with_round: true)
    a = self.credit_memo_payments.select{|a| !a.credit_memo&.credit? }.sum(&:amount) rescue 0
    with_round ? a.round(2) : a
  end

  def quiet_update_finance_values
    _cache_finance_values = {}
    %i[received_amount paid_amount refund_amount credit_amount credit_amount_of_discount].map do |value|
      _cache_finance_values[value] = send("dynamic_#{value}")
    end
    update_columns(finance_values: _cache_finance_values)
  end
end
