module Dangerable

  def analisys
    danger_detail
  end

  def danger_detail
    arr = __send__("#{self.class.name.tableize.singularize}_danger_detail") rescue []
    arr.map do |a| 
      "#{show_booking_and_house} #{a}"
    end
  end

  def show_booking_and_house
    arr = []
    arr << "B##{booking_id}" if respond_to?(:booking_id)
    arr << "H##{house_id}" if respond_to?(:house_id)
    arr.join(" ")
  end

  def quotation_danger_detail
    arr = []
    !booking.booked? and return arr
    arr << "Pet Fee Lost" if pet_rent.to_f > 0 && pet_fee.to_f <= 0 
    arr << "Pet Fee Repeated!!!!!" if negotiated_pet_fee.to_f > 0 && pet_fee.to_f > 0 # Bug HWA-5941
    arr << "Pet Rent Lost" if pet_fee.to_f > 0 && pet_rent.to_f <= 0
    arr << "Cleaning Fee Lost" if cleaning_fee.to_f < 0
    arr << "All-In Lost Utility Fund" if utility_fund.to_f <= 0 && all_inclusive?
    arr << "Breakdown Has Utility Fund" if utility_fund.to_f > 0 && breakdown?
    arr << "Breakdown Lost Application Fee" if application_fee.to_f <= 0 && breakdown? && booking&.from_internal?
    arr << "Revenue Lost Or Wrong: Quote##{id}" if revenue_wrong?
    arr
  end

  def revenue_wrong?
    booking&.contract_type == 'fix_income' and return
    if empty_revenue? || daily_revenue.to_f <= 0 || owner_daily_revenue.to_f <= 0
      return notify_pms_data_update("auto_check_and_refix_revenues")
    end
  end

  def booking_danger_detail
    arr = []
    !booked? and return arr
    is = invoices.sort_by(&:start_date).select(&:start_date)
    if is.size > 1 && check_in.day >= 15 && (i = is.first).covers < 15
      arr << "First Invoice Too Short: CoverDays: #{i.covers.to_i} Amount: #{i.total_receivable}"
    end
    ord = quotation.daily_owner_revenue.to_f
    arr << "Arrival Date Early than CheckIn" if arrival_date && arrival_date < check_in
    arr << "Departure Date Later than CheckOut" if departure_date && departure_date > check_out
    if contract_type != 'fix_income' && booking_revenues.empty?
      arr << "OwnerRevenue $0" if ord == 0
      arr << "OwnerRevenue not calculated"
      notify_pms_data_update("fill_fixed_revenues_to_db")
    end
    arr
  end

  def invoice_danger_detail
    arr = []
    !booking.booked? and return arr
    arr << "Pure Deposit in Invoice" if transaction_type == 'deposit'
    arr
  end

  def booking_revenue_danger_detail
    arr = []
    if version == 2
      b = owner_revenue / covered_days
      c = revenueable.daily_owner_revenue
      arr << "Revenue Wrong: B##{booking_id}, #{c} -> #{b} " if b.round(2) != c.round(2) && c != 0 
    end
    arr
  end

  def contract_amendment_danger_detail
    arr = []
    !booking.booked? and return arr

    cs = [booking.quotation] + booking.contract_amendments.active_amendments
    cs.each_with_index do |c, i|
      if next_one = cs[i + 1]
        e, s = next_one.start_date, c.end_date
        arr << "Start Date not connected #{s} #{e} Ext##{id}" if (e - s).abs > 1
      end
    end
    arr << "New CheckIn May Be Wrong: Ext##{id}" if new_check_in != old_check_in
    arr << "New CheckOut May Be Wrong: Ext##{id}" if new_check_out != end_date && new_check_in == old_check_in
    arr << "New CheckIn May Be Wrong: Ext##{id}" if new_check_in != start_date && new_check_out == old_check_out
    arr << "Revenue Lost Or Wrong: Ext##{id}" if revenue_wrong?
    arr
  end

  def notify_pms_data_update(key)
    RedisHelper.lpush(:fix_data, {klass: self.class.name, id: id, key: key})
  end

  def cost_danger_detail
    arr = []
    if holidale_expense.to_f.abs > 1000
      # arr << "Suspected Amount #{PmsRecord.to_currency(holidale_expense)}"
      # cost_items.each_with_index do |c, i|
      #   c.scanned_files.each_with_index {|a, i| arr << "CostItem#{c.id} [File #{i + 1}](#{a.private_url})" }
      # end
    end
    # arr << "Frozen Data changes: " if approved?
    arr
  end

  def cost_item_danger_detail
    arr = []
    # if amount.to_f.abs > 300
    #   arr << "Suspected Amount #{PmsRecord.to_currency(amount)}"
    #   scanned_files.each_with_index {|a, i| arr << "[File #{i + 1}](#{a.private_url})"}
    # end
    if item_total_amount != cost.holidale_expense.to_f && cost&.purchase_order?
      arr << "Suspected Amount: 
      Holidale Expense: #{PmsRecord.to_currency(cost&.holidale_expense)}
      Item Total Amount: #{PmsRecord.to_currency(item_total_amount)}"
    end
    # arr << "Frozen Data changes: " if approved?
    arr
  end

  def cost_sub_item_danger_detail
    # cost_item_danger_detail
  end

  def danger_detail_full_msg(audit = nil)
    danger_detail.map! do |d|
      if audit && d.to_s =~ /Frozen Data/
        d += audit.audited_changes.to_a.map{|a| a.join(" = ")}.join("\n")
      else
        d
      end
    end.uniq.join("\n")
  end
end
