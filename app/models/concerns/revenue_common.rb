module RevenueCommon
  def empty_revenue?
    booking&.created_at.to_date < '2022-06-01'.to_date and return false
    booking.contract_type == 'fix_income' and return false
    zero_revenue = booking_revenues.empty? || extra.dig(:revenue_details, :shareable_revenue).to_f <= 0
    status != 'voided' && zero_revenue # && booking.real_check_out_date == end_date
  end
end
