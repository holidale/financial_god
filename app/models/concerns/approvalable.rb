module Approvalable

  def approval
    approvals.last
  end

  def for_approval_test?
    created_at.to_date < APPROVAL_V2_START # and return false, "PO##{id} It's Not Start Time"
  end

  # Update status etc
  def hook_lark
    if approval&.approved?  # (siblings << self).all?(&:approved?)
      invoice_cost = Cost.find_by(id: cost_id)
      [invoice_cost, self].compact.uniq.map do |c| 
        c.user_id ||= c.audits.first&.user_id rescue nil
        if !c.approved?
          origin_status = c.status.dup
          c.update(status: 'approved', approved_date: Time.current) if !c.paid?
          c.save(validate: false)
          c.audits.create(
            user_id: approval&.final_approver&.id, user_type: 'User', action: 'update',
            audited_changes: YAML.dump({"status" => [origin_status, "approved"]})
          )
        end
      end
      msg = "PO##{id} Approved"
      msg = "Single Invoice from PO##{id} Approved" if single_invoice?
      LarkMsg.enqueue(:po_entire_approved, msg)
      RedisHelper.lpush("po_entire_approved", {cost_id: id})
    end
  end

  def done?
    approved? || paid?
  end

  def approved?
    approval&.approved?
  end

  def rejected?
    approval&.rejected?
  end

  def cancelled?
    approval&.cancelled?
  end

  def cancel
    approval&.cancel
  end

  def item_total_amount
    (cost_items.flat_map{|a| a.cost_sub_items.sum(&:amount)}.sum +
       cost_items.sum(&:amount)).round(2)
  end

  def contractor
    cleaner&.full_name
  end

  def product_user
    service_schedule && service_schedule.audits.find{|a| a.audited_changes["assignee_id"]}&.user
  rescue => e
    user
  end

  def fetch_creator
    default_productor = FeishuObject.admin&.pms_user
    _a = audits.find{|a| a.audited_changes["status"] == ['bill_verification', 'pending']}
    return product_user || _a&.user || default_productor if bill_verification? || _a

    audits.first&.user || user || default_productor
  end

  def po_submitter(creator_id = nil)
    for_approval_test? and return FeishuObject.admin # for Test

    creator_id ||= fetch_creator&.id
    fuser = User.find(creator_id).feishu_user rescue nil
    fuser || audits.find(&:user)&.user&.feishu_user || FeishuObject.admin
  end

  def lark_amount
    item_total_amount
    # (amount + cost_sub_items.sum(&:amount)).round(2)
  end

  def lark_desc
    arr, bs, ws = [], cost_items.map(&:booking_id).uniq, cost_items.map(&:service_schedule_id).uniq
    arr << "Single Invoice PO" if invoice?
    cost_items.each do |ci|
      arr << "#{ci.customer} #{ci.description}: #{PmsRecord.to_currency(ci.amount)}, #{ci.financial_account&.name}"
      cost_sub_items.each do |si|
        arr << "  \n#{si.description}: #{PmsRecord.to_currency(si.amount)}"
      end
      arr << "\n"
    end

    bs.compact.map {|booking_id| arr << "B##{booking_id}" }
    ws.compact.map {|wid| arr << "WorkOrder: #{PmsRecord::SITE}/service_schedules/#{wid}/edit"}
    arr << "PO Link: #{new_po_link}"
    arr.join("\n")
  end

  def new_po_link
    "#{PmsRecord::SITE}/new_pms/costs/#{cost_type_in_new_pms}/option/#{id}"
  end

  def cost_type_in_new_pms
    category == 'purchase_order' ? "po" : "invoice"
  end

  def feishu_object(user)
    user.feishu_user
  end

  def house
    cost_items.first&.house
  end

  def to_approval_form(creator_id = nil)
    fu = po_submitter(creator_id)
    nr = pending_reimbursement? ? "YES" : "NO"
    ae_field_id = for_approval_test? ? "widget16781485639900001" : "widget16620145654170001"
    base = [
      {id: "widget16478504452090001", type: "contact", value: [ fu&.feishu_user_id ]},
      {id: "widget16478506876890001", type: "date", value: bill_date.strftime("%FT00:00:00%z")[0, 22] + ":00"},
      {id: "widget16613349608230001", type: "input", value: contractor },
      {id: "widget1", type: "textarea", value: lark_desc },
      {id: "widget16575172626010001", type: "amount", value: lark_amount },
      # {id: "widget16575173096400001", type: "radioV2", name: "Needs to be reimbursed", value: nr},
      # {id: "widget16575173742370001", type: "radioV2", name: "Payment party", value: customer, },
      # {id: "widget16613371408670001", type: "input", name: "Account", value: financial_account&.name },
      {id: "widget16613371175090001", type: "input", name: "Property", value: house&.qb_name },
      {id: ae_field_id, type: "radioV2", name: "AE", value: house&.ae_user&.full_name || "None"}
    ]

    files.present? ? base << files_form : base
  end

  def files_form
    {
      id: "widget16478504910870001",
      name: "Attachment",
      type: "attachmentV2",
      value: files
    }
  end

  def files
    cost_items.map do |ci|
      ci.scanned_files.map do |sf|
        LarkApp.inner_tool.upload_to_approval(sf.private_url)[:code] if sf.private_url
      end
    end.flatten.compact
  end

  def can_resubmit?
    rejected? || approval.nil? || cancelled?
  end

  def to_lark_approval
    LarkApproval.po.create(id)
  rescue => e
    "PO##{id} to Lark Failed: #{e.message}"
  end
end
