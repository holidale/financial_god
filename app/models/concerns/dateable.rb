module Dateable
  extend ActiveSupport::Concern

  SPECIAL_PERIODS = %w()
  DEFAULT_DATE_COLUMN = "finance_ensure_at"

  included do
    scope :for_today, ->(column = DEFAULT_DATE_COLUMN) {where(column => Date.current)}
    scope :from_date, ->(date, column = DEFAULT_DATE_COLUMN) {where(column => [date..Date.current])}
    scope :for_week, ->(column = DEFAULT_DATE_COLUMN) {from_date(7.days.ago.to_date, column)}
    scope :for_month, ->(column = DEFAULT_DATE_COLUMN) {from_date(30.days.ago.to_date, column)}

    scope :recent_year, ->(column = DEFAULT_DATE_COLUMN) {from_date(1.year.ago.to_date, column)}
    scope :recent_2_year, ->(column = DEFAULT_DATE_COLUMN) {from_date(2.year.ago.to_date, column)}
    scope :recent_3_year, ->(column = DEFAULT_DATE_COLUMN) {from_date(3.year.ago.to_date, column)}

    scope :for_period_like, ->(date, column = DEFAULT_DATE_COLUMN) do
      date.to_s !~ /,/ and return where("#{column} like '#{date}%'")
      sd, ed = date.to_s.split(",").map(&:to_date)
      for_period(sd, ed, column)
    end
    scope :natural_month, ->(month, column = DEFAULT_DATE_COLUMN) {for_period_like(month, column)}
    scope :natural_year, ->(year, column = DEFAULT_DATE_COLUMN) {for_period_like(year, column)}
    scope :for_period, ->(start_at, end_at, column = DEFAULT_DATE_COLUMN) {where(column => (start_at..end_at))}
  end

  def self.parse_period(period)
    period.to_s.size == 4 and return "#{period}-01-01".to_date
    period.to_s.size == 7 and return "#{period}-01".to_date
    period.to_s.size == 10 and return period.to_date
  rescue => e
    puts "parse_period error: --> #{e.message}"
    period
  end
end
