# frozen_string_literal: true

module WorkorderLarkTaskConcern
  extend ActiveSupport::Concern

  # Notify to:
  # new_cleaning_inspection_workorder => Cleaning Inspection Gardening Reminder
  # regional_overdue_wo_North_Carolina => Workorders NorthCarolina
  def lark_outer_msg_key
    return if "Ad Hoc Inspection" == service&.name
    return "new_cleaning_inspection_workorder" unless maintenance_only?
    return if related_region.blank?

    "regional_overdue_wo_#{related_region.name.to_s.gsub(' ', '_')}"
  end

  def create_lark_task(data)
    return if feishu_task.present?

    task_payload = data[:task_payload] || lark_task_payload
    task = LarkTask.new(payload: task_payload).create
    Sidekiq.logger.info("Workorder Create Task:#{id}: task body #{task}")

    if task["code"] == 0
      lark_task_id = task.dig("data", "task", "id")
      feishu_task = FeishuTask.create(workorder_id: id, remote_id: lark_task_id)
      lark_group = lark_task_group
      feishu_task.feishu_objects << lark_group if lark_group.present?

      Sidekiq.logger.info("Workorder Create Task:#{id}: task_id #{lark_task_id}")

      create_lark_task_comments(lark_task_id, notes.pluck(:id))

      update_extra(lark_task_id: lark_task_id)
      save(validate: false)

      if lark_outer_msg_key.blank?
        Sidekiq.logger.info("create_lark_task lark_outer_msg_key blank: #{id}")
        return
      end

      return if data[:skip_send_lark_msg]

      if data[:card_msg].present?
        data.dig(:card_msg, :elements).pop
        data.dig(:card_msg, :elements) << "[Please Click to View](#{lark_task_view_url})"
        msg = data[:card_msg]
      end

      msg ||= create_lark_task_msg
      LarkMsg.enqueue(:msg_for_all_employees, msg) if data[:notify_all_employees]
      LarkMsg.enqueue(lark_outer_msg_key, msg)
    end
  end

  def engineer_open_ids
    %w[shuo eric].map do|name|
      FeishuObject.find_user_with(name)&.open_id
    end
  end

  def lark_task_payload(collaborator_ids: nil, follower_ids: nil)
    group = lark_task_group
    ae = FeishuObject.find_user_with(house.ae_user&.name)
    _assignee = FeishuObject.find_user_with(assignee&.name)

    if group.blank?
      Sidekiq.logger.info("Workorder Create Task:#{id}: group missing #{lark_outer_msg_key}")
    end

    _collaborator_ids = collaborator_ids || Array(ae&.open_id) || Array(_assignee&.open_id)
    _follower_ids = follower_ids || new_lark_task_follower_ids(_collaborator_ids, group)

    if extra[:source] == 'Report an issue from Site' # from main site
      _follower_ids = new_lark_task_follower_ids(_collaborator_ids, big_wo_group)
    end

    base = {
      summary: "Work Order【#{service&.name}】",
      description: lark_task_description,
      origin: {
        platform_i18n_name: {
          zh_cn: "PMS Workorder",
          en_us: "PMS Workorder",
        }.to_json
      },
      collaborator_ids: Array(_collaborator_ids),
      follower_ids: Array(_follower_ids),
    }

    base.merge!(
      due: {
        time: due.beginning_of_day.to_i,
        timezone: (Time.zone.tzinfo.name rescue "America/Los_Angeles")
      }
    ) if due

    base
  end

  def lark_task_description
    [
      "Address: #{house.full_address_us_format}",
      "Assignee: #{assignee&.full_name}",
      "A/E: #{house&.ae_user&.full_name}",
      "Content: #{note}",
      "Contractor: #{cleaner&.name_with_phone_title}",
      "Linker: #{linker_url}",
      "ADD New PO: #{PublicConfigs.end_points.pms}/costs/add_cost?category=purchase_order&house_id=#{house.id}&service_schedule_id=#{id}",
      "PMS Link: #{PublicConfigs.end_points.pms}/service_schedules/#{id}"
    ].join("\n")
  end

  # TODO: better move to pms
  def create_lark_task_msg
    msg = {
      title: "New Work Order Created【#{service&.name}】",
      content: [
        [
          {
            tag: "text",
            text: "ID: #{id}, Address: #{house.full_address_us_format}",
          }
        ],
        [
          {
            tag: "text",
            text: "Status: #{status}, Due: #{due.strftime("%m/%d/%Y")}",
          }
        ],
        [
          {
            tag: "text",
            text: "Assignee: #{assignee&.full_name}, A/E: #{house&.ae_user&.full_name}",
          }
        ],
        *feishu_links
      ],
      msg_type: "post",
    }

    LarkMsg.assemble_post_msg(msg)
  end

  def feishu_links
    [
      [
        {
          tag: "a",
          href: lark_task_view_url,
          text: "[Task Link]",
        },
        {
          tag: "a",
          href: linker_url,
          text: "[Linker]",
        }
      ],
    ]
  end

  def linker_url
    "#{PublicConfigs.end_points.pms}/new_pms/workorder/linker/#{id}"
  end

  def lark_task_view_url
    "#{PublicConfigs.lark.task_url}?guid=#{lark_task_id}"
  end

  def lark_task_id
    extra[:lark_task_id]
  end

  def lark_task_group
    return if service&.name == "Ad Hoc Inspection"
    return all_employees_group if service&.name == "Urgent Requests"
    return FeishuObject.find_user_with("Workorders #{related_region&.name}") if maintenance_only?

    big_wo_group
  end

  def all_employees_group
    FeishuObject.find_by_id(699)
  end

  def big_wo_group
    FeishuObject.find(120)
  end

  def new_lark_task_follower_ids(collaborator_ids, group)
    follower_ids = Array(group&.chat_member_open_ids)
    follower_ids = follower_ids.flatten - collaborator_ids + engineer_open_ids
    follower_ids.uniq.compact
  end

  def update_lark_task(data)
    return if feishu_task.blank?

    payload = update_lark_task_payload(data)

    if payload.present?
      feishu_task.update_remote(payload)
      push_update_lark_task_msg(data[:update_type])
    end

    if lark_task_completed?(data)
      feishu_task.complete_remote
      push_update_lark_task_msg(data[:update_type])
    end

    if create_lark_task_comment?(data)
      note = Note.find(data.dig("data", "note", "id"))
      create_lark_task_comments(feishu_task.remote_id, [note.id])
      push_update_note_lark_task_msg(note, "created")
    end
  end

  def update_lark_task_payload(data)
    case data[:update_type]
    when "due_changed"
      { task: { due: data.dig(:data, :due) }, update_fields: ["due"] }
    end
  end

  def lark_task_completed?(data)
    "completed" == data[:update_type].to_s
  end

  def create_lark_task_comment?(data)
    %w[add_comment].include?(data[:update_type].to_s)
  end

  def create_lark_task_comments(task_id, note_ids)
    return if note_ids.blank?

    notes.where(id: note_ids).each do |note|
      next if FeishuTaskComment.exists?(commentable: note)

      note_content = "[#{note.creator&.name}] #{note.content}"
      comment = LarkTaskComment.new(task_id: task_id, payload: { content: note_content }).create
      comment_id = comment.dig("data", "comment", "id")

      FeishuTaskComment.create(
        commentable: note,
        lark_task_id: task_id,
        lark_comment_id: comment_id
      ) if comment["code"].to_i == 0
    end
  end

  def push_update_lark_task_msg(update_type)
    if lark_outer_msg_key.blank?
      Sidekiq.logger.info("push_update_lark_task_msg lark_outer_msg_key blank: #{id}")
      return
    end

    # Feishu has bug when create lark task, they send many events, we can't find out which event is created event
    # so our code handle events always as "updated event".
    # this will lead to send due_changed event notify, when create lark task,
    # sometimes the due_changed event notify first, but lark_task_id not exist,
    # if lark_task_id blank the msg link can't open,
    # so we should skip send notify when lark_task_id is blank.
    return if lark_task_id.blank?

    LarkMsg.enqueue(lark_outer_msg_key, update_lark_task_msg(update_type))
  end

  # TODO: better move to pms
  def update_lark_task_msg(update_type)
    msg = {
      title: "Work Order【#{service&.name}】#{update_type.to_s.titleize}",
      content: [
        [
          {
            tag: "text",
            text: "Address: #{house.full_address_us_format}",
          }
        ],
        *feishu_links
      ],
    }

    LarkMsg.assemble_post_msg(msg)
  end

  def push_update_note_lark_task_msg(note, type)
    if lark_outer_msg_key.blank?
      Sidekiq.logger.info("push_update_note_lark_task_msg lark_outer_msg_key blank: #{id}")
      return
    end

    LarkMsg.enqueue(lark_outer_msg_key, update_note_lark_task_msg(note, type))
  end

  # TODO: better move to pms
  def update_note_lark_task_msg(note, type)
    msg = {
      title: "Work Order【#{service&.name}】#{type.to_s.titleize} A Note",
      content: [
        [
          {
            tag: "text",
            text: "Content: [#{note&.creator&.name}](#{note&.content.to_s.squish})",
          }
        ],
        *feishu_links
      ],
    }

    LarkMsg.assemble_post_msg(msg)
  end
end
