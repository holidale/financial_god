# all basic operation for column :extra can be dealed with here
# The :extra is a Hash based column which can paly as a very flexable role
# to store many unimportant data or log data
#
module ExtraObjectable
  extend ActiveSupport::Concern
  included do
    serialize :extra, Hash

    def update_extra(hash)
      _extra = extra
      self.extra = _extra.merge(hash)
    end
    alias :assign_on_extra :update_extra

    def extra
      (super || {}).with_indifferent_access
    end
  end
end
