module TimeTrackable

  # STATUS = {
  #   "ServiceSchedule" => {
  #     pending 
  #     scheduled 
  #     submitted 
  #     completed 
  #     not_an_issue 
  #     follow_up 
  #     reviewed
  #   }
  # }

  module ForAudit
    def track_status
      _ac = audited_changes["status"]
      _ac.is_a?(String) and return _ac
      _ac.is_a?(Array) and return _ac.last
      nil
    end

    def track_action
      action == 'create' and return 'start'

      ss = track_status
      case auditable_type
      when 'ServiceSchedule'
        ss == 'completed' and return 'done'
        changed_attr?("assignee_id") and return 'change_assignee'
        changed_attr?("cleaner_id") and return 'change_vendor'
        changed_attr?("review") and return 'assignee_review'
      end
    end

    def changed_attr?(_attr)
      _ac = audited_changes
      (arr = _ac[_attr]).is_a?(Array) and return arr.uniq.size > 1
      false
    end
  end

  def parse_track_times
    _status = nil
    audits.each_with_index do |a, i|
      _status = a.track_status || _status
      action_name = a.track_action
      action_name = "first_response" if i == 1 && a.user_id.present?
      if action_name
        tt = TimeTracker.where(uniq_source_key: "Audit##{a.id}").first_or_create
        tt.update(
          operator_id: a.user_id, trackable_type: self.class.name, trackable_id: id,
          action_name: action_name, status: status, created_at: a.created_at,
        )
      end
    end
  end
end
