module Timeable
  extend ActiveSupport::Concern
  included do
    scope :_on_day, -> (col = 'created_at', day = Date.current) {
      where("? <= #{col} AND #{col} <= ?", day.beginning_of_day, day.end_of_day)
    }
    scope :today, -> (col = 'created_at') {_on_day(col)}
    scope :yesterday, -> (col = 'created_at') {_on_day(col, Date.yesterday)}

    scope :created_on, -> (day = Date.current) {_on_day}
    scope :created_on_today, -> {created_on(Date.current)}
    scope :created_on_yesterday, -> {created_on(Date.yesterday)}

    scope :updated_on_today, -> {_on_day("updated_at", Date.current)}
    scope :updated_on_yesterday, -> {_on_day("updated_at", Date.yesterday)}

    scope :recent_created_in_minutes, ->(n) {where(created_at: n.minutes.ago..Time.current)}
    scope :recent_updated_in_minutes, ->(n) {where(updated_at: n.minutes.ago..Time.current)}
    # scope :recent, ->(limit = 10) {order("created_at DESC").limit(limit)}
  end
end
