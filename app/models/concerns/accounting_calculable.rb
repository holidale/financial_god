module AccountingCalculable
  extend ActiveSupport::Concern
  included do
    scope :for_booking, ->(bookings) {where(booking_id: bookings)}
    scope :for_house, ->(houses) {where(house_id: houses)}

    scope :for_account, -> (account){where(account: account)}
    scope :rental_income, -> {for_account(RENTAL_INCOME_ACCOUNT)}
  end
end
