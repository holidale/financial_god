module GlobalCacheable
  extend ActiveSupport::Concern
  included do
    def self.global_cache
      RedisHelper.get_json("#{self.name}:global_cache") || {}
    end
  end
end
