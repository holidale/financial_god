module AuditMonitor

  def monit!
    _type = auditable_type.tableize.singularize
    __send__("monit_#{_type}").compact.presence rescue nil
  end

  def monit_host_contract
    house = auditable.house
    return unless house.manager_group_id == 2

    _changes = audited_changes
    msg = case action
      when 'create'
        "New Host Contract ##{auditable.id} from #{_changes['effect_date']} to #{_changes['renew_date']},House #{house.short_address}"
      when 'update'
        factors = %w(renew_date effect_date).map do |a|
          next unless (cs = _changes[a]).is_a?(Array)
          "#{a.titleize } Changed from #{cs[0]} to #{cs[1]}"
        end.compact
        "Host Contract ##{auditable.id} #{factors.join(',')},House #{house.short_address}" if factors.present?
      end
    LarkMsg.enqueue(:host_contract_updated, header: false, elements: [msg]) if msg
  rescue => e
    LarkMsg.enqueue(:monit_audits_host_contract_fail, "Monit Host Contract Changes Error #{e.message}")
    false
  end

  def monit_service_schedule
    s = auditable
    title = "Workorder Alert, Operator: #{user&.name || 'System'}【#{s.service&.name}】"
    data = ["**#{title}**"]
    data << "【#{s.status.titleize}】#{s.house.address2} #{LarkMsg.link_to_pms(s, "WO##{s.id}")} Event: #{action}"
    ae = s.house.ae_user&.name
    if aa = FeishuObject.find_user_with(ae)
      aa.notify(header: false, elements: data)
    end
    LarkMsg.enqueue(:"monit_service_schedule_#{ae.to_s.split(" ").first.to_s.downcase}", 
                    header: false, elements: data)
  end

  def monit_payment
    if action == 'update'
      if b = Invoice.find_by(id: auditable_id)
        _changes = audited_changes
        msg = ["**Invoice Amount Changed**"]
        %w(amount).each do |a|
          if (cs = _changes[a]).is_a?(Array)
            msg << "Invoice##{auditable_id} #{a.titleize} Changed from #{cs[0]} to #{cs[1]}"
          end
        end
        LarkMsg.enqueue(:invoice_amount_updated, header: false, elements: msg) if msg.size > 1
      end
    end
  end

  def monit_booking
    if b = Booking.find_by(id: auditable_id)
      if action == 'update'
        _changes = audited_changes
        msg = ["B##{LarkMsg.link_to_pms(b)} Special Data Updated"]

        %w(departure_date arrival_date status).each do |a|
          if (cs = _changes[a]).is_a?(Array)
            msg << "#{a.titleize }Changed from #{cs[0]} to #{cs[1]}"
          end
        end
        LarkMsg.enqueue(:booking_updates, header: false, elements: msg) if msg.size > 1
      end
    end
    b.house.notify_pms_data_update("earliest_available_date_for_house") rescue nil
    monit_user_change_houses
  end

  def monit_user_change_houses
    b = Booking.find_by(id: auditable_id)
    return if b.blank?
    return if action == "destroy"
    return if b.status != "booked"
    bs = b.user.bookings.pure_booked.where("id != ? and house_id != ? and COALESCE(bookings.departure_date, bookings.check_out) >= ? and COALESCE(bookings.arrival_date, bookings.check_in) <= ?", b.id, b.house_id, b.real_check_in_date, b.real_check_out_date)
    return if bs.size == 0
    msg = ["B##{LarkMsg.link_to_pms(b)} User Maybe Change House"]
    msg << "New House Booking ID List #{bs.map{|bb| bb.id }.join(',')}"
    LarkMsg.enqueue(:user_change_houses, header: false, elements: msg)
  rescue => e
    msg = ["B##{LarkMsg.link_to_pms(b)} monit_user_change_houses error #{e.message}"]
    LarkMsg.enqueue(:err_user_change_houses, header: false, elements: msg)
  end

  def monit_cost
    c = auditable
    if action == 'update'
      a, b = audited_changes["holidale_expense"]
      if c&.finished? && (a != b)
        msg = "PO##{LarkMsg.link_to_pms(c)} amount changed after approved"
        LarkMsg.enqueue(:po_amount_changed, header: false, elements: [msg])
      end

      a, b = audited_changes["status"]
      if b == 'approved' && (a != b) && c&.holidale_expense.abs.to_f > 500
        msg = "PO##{LarkMsg.link_to_pms(c)} amount is #{PmsRecord.to_currency(c&.holidale_expense)}, which exceeds $500 and has already been approved"
        LarkMsg.enqueue(:po_amount_exceeds_and_approved, header: "PO Approved", elements: [msg])
      end
    elsif action == "create"
      if b = c.user.bookings.pure_booked.find{|b| b.extra.dig(:youtube_tv_info, :cost_id) == c.id}
        msg = "B##{b.id} PO##{LarkMsg.link_to_pms(c)} Guest: #{b.user.name}"
        LarkMsg.enqueue(:po_from_youtube_tv, header: "New YouTubeTv PO Created", elements: [msg])
      end
      LarkMsg.enqueue(:po_bill_verification, "New Bill Verification PO##{c.id}") if c&.bill_verification?

      if audited_changes["status"] == 'approved' && c&.holidale_expense.abs.to_f > 500
        msg = "PO##{LarkMsg.link_to_pms(c)} amount is #{PmsRecord.to_currency(c&.holidale_expense)}, which exceeds $500 and has already been approved"
        LarkMsg.enqueue(:po_amount_exceeds_and_approved, header: "PO Approved", elements: [msg])
      end
    end

  end

  def monit_cost_item
    if action == 'destroy'
      cid = auditable_id
      c = audited_changes['cost_id']
      if as = Approval.where(cost_item_id: cid).presence
        as.map(&:cancel)
        msg = "CostItem #{cid} Cost##{c} from Audit##{id} Cancelled"
        LarkMsg.enqueue(:po_approval_cancelled, msg)
        msg
      end
    end
  end

  def monit_quotation
    if action == 'destroy'
      cid = auditable_id
      msg = "Quote##{cid} B##{associated_id} in Audit##{id} Was Deleted"
      LarkMsg.enqueue(:quote_delete, msg)
      msg
    end
  end

  # 2 conditions
  #   1. Passed -> Unchecked # when extension
  #   2. any status -> Request_change
  def monit_booking_audit
    msg = ""
    !(ac = audited_changes["status"]).present? and return
    b = Booking.find_by(id: associated_id)
    !b&.booked? and return
    link = LarkMsg.link_to_pms(b)
    if ac == [0,3]
      msg = "BookingAudit for B##{link} Changed to Unchecked"
      auditable.user.feishu_user.notify(msg) rescue nil
    end

    if ac == 1 || (ac.is_a?(Array) && ac.last == 1)
      un = auditable.user&.full_name || "System"
      msg = "#{un} make a Request Change for Your Booking B##{link}" 
      (b.referrer || b.creator).feishu_user.notify(msg) rescue nil
    end

    LarkMsg.enqueue(:booking_audit_change, header: false, elements: [msg]) if msg.present?
  end

  def monit_note
    type, _id = associated_type, associated_id
    _user = user
    _user ||= auditable&.user rescue nil
    _user ||= auditable&.creator rescue nil
    msg = ["#{_user&.full_name || 'System'} #{action} Note: #{type}##{_id}"]
    msg << "B##{LarkMsg.link_to_pms(type.constantize.find_by(id: _id)&.booking)}" if type == 'BookingAudit'
    if _content = audited_changes["content"].presence
      msg << _content
      LarkMsg.enqueue(:note_from_audit, header: false, elements: msg)
    end
  end

  def monit_contract_amendment
    house = auditable&.house
    _changes = audited_changes
    msg = "Extension #{action}d ##{auditable_id}, B##{auditable&.booking_id}, House #{house&.short_address}"
    LarkMsg.enqueue(:contract_amendment_updated, header: false, elements: [msg])
  end

  def monit_region
    return unless audited_changes.key?("name")

    was_name, cur_name = audited_changes["name"]
    return if [was_name, cur_name].any?(&:blank?)

    wo_group = FeishuObject.find_user_with("Workorders #{was_name}")
    return if wo_group.blank?

    wo_group.alias_names = (wo_group.alias_names.to_s.split(",") << "Workorders #{cur_name}").join(",")
    wo_group.outer_msg_keys = (wo_group.outer_msg_keys.to_s.split("\r\n") << "regional_overdue_wo_#{cur_name.gsub(' ', '_')}").join("\r\n")
    wo_group.save
  end
end
