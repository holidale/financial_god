module RevenuePerAvailableRoomConcern
  def covers_between(s, e, b)
    _gap = b.real_check_out_date <= e && b.real_check_out_date >= s ? b.gap_days : 1
    ([b.real_check_out_date, e].min - [b.real_check_in_date, s].max + _gap)
  end

  def room_available(s, e)
    contracts = host_contracts.select{ |hc| hc.effect_date <= e && hc.renew_date >= s }
    if s == e
      return 1 - invalid_days(s, e) if contracts.length > 0
      return 0
    end
    total = contracts.inject(0) { |total, contract| total + ([contract.renew_date, e].min - [contract.effect_date, s].max + 1) }
    total -= invalid_days(s, e)
  end

  def room_sold(s, e)
    bs = booked_bookings_by_date_range(s, e)
    if s == e
      return 1 if bs.length > 0
      return 0
    end
    bs.inject(0) { |total, b| total + covers_between(s, e, b)}
  end

  def occupancy(s, e)
    room_sold(s, e) / room_available(s, e).to_f rescue 0
  end

  def total_rental(s, e)
    bs = booked_bookings_by_date_range(s, e)
    bs.inject(0) { |total, b| total + (b.shared_owner_revenue[0] rescue 0) * covers_between(s, e, b) / b.los.to_f }
  end

  def total_revenue(s, e)
    bs = booked_bookings_by_date_range(s, e)
    bs.inject(0) { |total, b| total + (b.total_revenue_kpi_export rescue 0) * covers_between(s, e, b) / b.los.to_f }
  end

  # Total Rental Revenue / Room Sold
  def adr(s, e)
    total_rental(s, e) / room_sold(s, e).to_f rescue 0
  end

  # Total Revenue / Room Sold
  def tadr(s, e)
    total_revenue(s, e) / room_sold(s, e).to_f rescue 0
  end

  # Total Rental Revenue / Room Available
  def revpar(s, e)
    total_rental(s, e) / room_available(s, e).to_f rescue 0
  end

  # Total Revenue / Room Available
  def trevpar(s, e)
    total_revenue(s, e) / room_available(s, e).to_f rescue 0
  end

  def house_area
    area = nil
    case state&.downcase
    when 'ca'
      if city == "irvine"
        area = "irvine"
      else
        area = "ca"
      end
    when 'fl', 'tx'
      area = state&.downcase
    when 'or', 'nv'
      area = "nv_or"
    end

    area
  end

  def bedroom_num_key
    if self.bedroom == 0
      return 0
    elsif self.bedroom <= 4
      return self.bedroom
    else
      return 5
    end
  end

  def invalid_days(s, e)
    bs = bookings.select{ |b| Booking.long_time_blocked_status.include?(b.status) && b.check_out >= s && b.check_in <= e}
    bs.inject(0) { |total, b| total + covers_between(s, e, b) }
  end

  def booked_bookings_by_date_range(s, e)
    bookings.select{ |b| b.status == "booked" && b.real_check_out_date >= s && b.real_check_in_date <= e}
  end

end
