module Auditable
  def audits
    Audit.where(auditable_type: self.class.name, auditable_id: id)
  end
end
