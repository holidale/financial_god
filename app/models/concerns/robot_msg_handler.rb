module RobotMsgHandler
  SITE = PmsRecord::SITE
  def handle_worker_msg(task_body)
    Sidekiq.logger.info("TaskBody in #{id} is #{task_body.to_json}")
    send("handle_#{worker.tableize}", task_body.with_indifferent_access) 
  rescue => e
    notify_robots parse_results(task_body[:data].to_json)  # if not handled, use common
    FeishuObject.admin.notify("Handle Worker error: #{worker} --> #{e.message}")
    Sidekiq.logger.info("ROBOT Worker #{worker} is not handled now, #{e.message}")
  end

  def handle_robot_monit_houses_changes_workers(task_body)
    notify_arr_data_with_pure_title("Houses Monitor", task_body)
  end

  def handle_robot_monit_heart_beat_workers(task_body)
    notify_arr_data false, task_body[:data]
  end

  def handle_robot_monit_sql_related_workers(task_body)
    notify_arr_data_with_pure_title("Server Warning", task_body)
  end

  def handle_robot_monit_pms_status_workers(task_body)
    data = task_body[:data] || []
    data.unshift("**Service Error**") if data.present?
    notify_arr_data(false, data)
  end

  def handle_robot_monit_expired_services_workers(task_body)
    notify_arr_data("Expires List", task_body[:data])
  end

  def handle_robot_monit_sales_data_workers(task_body)
    notify_arr_data(false, task_body[:data])
  end

  def handle_robot_monit_salesforce_data_workers(task_body)
    notify_arr_data_with_pure_title("SalesForce", task_body)
  end

  def handle_robot_monit_fix_exception_data_workers(task_body)
    notify_arr_data("Exception Data", task_body[:data])
  end

  def handle_robot_monit_po_workers(task_body)
    notify_arr_data("PO Approvals", task_body[:data])
  end

  def handle_robot_monit_top_summary_workers(task_body)
    notify_arr_data(false, task_body[:data])
  end

  def handle_robot_for_get_booked_bookings_workers(task_body)
    task_body[:data]&.map do |a|
      obj = Booking.find_by(id: a[:booking_id]) || ContractAmendment.find_by(id: a[:contract_amendment_id])
      notify_robots(header: false, elements: obj.data_for_robot_msg_content)
      obj.assign_on_extra(lark_broadcast: Time.current) && obj.save 
    end
  end

  def handle_robot_monit_approvals_workers(task_body)
    as = task_body[:data]&.map do |a|
      a.is_a?(String) ? a : "Approvals##{a[:id]} from #{a[:user_name]}: #{a[:status]}"
    end
    notify_arr_data("Got Approvals", as)
  end

  def handle_robot_monit_audits_workers(task_body)
    notify_arr_data("Danger Operations:", task_body[:data])
  end

  def handle_robot_monit_owner_revenue_pdf_workers(task_body)
    pdfs = task_body[:data]&.map do |file|
      file = file.split("/").last
      month, hid = file.scan(/(\d{4}-\d{2}-\d{2})|(\d+).pdf/).flatten.compact
      house = House.find_by(id: hid)
      "[H##{hid} #{house&.address2}](#{SITE}/houses/#{hid}) for month #{month[0, 7]}"
    end
    notify_arr_data("OwnerStatements Below Sent", pdfs)
  end

  def handle_robot_monit_new_invoices_workers(task_body)
    bid = task_body[:booking_id]
    invoice_ids = "[#{task_body[:invoice_ids]}](#{SITE}/bookings/#{bid}/show_and_edit)"
    msg = {header: "Booking##{bid}", elements: [
      {fields: {"New Invoices" => invoice_ids}}
    ]}
    notify_robots(msg)
  end

  def handle_robot_monit_qb_sync_failed_items_workers(task_body)
    if q = QbSyncRecord.find_by(id: task_body[:qb_sync_record_id])
      q.skip_alert? and return
      msg = "QB Notice: #{q.fail_type_desc}\n"
      msg += "Sync Key: #{q.key}\nMsg: "
      msg += q.extra["err_msg"]
      notify_robots msg
    end
  end

  def handle_robot_monit_deploy_pms_workers(task_body)
    if hotfixes = task_body[:data].delete(:HotFix) rescue []
      LarkMsg.enqueue(:release_hotfix, header: "Hotfix", elements: hotfixes)
    end
    if keys = (task_body[:data].delete(:keys) rescue []).presence
      # notify_robots parse_results(task_body[:data])
      tasks = JiraTask.where(key: keys)
      msgs = ["**Features/Improvement List**"]
      (fs = tasks.select{|a| a.issuetype.to_s.downcase != 'bug'}).each_with_index do |tt, index|
        msgs << "  #{index + 1}. #{Jira.link_task(tt)}"
        video, istct = tt.video.presence, tt.instruction.presence
        msgs << "[Instruction](#{istct})#{video ? ", [Video](#{video})" : ""}" if video || istct
      end
      if bs = (tasks - fs).presence
        msgs << "**BugsFixed List**"
        (tasks - fs).each_with_index do |tt, index|
          msgs << "  #{index + 1}. #{Jira.link_task(tt)}"
        end
      end
      
      notify_arr_data("PMS New Release For #{Date.current.strftime("%m/%d/%Y")}", msgs)
      finish_jira_tasks(keys)
    end
  end

  # Notify Test User
  def handle_robot_monit_jira_tests_workers(task_body)
    notify_arr_data_with_pure_title("Major Test Tasks", task_body)
  end

  def handle_robot_monit_jira_process_workers(task_body)
    holiday? and return
    notify_arr_data(false, task_body[:data])
  end

  def month_end?
    Date.current == Date.current.end_of_month
  end

  def handle_robot_monit_near_check_in_booking_workers(task_body)
    if task_body[:data].is_a?(String)
      return notify_robots(task_body[:data])
    end

    eles = task_body[:data].map do |booking_id|
      b = Booking.find(booking_id)
      "【H##{b.house_id} #{b.house&.address2}】[B##{booking_id}](#{SITE}/bookings/#{booking_id}/show_and_edit)"
    end
    notify_arr_data("Tomorrow Check In List", eles)
  end

  def finish_jira_tasks(keys)
    keys.map do |a| 
      if issue = CommandCaller.call_ruby("jira.rb", "get_issue", a).presence
        at_issue_monitors(issue)
      end
      Jira.close_issue(a)
    end
  end

  def at_issue_monitors(issue)
    desc = issue["fields"]["summary"]
    (issue["fields"]["customfield_10911"] || []).each do |real_reporter|
      FeishuObject.find_user_with(real_reporter).notify(header: "Your Issue Has Been Resolved", elements: [desc])
    end

    FeishuObject.find_user_with(issue["fields"]["assignee"]['displayName'].to_s.downcase).notify(
      header: "Your Task Has Been Deployed", elements: [desc])
  rescue => e
    "No Issue Real Reporter"
  end

  def handle_robot_monit_outer_msg_workers(task_body)
    arr = wrapper_notify_list(task_body[:data])
    arr&.each do |task|
      if meta_msg = (JSON.parse(task) rescue {}).presence
        msg = parse_external_msg(meta_msg['type'], meta_msg['body'])
        notify_robots(msg, objects_need_to_notify(meta_msg))
        # hook_final_external_msg(meta_msg['type'], msg, arr)
      end
    end
  end

  # original msg from external
  def parse_external_msg(type, msg)
    msg.is_a?(String) and return format_string_msg(type, msg)
    !msg.is_a?(Array) and return msg
    eles = msg.map do |m|
      m.is_a?(String) ? format_string_msg(type, m) : {fields: m}
    end
    {header: "External Msg For #{type.titleize}", elements: eles}
  end

  ACTION_EVENTS = YAML.load_file("config/actions.yml")
  def format_string_msg(type, msg)
    type != 'track_action' and return msg
    a, b = msg.split(/GET|POST|DELETE|PUT|PATCH/)
    _action, _path, data = b.split(" ", 3)
    aes = ACTION_EVENTS
    s = aes.dig("full", "#{_action}-#{_path}") || [
      aes[_action] || _action, aes[_path] || _path
    ].compact.join(" ").presence
    [a, s, data].compact.map(&:strip).join(" ")
  rescue => e
    msg
  end

  def wrapper_notify_list(list)
    !list.empty? and return list.uniq
    []
  end

  NOT_USED = %w(overdue_bookings) # ever used but now no need to alert
  def objects_need_to_notify(meta_msg)
    _type = meta_msg['type'] || 'test'
    NOT_USED.include?(_type) and return []

    Sidekiq.logger.info("Notity Outer Msg with type: #{_type}")
    arr = (robots + feishu_objects).select do |a| 
      a.outer_msg_keys.to_s.split.any?{|b| (_type =~ /#{b}/ && _type !~ /\d+/) || _type == b }
    end 
    arr << FeishuObject.admin if !RedisHelper.smembers("admin_skipped").any?{|b| _type =~ /#{b}/   }

    arr.uniq
  end

  def hook_final_external_msg(_type, meta_msg, arr)
  end

  def notify_arr_data_with_pure_title(title, task_body)
    data = task_body[:data] || []
    data.unshift("**#{title}**") if data.present?
    notify_arr_data(false, data)
  end

  def notify_arr_data(title, eles)
    eles.empty? and return
    msg = {header: title, elements: eles}
    notify_robots(msg)
  end
end
