module RobotBasicMsg
  def basic_card_fields(original_data = false)
    _base = -> { JSON.parse(to_json).reject{|k,v| v.is_a?(String) && v.size > 200}}
    case self.class.name
    when "House"
      base = {
        CurrentBooking: LarkMsg.link_to_pms(current_booking),
        Title: title,
        Address: LarkMsg.link_to_pms(self, info_for_ar_due), 
        ContractType: contract_type, AE: ae_user&.full_name,
        Landlord: landlord&.full_name, Monthly: PmsRecord.to_currency(monthly),
        earliest_available: earliest_available_date_for_house.to_s
      }
      base.merge!(Hash[amenity_items.map{|item| [item, amenity&.send(item)]}]) if amenity_items.present?
    when "Booking"
      base = {ID: LarkMsg.link_to_pms(self)}
      base.merge!(Hash[data_for_robot_msg.map{|k, v| [k.to_s.titleize, v]}])
      base["Address"] = LarkMsg.link_to_pms(house, base["Address"])
      # return house.basic_card_fields + ['divider', {fields: base.merge(Status: status)}]
      base.merge!(
        QuotationID: quotation&.id, Status: status, Source: booking_source, 
        ContractType: contract_type, 
        QuoteType: quotation&.form_type, IsIndividual: is_individual_guest?, Org: org&.name,
        RealLeaseTerm: real_lease_term
      )
      return [{fields: base}] + 
        ["divider", "**Invoices**"] + invoices.effective.map{|a| a.lark_attrs.values.join(" | ")} + 
        ["divider", "**Revenues**"] + booking_revenues.map{|a| a.lark_attrs.values.join(" | ")}
    when "Invoice"
      ac = account_record
      base = {
        Booking: LarkMsg.link_to_pms(booking), Status: status, 
        Amount: PmsRecord.to_currency(total_receivable), 
        BalanceDue: PmsRecord.to_currency(balance_due), InvoiceDate: invoice_date
      }
      base = ac ? base.merge(Customer: "#{ac.responser_type}:#{ac.responser_name}") : base
    when "User"
      base = {
        FullName: full_name, Roles: roles.map(&:name).join(","),
        Email: email, Phone: phone, Username: username
      }
      b = bookings.booked.last
      last_booking_msg = b&.basic_card_fields || []
      return [{fields: base}, "divider", 
              "**Last Booking**: #{LarkMsg.link_to_pms(b)}"] + last_booking_msg
    when "Approval"
      _forms = form.map{|a| [a[:name], a[:value]]}.select{|a| a.last.to_s !~ /feishu|asset|png|pdf|jpg/ }
      base = {
        approval_name: approval_name, serial_number: serial_number, 
        status: status, User: FeishuObject.find_user_with(open_id)&.name,
      }.merge(Hash[_forms])
    when "Cost"
      base = _base.call.merge(
        "due_date" => bill_date.to_s[0, 10], "due_date" => due_date.to_s[0, 10],
        CostItemsCount: cost_items.size, CostItemsIDs: cost_items.map(&:id).join(","),
        AllCostCount: all_costs.size
      )
    when "Cleaner"
      base = _base.call
      %w(last_sync_time qbid greenland_qb_id).map{|a| base.delete(a)}
      base
    else
      base = _base.call
    end
    original_data ? base : [{fields: base}]
  end

end
