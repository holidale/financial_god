# frozen_string_literal: true

module FeishuObjectTaskConcern
  extend ActiveSupport::Concern

  included do
    has_many :feishu_task_feishu_object_relationships
    has_many :feishu_tasks, through: :feishu_task_feishu_object_relationships
  end
end