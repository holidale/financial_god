# frozen_string_literal: true

class ContractorCategory < PmsRecord

  has_many :contractor_category_relationships
  has_many :cleaners, through: :contractor_category_relationships, source_type: :Cleaner, source: :relatable
  has_many :services, through: :contractor_category_relationships, source_type: :Service, source: :relatable

end
