class Amenity < PmsRecord
  belongs_to :house
  has_many :amenity_translations

  def trash_day_in_words
    Date::DAYNAMES[trash_day] if trash_day == 0
  end

  def recycle_day_in_words
    Date::DAYNAMES[recycle_day] if recycle_day == 0
  end

  def parking
    amenity_translations.find_by(locale: I18n.locale)&.parking
  end

  def parking_note
    amenity_translations.find_by(locale: I18n.locale)&.parking_note
  end
end
