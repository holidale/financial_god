class WorkorderMessage < PmsRecord
  include ExtraObjectable

  belongs_to :workorder, class_name: :ServiceSchedule, foreign_key: :workorder_id
  belongs_to :call_log, primary_key: :sid, foreign_key: :sid
  belongs_to :short_message_log, primary_key: :sid, foreign_key: :sid

  enum msg_type: {
    text: 0,
    file: 1,
    call: 2,
    group_add_member: 10,
    group_remove_member: 11
  }

  enum source: {
    note: 0,
    sms: 1,
    voice: 2,
    event: 3
  }

  %w(from to direction).each do |a|
    define_method a do
      extra[a]
    end
  end

end
