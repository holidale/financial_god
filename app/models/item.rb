class Item < PmsRecord
  has_many :booking_items
  has_many :bookings, through: :booking_items

  validates :sku, uniqueness: true
  validates_numericality_of :price, :expense, greater_than_or_equal_to: 0
  validates :sku, :name, :brand, presence: true
  enum cost_center: { general: 0, landlord: 1, pm: 2 }
  enum category: { payment: 0, product: 1, deposit: 2 }
end
