class Quotation < PmsRecord
  include ExtraObjectable
  include RevenueCommon

  belongs_to :booking
  has_many :booking_revenues, as: :revenueable
  enum form_type: {
    breakdown: "breakdown",
    all_inclusive: "all_inclusive"
  }

  def apply_agency_labor?
    booking.send_negotiated_invoice && booking.apply_agency_labor?
  end

  def agency_labor_as_booking_fee?
    agency_labor_calculator.agency_labor_flat_rate_per_booking?
  end

  def rent_with_agency_labor?
    apply_agency_labor? && !agency_labor_as_booking_fee?
  end

  def agency_labor_calculator
    booking.agency_labor_event || organization
  end

  def total_agency_labor
    return 0 unless apply_agency_labor?
    return 0 if agency_labor_calculator.blank?

    agency_labor_calculator.total_agency_labor(self) rescue 0
  end

  def daily_agency_labor
    total_agency_labor / los.to_f
  end

  def pet_fee
    o = super.to_i
    o > 0 ? o : negotiated_pet_fee || guest_pet_fee
  end

  def cleaning_fee
    o = super.to_i
    o > 0 ? o : negotiated_cleaning_fee || guest_cleaning_fee
  end

  def pet_rent
    negotiated_pet_rent || guest_pet_rent
  end
end
