class Audit < PmsRecord
  include Dateable
  include AuditMonitor
  include TimeTrackable::ForAudit
  belongs_to :user
  belongs_to :auditable, polymorphic: true

  def is_danger?
    destroy_obj? and return true
    auditable&.danger_detail.present? rescue false
  end

  def user_name
    user&.full_name || "System"
  end

  def destroy_obj?
    action == "destroy"
  end

  def auditable
    super rescue nil
  end

  def audited_changes
    YAML.load super
  end

  def object_creator
    User.find_by(id: audited_changes['user_id'] || audited_changes['creator_id'])
  end

  def danger_msg
    !is_danger? and return
    destroy_obj? && user == object_creator and return

    msg = "#{user_name}##{user_id} #{action} #{auditable_type}##{auditable_id} #{auditable&.danger_detail_full_msg(self)}"
    action == "destroy" ? "#{msg} Creator:#{object_creator&.full_name || "None"}" : msg
  end

end
