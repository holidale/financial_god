class User < PmsRecord
  belongs_to :organization
  has_one :feishu_object
  has_many :bookings
  has_many :houses

  include QuickbookUserable
  has_and_belongs_to_many :roles, join_table: :users_roles

  scope :pms_users, -> {where("email like ? or email like ?", '%holidale.com', '%month2month.com')}
  scope :active, -> {where(active: true)}

  def shared_session_key
    "SharedSession:#{id}"
  end

  def pms_logout?
    !RedisHelper.get(shared_session_key).present?
  end

  def feishu_user
    feishu_object and return feishu_object

    name, domain = email.to_s.split("@")
    another_domain = domain == 'holidale.com' ? 'month2month.com' : 'holidale.com'
    User.find_by(email: "#{name}@#{another_domain}")&.feishu_object
  end

  def full_name
    "#{first_name.to_s.humanize.titleize} #{last_name.to_s.humanize.titleize}".squish
  end  
  alias :name :full_name

  def full_name_with_email
    "#{full_name}(#{email})"
  end

  def self.auth_from_pms(auth)
    auth.presence and return find_by(id: RedisHelper.get(auth))
  end

  def debugger?
    !Rails.env.production? || email == DEBUGGER_EMAIL
  end

  def invoices_summary(period = nil)
    balance_dues = AccountRecord.invoices.for_individual(self)
    !balance_dues.present? and return Invoice.none
    is = Invoice.where(id: balance_dues.map(&:record_source_id)).includes(booking: [:user, :house])
    period ? is.for_period_like(period, :due_date) : is
  end

  %w(engineer admin).each do |a|
    define_method "is_#{a}?" do
      roles.any?{|role| role.name == a}
    end
  end
end
