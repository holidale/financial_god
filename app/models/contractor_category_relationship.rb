# frozen_string_literal: true

class ContractorCategoryRelationship < PmsRecord
  belongs_to :contractor_category
  belongs_to :relatable, polymorphic: true
end

