class Cleaner < PmsRecord
  has_many :houses
  has_many :cleaning_schedules
  has_many :service_schedules
  has_many :costs
  has_many :notes, as: :notable, dependent: :destroy
  has_many :scanned_files, as: :relatable, dependent: :destroy
  has_many :recurring_work_orders, dependent: :destroy
  has_many :cleaners_regions
  has_many :coverage_regions, through: :cleaners_regions, source: :region
  has_many :contractor_category_relationships, as: :relatable
  has_many :contractor_categories, through: :contractor_category_relationships, as: :contractor_category
  belongs_to :user

  scope :by_contractor_category, ->(category_id) {
    joins(:contractor_categories).where(contractor_categories: { id: category_id })
  }

  scope :by_coverage_region, -> (region_id) {
    return where(nil) if region_id.blank?
    joins(:coverage_regions).where(regions: { id: region_id })
  }

  scope :active_cleaners, -> {
    where(status: true)
  }

  def name_with_phone_title
    "#{self.full_name} (#{self.profession} #{self.phone})"
  end
end
