# frozen_string_literal: true

class CleanersRegion < PmsRecord
  belongs_to :cleaner
  belongs_to :region
end

