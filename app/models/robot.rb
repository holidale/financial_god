class Robot < ApplicationRecord
  has_and_belongs_to_many :tasks, join_table: :robots_robots_tasks, class_name: "RobotTask"
  enum platform: %w(lark)

  def token_in_url
    url.to_s.split("/").last
  end

  def run_with_msg(msg)
    LarkRobot.new(self).send_msg(msg)
  end
  alias :notify :run_with_msg

  def self.special_actions
    true
  end
end
