class FeePolicy < PmsRecord
  has_many :bookings
  has_and_belongs_to_many :organizations
  enum contract_contact: %w(guest organization).map { |c| [c, c] }.to_h
end
