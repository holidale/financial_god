class QbSyncRecord < PmsRecord
  include ExtraObjectable
  # belongs_to :user
  belongs_to :qb_object

  enum sync_type: %i(qb_create qb_update pms_update)
  enum status: %i(not_start pending success qb_fail other)
  enum data_category: {
    invoice_new: "Invoice-new",
    je_report_new: "JE Report-new",
    invoice_modify: "Invoice-modify",
    je_report_modify: "JE Report-modify",
    voided_invoice: "Voided Invoice",
    bad_debt_invoice: "Bad Debt Invoice",
    receive_payments_to_pms: "Payments to PMS",
  }

  scope :for_invoice, -> (invoice_id) {where("`key` like '#{invoice_id}%'")}
  scope :recent, -> {order("id DESC")}
  scope :recent_synced_in_minutes, ->(n) {where(updated_at: n.minutes.ago..Time.current)}
  
  def fail_type_desc
    receive_payments_to_pms? and return "Match QB Payment Error"
    invoice_new? || invoice_modify? and return "Sync Invoice Error"
    je_report_new? || je_report_modify? and return "Sync JE Error"
    data_category.to_s.titleize
  end

  def rp_total_applied?
    receive_payments_to_pms? && extra['err_msg'].to_s =~ /unapplied amount/ &&
      extra["original_data"].all?{|a| ReceivedPayment.find_by(qbid:a["QbId"]).status == "applied"}
  end

  def skip_alert?
    rp_total_applied? || voided_invoice? || extra[:err_msg].to_s =~ /Connnect error|Account Period Closed|is already fully paid/
  end
end
