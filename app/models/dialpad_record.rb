class DialpadRecord < ApplicationRecord

  def self.parse_remote_csv_and_save(csv_url)
    return if csv_url.blank?
    csv_data = URI.open(csv_url) do |file|
      CSV.parse(file, headers: true)
    end

    csv_data.each do |row|
      time_str = row['date']
      next if time_str.blank?
      message_id = row['message_id']
      time = DateTime.strptime(time_str, "%Y-%m-%d %H:%M:%S.%N")
      next if row['from_phone'].to_s.length <= 7
      next if DialpadRecord.find_by(message_id: message_id).present?
      DialpadRecord.create!(
        record_time: time,
        message_id: message_id,
        name: row['name'],
        email: row['email'],
        target_type: row['target_type'],
        target_id: row['target_id'],
        sender_id: row['sender_id'],
        direction: row['direction'],
        to_phone: row['to_phone'],
        from_phone: row['from_phone'],
        encrypted_aes_text: row['encrypted_aes_text'],
        mms: row['mms'],
        timezone: row['timezone']
      )
    end
  rescue => e
    LarkMsg.enqueue(:parse_dialpad_remote_csv_and_save_error, "#{e.message}")
  end
end
