class FeishuTask < ApplicationRecord
  belongs_to :workorder, class_name: :ServiceSchedule

  has_many :feishu_task_feishu_object_relationships
  has_many :feishu_objects, through: :feishu_task_feishu_object_relationships

  def push_completed_event_to_pms
    return if workorder.blank? || workorder.completed?
    return unless remote_completed?

    RedisHelper.lpush("LarkEvent", {
      event_for: "task",
      object_type: "ServiceSchedule",
      object_id: workorder_id,
      event_type: :completed,
      data: {
        completed: remote_completed?
      }
    })

    workorder.push_update_lark_task_msg("Completed")
  end

  def update_remote(payload)
    log_lark_api_res(__method__) do
      lark_task.update(payload)
    end
  end

  def complete_remote
    log_lark_api_res(__method__) do
      lark_task.complete
    end
  end

  def uncomplete_remote
    log_lark_api_res(__method__) do
      lark_task.uncomplete
    end
  end

  def remote_completed?
    lark_task_remote_data["complete_time"].to_i > 0
  end

  def lark_task
    @lark_task ||= LarkTask.new(task_id: remote_id)
  end

  def lark_task_remote_data
    @lark_task_remote_data ||= lark_task.remote_data.dig("data", "task")
  end

  def log_lark_api_res(name, &request)
    res = request.call.with_indifferent_access
    if res[:code] != 0
      Sidekiq.logger.info("FeishuTask##{name}:#{id} failed: #{res}")
    end
    res
  end

  class << self
    def init_feishu_object_relationships
      FeishuTask.find_each do |task|
        feishu_object = task.workorder&.lark_task_group
        next if feishu_object.blank?

        feishu_object.feishu_tasks << task
      end
    end
  end
end
