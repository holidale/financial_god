class FeishuTaskComment < ApplicationRecord
  belongs_to :commentable, polymorphic: true

  def remote_creator_id
    remote_data[:creator_id]
  end

  def remote_data
    data = lark_task_comment.remote_data
    if data["code"] != 0
      return {}
    end

    data.dig("data", "comment").with_indifferent_access
  end

  def lark_task_comment
    @lark_task_comment ||= LarkTaskComment.new(task_id: lark_task_id, comment_id: lark_comment_id)
  end
end
