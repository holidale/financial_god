class FeishuObject < ApplicationRecord
  include ExtraObjectable
  include FeishuObjectTaskConcern

  has_and_belongs_to_many :tasks, join_table: :feishu_objects_robot_tasks, class_name: "RobotTask"
  enum remote_type: %i(user group_chat p2p_chat file)
  
  before_save :sync_remote_info
  before_save :check_outer_msg_keys
  after_commit :cache_open_ids

  scope :user_and_groups, -> {where(remote_type: %w(user group_chat))}

  def cache_open_ids
    RedisHelper.cache_json!("LarkUser::#{open_id}", json_base_open_id) rescue nil
  end

  def json_base_open_id
    {id: id, name: name, avatar: extra.dig(:user_info, :avatar, :avatar_240)}
  end

  def pms_user
    User.find_by(id: user_id)
  end

  def feishu_user_id
    %w(content user_info).each do |a|
      _id = extra.dig(a, "user_id") and return _id
    end
  end

  def check_outer_msg_keys
    !outer_msg_keys.present? and return
    outer_worker = RobotTask.find_by(worker: "RobotMonitOuterMsgWorker")
    tasks.include?(outer_worker) and return
    tasks << outer_worker
  end

  def sync_remote_info
    send("sync_remote_#{remote_type}")
  rescue => e
    Sidekiq.logger.info("sync_remote_info error #{e.message}")
  end

  def sync_remote_user
    user_info = lark_app.get_user_info(extra["content"]["sender_id"]["open_id"])
    self.name = user_info["name"]
    assign_on_extra(user_info: user_info)
  end

  def avatar_key
    extra["avatar_key"]
  end

  def sync_remote_chat
    chat_info = lark_app.get_chat_info(extra["content"]["chat_id"])
    self.name = chat_info["name"]
    assign_on_extra(chat_info: chat_info)
  end
  alias :sync_remote_group_chat :sync_remote_chat
  alias :sync_remote_p2p_chat :sync_remote_chat

  def method_missing(name, *args)
    data = %i(content user_info chat_info user_auth_info).map {|a| (extra[a] || {})[name]  }.compact.first
    data || extra.dig(:content, :sender_id, name) || super
  end

  def request_chatgpt_when_empty?
    extra[:chatgpt].to_s == "true"
  end

  def notify(msg)
    !msg.present? and return

    msg_res = if user?
      lark_app.send_single_msg_to_user(open_id, msg)
    elsif group_chat? || p2p_chat?
      lark_app.send_single_msg_to_chat(chat_id, msg)
    end
    msg_id = msg_res.dig("data", "message_id")
    lark_app.like_and_react_msg(msg_id, request_type: 'user') if msg_res && LarkMsg.need_like?(msg)
    FeishuMessage.hook_msg("LarkApp", self, msg, msg_id) rescue nil
  # rescue => e
  #   Sidekiq.logger.info("Notify to FeishuObject #{id} failed #{e.message}")
  end

  def lark_app
    @lark_app ||= LarkApp.inner_tool
  end

  def self.find_user_with(user)
    !user.present? and return
    json = find_json_in_cache(user) and return find(json['id'])
    all.find{|a| a.matchable_names.include?(user) || a.open_id == user || 
             a.name.to_s.downcase =~ /#{user.to_s.downcase}/ rescue false}
  end

  def self.parse_event(event)
    header = event[:header]
    if header && header[:event_type] == "im.message.receive_v1"
      app_id = header[:app_id]
      parse_object(app_id, msg = event[:event][:message])
      parse_object(app_id, JSON.parse(msg[:content]), 'file') if msg[:message_type] == 'file'
      parse_object(app_id, event[:event][:sender], 'sender')
    end
  end

  def self.parse_object(app_id, body, body_type = 'chat')
    type = "#{body[:chat_type]}_chat"
    key = body[:chat_id]
    if body_type == 'sender'
      type = body[:sender_type]
      key = body[:sender_id][:union_id]
    elsif body_type == 'file'
      type = 'file'
      key = body[:file_key] || body["file_key"]
    end
    o = where(app_id: app_id, remote_type: type, key: key).first_or_create
    o.assign_on_extra(content: body)
    o.save
  end

  def self.special_actions
    true
  end

  def fill_default_alias
    self.alias_names ||= name.to_s.split(/\(|\（/).map{|b|b.downcase[/[a-z]+/]}.join(",")
    self.save
  end

  # for finding me
  def matchable_names
    _names = name.to_s.split(/\(|\（/).map{|a| a.to_s.gsub(/\)|\）/, "").strip }
    (alias_names.to_s.split(",") + _names).uniq || []
  end

  def chat_member_open_ids
    chat_members.map { |m| m["member_id"] }
  end

  def chat_members
    chat&.all_members || []
  end

  def chat
    chat_id = extra.dig("content", "chat_id")
    return if chat_id.blank?

    LarkChat.new(chat_id: chat_id)
  end

  class << self
    def select_values(tag, obj)
      us = User.pms_users.select{|a| a.email !~ /^guest/}.collect do |a| 
        [a.full_name_with_email, a.id]
      end.sort_by(&:first)
      {user_id: us}[tag.to_sym]
    end

    def admin
      find_user_with('eric')
    end

    def find_json_in_cache(user)
      RedisHelper.get_json("LarkUser::#{user}").presence
    end
  end
end
