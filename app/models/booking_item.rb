class BookingItem < PmsRecord
  belongs_to :booking
  belongs_to :item
  belongs_to :receipt
  validates :price, numericality: {greater_than: 0}
  validates :quantity, numericality: {greater_than: 0}
  scope :checked_out, -> { where(checked_out: true) }
  scope :not_checked_out, -> { where(checked_out: false) }
  scope :added_services, -> { joins(:item).where("items.category = ?", 1) }
  scope :added_payments, -> { joins(:item).where("items.category = ?", 0) }


  def value
    self.quantity * self.price
  end

  def name
    self.item.name
  end

  def brand
    self.item.brand
  end
  
end
