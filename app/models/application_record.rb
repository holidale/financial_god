class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class
  include Dangerable
  include RobotBasicMsg
  include Timeable

  def self.to_currency(_amount)
    _amount.to_s =~ /[a-zA-Z]+/ and return _amount
    ActionController::Base.helpers.number_to_currency(_amount, precision: 2)
  end

  def to_currency(_amount)
    ApplicationRecord.to_currency(_amount)
  end

  def percentage(a, b)
    "#{simple_percentage(a,b)} (#{a}/#{b})"
  end

  def simple_percentage(a, b)
    b.to_f <= 0 and return "0%"
    "#{(a / b.to_f * 100).round(2)}%"
  end

  class << self
    def holidays
      RedisHelper.smembers("holidays")
    end

    def holiday?
      holidays.include?(Date.current.to_s) || weekend?
    end

    def weekend?
      [6, 0].include?(Time.now.wday)
    end
  end

  def holiday?
    self.class.holiday?
  end

  def weekend?
    self.class.weekend?
  end

end
