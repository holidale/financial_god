class House < PmsRecord
  include Analysisable
  include RevenuePerAvailableRoomConcern
  has_many :bookings
  has_many :channel_listings
  has_many :contracts, class_name: "HostContract"
  has_many :daily_incomes
  has_many :journal_entries
  has_many :utility_fees
  has_one :house_translation
  has_one :property_spec
  has_one :amenity
  has_many :channels, through: :channel_listings

  belongs_to :user
  belongs_to :manager, :class_name => 'User', :foreign_key => 'manager_id'
  belongs_to :landlord, :class_name => 'User', :foreign_key => 'user_id'
  belongs_to :ae_user, :class_name => 'User', :foreign_key => 'ae_user_id'
  belongs_to :manager_group
  belongs_to :parent_house, :class_name => 'House', :foreign_key => 'house_id'

  has_one :location, as: :locationable, dependent: :destroy
  delegate :full_address_us_format, :address, :address2, :state, :city, :country, :zip, :longitude, :latitude, :community, to: :location, allow_nil: true

  scope :active, -> {where(active: true)}
  scope :native_listings, -> {where(manager_group_id: 2)}
  scope :active_native_listings, -> {where("houses.id = ? OR houses.active = ?", 10, true).where(manager_group_id: 2)}

  def bookings_count
    RedisHelper.smembers("house_#{id}_bookings").size
  end

  def invoices_count
    RedisHelper.smembers("house_#{id}_invoices").size
  end

  def short_address
    cached_key = "house_#{id}_short_address"
    a = RedisHelper.get(cached_key) and return a
    RedisHelper.cache(cached_key, "H##{id} #{address2}", 1.week)
  end

  def full_address
    self.location.try(:full_address)
  end

  def full_address_us_format
    self.location.try(:full_address_us_format)
  end

  def current_booking
    booked_bookings.current.last
  end

  def booked_bookings
    bookings.booked.includes(:contract_amendments)
  end

  def journal_covered_ranges(collection = [], account = RENTAL_INCOME_ACCOUNT)
    collection = journal_entries.for_account(account).includes(:booking) if collection.empty?
    collection.map do |a| 
      [a.journal_date, a.end_date, a.covered_days]
    end.sort_by(&:first).uniq
  end

  def journal_covered_days(collection = [], account = RENTAL_INCOME_ACCOUNT)
    collection = journal_entries.includes(:booking) if collection.empty?
    journal_covered_ranges(collection, account).map(&:last).sum.to_i
  end

  # auto refresh cache need to be set in PMS
  def basic_rental_info_from_jes
    jes = journal_entries.rental_income.includes(:booking)
    incomes = jes.sum(&:amount)
    days = journal_covered_days(jes)
    days == 0 and return {}

    res = {total: incomes, covered_days: days, daily: (incomes / days).round(2)}
    cache_basic_rental_info(res)
  end

  def cache_basic_rental_info(data = nil)
    data ||= basic_rental_info_from_jes
    RedisHelper.cache_json!("house_#{id}_basic_rental_info", data)
  end

  def cached_rental_info
    RedisHelper.get_json("house_#{id}_basic_rental_info")
  end

  def info_for_ar_due
    [address2, city.to_s.titleize, state.to_s.upcase].join(",")
  end

  def not_long_term?
    contract_type != "long_term_lease"
  end

  def qb_num
    "H##{id}"
  end

  def qb_name
    "#{qb_num}, #{full_address_us_format}".squish
  end

  def robot_name_in_list
    [qb_name, "AE: #{ae_user&.full_name}", contract_type&.titleize].join(", ")
  end

  def region
    "#{city.to_s.titleize}, #{state.to_s.upcase}"
  end

  def house_region
    Rails.cache.fetch("holidale/house_region/#{self.id}#{self.zip}", expires_in: 1.day) do
      result = []
      Region.all.each do |region|
        result << region.name if region.zip_codes_arr.include?(self.zip)
      end
      result
    end
  end

  def host_contracts
    contracts
  end

  def in_irvine?
    city.to_s.downcase.in?(["irvine", "尔湾"])
  end

  def minimum_stay
    in_irvine? and return property_spec&.minimum_stay || 30
    property_spec&.less_than_minimum_nights_stay and return 1
    property_spec&.minimum_stay || 1
  end

  def cluster_desc
    HouseCluster.cluster?(id) ? "(Cluster)" : ""
  end

  def desc_in_revpar
    "House #{cluster_desc} #{short_address}"
  end

  def earliest_available_date_for_house
    RedisHelper.get("earliest_available_date_for_house:#{id}:").to_date rescue nil
  end

  def latest_airbnb_id
    airbnb_listings = channel_listings.active_channels.joins(:channel).where('channels.name = "airbnb"').order('id asc')
    airbnb_listings.last&.airbnb_id
  end

  def query_for_html_fq(params)
    params_for_html = params.except(:quote_user_id, :q_type, :quote_source, :house_id) # Only include check_in/out, org_id
    "#{params_for_html.to_query}"
  end

  def quote_copiable_content(params = {})
    if params[:html]
      base_url = PublicConfigs.end_points.m2m
      query = query_for_html_fq(params[:quote_params])
      return "#{base_url}/quotation/#{id}?#{query}"
    elsif params[:quote_params].present? && ["breakdown", "break"].include?(params.dig(:quote_params, :q_type).downcase)
      url = PublicConfigs.end_points.pms +
      "/sales_templates/#{SalesTemplate.find(12).id}/parse?type=House&type_id=#{id}"
    else
      url = PublicConfigs.end_points.pms +
        "/sales_templates/#{SalesTemplate.first.id}/parse?type=House&type_id=#{id}"
    end
    url += "&#{params[:quote_params].to_query}" if params[:quote_params].presence
    res = Faraday.get(url).body
    res =~ /Forbidden/ ? quote_copiable_content_old(params) : res
  rescue => e
    quote_copiable_content_old(params)
  end

  def quote_copiable_content_old(params = {})
    sd = earliest_available_date_for_house || Date.current
    all_inclusive, breakdown = params[:all_inclusive], params[:breakdown]

    unit = all_inclusive["pay_by_day"] ? "Day" : "Night"
    s = "Thanks for reaching out. This home is available for move-in after #{sd}. Provided below is a link for this home which includes detailed information and photos: https://month2month.com/houses/#{id}/

Rental Details:
This #{property_spec.bedroom} bedroom / #{property_spec.bathroom.to_i} bath home is fully furnished including housewares, linens, towels, all utilities, cable/streaming, and wireless internet.

Rent: #{to_currency all_inclusive["rent_daily"]} / #{unit} (Utilities Included)
One-Time Cleaning Fee: #{to_currency all_inclusive["fees_list"].select{|a| a[:key].in?(["cleaning_fee"])}.sum{|a| a["amount"].to_f}.round(2)}

Security Deposit:  #{to_currency all_inclusive.dig("deposit")} (with approved credit)

or

Rent: #{to_currency breakdown["rent_daily"] }/ #{unit} (No Utilities)
One-Time Cleaning/Restocking Fee: #{to_currency breakdown["fees_list"].select{|a| a[:key].in?(["linen_fee", "cleaning_fee", "carpet_shampoo_fee"])}.sum{|a| a["amount"].to_f}.round(2)}

Security Deposit: #{to_currency breakdown.dig("deposit")} (with approved credit)"
  end

  def amenity_items
    [:gate_code, :door_code, :lockbox_code, :garage_code, :parking, :parking_note, :wifi_name, :wifi_password, :mailbox, :trash_day_in_words, :recycle_day_in_words, :trash_company, :program_code].select { |n| amenity&.send(n).present? }
  end

  def self.search_houses_by_address(search_str)
    url = PublicConfigs.end_points.pms +
      "/chrome_extension/v1/fast_search?search_str=#{search_str}"
    res = Faraday.get(url).body
    JSON.parse(res)["data"]["houses"]
  rescue
    nil
  end

  def self.house_for_region(region, purge: false)
    return {} if region.blank?
    key = "house_data_for-#{region[:id]}"
    Rails.cache.delete_matched(key) if purge
    Rails.cache.fetch(key, expires_in: 1.hour) do
      zip_codes = region.zip_codes_arr
      houses = House.joins(:location).active_native_listings.where('locations.zip': zip_codes).uniq
    end
  end

  HouseTranslation::COLUMNS.each do |a|
    define_method a do
      house_translation.send(a)
    end
  end
end
