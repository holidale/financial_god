class BookingAudit < PmsRecord
  belongs_to :booking
  belongs_to :user
  has_many :notes, as: :notable, dependent: :destroy

  enum status: %w[Passed Request_change Failed Unchecked]

  def audit_icon(size = "small")
    size = "small" unless %w(small large).include?(size)
    "booking_audit_#{self.status.downcase}_#{size}.svg"
  end

  def reset_status
    self.update(status: "Unchecked")
  end

end
