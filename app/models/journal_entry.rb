class JournalEntry < PmsRecord
  include ExtraObjectable

  belongs_to :house
  belongs_to :booking
  belongs_to :invoice, foreign_key: :payment_id
  belongs_to :sales, class_name: "User", foreign_key: :sales_id

  include Dateable
  include AccountingCalculable
  scope :include_day, -> (day) {where("journal_date <= '#{day}' and end_date >= '#{day}'")}
  scope :guest, -> {where(customerable_type: "User")}
  scope :org, -> {where(customerable_type: %w(QbCustomer Organization))}

  def finance_ensure_at
    journal_date
  end

  def pay_by_days
    journal_date == end_date || extra[:pay_by_days]
  end

  def rent_unit_name
    pay_by_days ? "days" : "nights"
  end

  def covered_days_with_unit
    [covered_days, "(#{rent_unit_name})"].join(" ")
  end

  def covered_in_period(period)
    period.to_s.size == 10 and return covered_the_day(period)

    base = range_days.select{|a| a.to_s =~ /#{period}/}.size
    delt = pay_by_days ? 0 : 1
    delt = 0 if middle_period?(period)

    final = base - delt
    final < 0 ? 0 : final
  end

  def covered_the_day(day)
    journal_date <= day.to_date && day.to_date < end_date and return 1
    pay_by_days ? 1 : 0
  end

  # Only last period for JE need to check pay_by_days
  def middle_period?(period)
    period.to_s.size == 4 and return period.to_i < end_date.year
    period.to_s.size == 7 and return end_date.to_s !~ /#{period}/
    false
  end

  def amount_in_period(period)
    days = covered_in_period(period)
    daily_amount * days
  end

  def amount
    super.abs rescue 0
  end

  def covered_days
    range_days.size - (pay_by_days ? 0 : 1)
  end

  def range_days
    (journal_date..end_date).to_a
  end

  def daily_amount
    covered_days == 0 ? 0 : (amount / covered_days)
  end

  class << self
    def compute_all_dalies(account: RENTAL_INCOME_ACCOUNT)
      start = order("journal_date").first.journal_date
      end_date = order("end_date").last.end_date
      (start..end_date).each do |day|
        jes = JournalEntry.include_day(day).for_account(account)
        jes = jes.reject{|je| je.end_date.to_s == day.to_s && !je.pay_by_days}
        if (a = jes.sum(&:daily_amount)) > 0
          di = DailyIncome.where(day: day, house_id: 0, account: account).first_or_create
          di.update_extra(je_ids: jes.map(&:id))
          di.total = a
          di.save
        end
      end
    end
  end
end
