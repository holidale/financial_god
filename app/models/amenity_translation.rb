class AmenityTranslation < PmsRecord
  belongs_to :amenity

  def trash_day_in_words
    Date::DAYNAMES[trash_day] if trash_day == 0
  end

  def recycle_day_in_words
    Date::DAYNAMES[recycle_day] if recycle_day == 0
  end
end
