class DeductRefund < PmsRecord
  belongs_to :booking
  belongs_to :invoice, foreign_key: :payment_id
  belongs_to :refund_receipt
end
