class CreditMemoReceivedPayment < PmsRecord
  self.table_name = "credit_memos_received_payments"
  belongs_to :credit_memo
  belongs_to :received_payment
end
