class CreditMemo < PmsRecord
  belongs_to :booking
  has_many :credit_memo_received_payments
  has_many :received_payments, through: :credit_memo_received_payments

  belongs_to :refund_receipt

  has_many :credit_memo_payments
  has_many :invoice, through: :credit_memo_payments, foreign_key: :payment_id
end
