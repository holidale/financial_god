class TokenReward < PmsRecord
  belongs_to :user
  belongs_to :service
  belongs_to :service_schedule

  validates :token, presence: true, numericality: { greater_than_or_equal_to: 0 }

  def self.get_weekly_data(date, house_ids = nil, service_type = nil)
    start_date = date.beginning_of_week
    end_date = date.end_of_week
    tr = TokenReward.where(created_at: start_date...end_date)

    if house_ids.present? || service_type.present?
      ss = ServiceSchedule.all
      ss = ss.where(house_id: house_ids) if house_ids.present?
      ss = ss.with_type(service_type) if service_type.present?
      tr = tr.joins(:service_schedule).merge(ss)
    end

    tr.joins(:user).group(:user_id).select("user_id, CONCAT(users.first_name, ' ', users.last_name) AS full_name, SUM(token_rewards.token) AS total_token")
  end
end