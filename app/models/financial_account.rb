class FinancialAccount < PmsRecord
  has_many :cost_items, dependent: :destroy
  scope :actived, -> { where(active: true) }
  scope :inactive, -> { where.not(active: true) }
end
