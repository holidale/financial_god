class CreditMemoPayment < PmsRecord
  self.table_name = "#{DBName}.credit_memos_payments"
  belongs_to :credit_memo
  belongs_to :payment

  def paid_on
    credit_memo&.credit_memo_date
  end
end
