class DailyIncome < ApplicationRecord
  # belongs_to :house
  include ExtraObjectable
  include AccountingCalculable
  include Dateable

  MonthMapper = %w(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec)

  scope :include_day, -> (day) {where(day: day)}

  def je_ids
    extra[:je_ids] || []
  end

  def journal_entries
    JournalEntry.journal_entries_from_ids(je_ids)
  end

  def amount
    total
  end

  def finance_ensure_at
    day
  end

  class << self
    def init_with_house(house, day)
      where(house_id: house, day: day).first_or_create
    end

    def journal_entries_from_ids(ids)
      JournalEntry.where(id: ids || [])
    end

    def check!(account = RENTAL_INCOME_ACCOUNT)
      a = for_account(account).sum(&:amount) 
      b = JournalEntry.for_account(account).sum(&:amount)
      [a, b]
    end
  end
end
