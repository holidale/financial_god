class Region < PmsRecord
  def self.get_hierarchical_regions(pid: 0, filter_not_show: false, include_photo: false)
    regions = self.where(pid: pid).order(order: :asc)
    regions = regions.where(is_show: 1) if filter_not_show
    out_regions = []
    regions.each do |one_region|
      out_one_region = {}
      one_region.attributes.each { |k, v| out_one_region[k.to_sym] = v }
      out_one_region[:children] = get_hierarchical_regions(pid: one_region[:id], filter_not_show: filter_not_show, include_photo: include_photo)
      out_one_region[:photo] = one_region.photo if include_photo
      out_regions << out_one_region
    end
    out_regions
  end

  def self.get_children_ids(pid = 0, include_self = false)
    hierarchical_regions = get_hierarchical_regions(pid: pid)
    children_ids = recursion_get_children_ids hierarchical_regions
    children_ids << pid if include_self
    children_ids
  end

  def self.recursion_get_children_ids(regions, ids = [])
    regions.each do |one_region|
      ids << one_region[:id]
      if one_region[:children].present?
        recursion_get_children_ids one_region[:children], ids
      end
    end
    ids
  end

  def self.get_region_zip_codes(id)
    zip_codes = []
    region_ids = get_children_ids id, true
    regions = Region.where(id: region_ids)
    regions.each do |one_region|
      codes_arr = one_region[:zip_codes].split(',')
      codes_arr.each do |one_code|
        if one_code =~ /~/
          one_code_arr = one_code.split('~')
          (one_code_arr[0]..one_code_arr[1]).each { |item| zip_codes << item }
        else
          zip_codes << one_code
        end
      end
    end
    zip_codes.uniq
  end

  def zip_codes_arr
    Region.get_region_zip_codes(id)
  end
end