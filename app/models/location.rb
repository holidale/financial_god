class Location < PmsRecord
  belongs_to :locationable, polymorphic: true, touch: true
  has_many :location_translations

  def city
    location_translations.find_by(locale: "en")&.city
  end

  def full_address
    str = ""
    str << "#{self.address2.to_s}" unless self.address2.blank?
    str << " ##{self.apartment_number}" unless self.apartment_number.blank?
    str << ", #{self.city&.titleize}, #{I18n.t(self.state, scope: self.country+'_state_list')} #{self.zip.split("-")&.first}"
  end

  def full_address_us_format
    str = ""
    str << "#{self.address2.to_s}" unless self.address2.blank?
    str << " ##{self.apartment_number}" unless self.apartment_number.blank?
    str << ", #{self.city&.titleize}, #{I18n.t(self.state, scope: self.country+'_state_list')} #{self.zip.split("-")&.first}"
  end
end
