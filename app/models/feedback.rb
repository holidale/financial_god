class Feedback < PmsRecord
  # include ExtraObjectable
  belongs_to :user

  def lark_json
    {
      landlord: landlord, 
      ae: ae,
      rental_score: rental_score,
      maintenance_score: maintenance_score,
      satisfaction_score: satisfaction_score,
      listing_score: listing_score,
      share_willingness_score: share_willingness_score, 
      content: content
    }
  end

  def lark_msg
    "Feedback from #{source.to_s.titleize}: User##{user&.name}, content: #{content}, Score: #{rental_score}|#{maintenance_score}|#{satisfaction_score}|#{listing_score}|#{share_willingness_score}, AE: #{ae}"
  end

  def landlord
    user&.name
  end

  def ae
    user.nil? and return nil
    hs = user.houses + House.where(id: content.to_s.scan(/ID:(\d+)/).flatten)
    hs.uniq.map{|a| a.ae_user&.name}.uniq.join(",")
  end
end
