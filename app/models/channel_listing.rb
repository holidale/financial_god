class ChannelListing < PmsRecord
  belongs_to :channel
  belongs_to :house

  scope :active_channels, -> {
    where(status: true)
  }

  def airbnb_id
    return nil unless channel&.name == "airbnb"
    ids = channel_listing_id&.split(';')
    ids&.each_with_index do |listing, index|
      a = listing.split(',')
      return a[0].strip if a[1].present? && a[1].strip.titlecase == "Master"
      return a[0].strip if (index == (ids.length - 1))
    end
  rescue
    nil
  end
end
