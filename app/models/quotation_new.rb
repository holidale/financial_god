class QuotationNew < ApplicationRecord
  self.table_name = "#{PublicConfigs.qcenter_db_name}.quotations"
  include ExtraObjectable
  include RevenueCommon

  belongs_to :booking
  has_many :records, class_name: "QuotationRecord", foreign_key: :quotation_id
  has_many :booking_revenues, as: :revenueable

  enum quote_source: %w(individual bd)
  enum quote_type: %w(all_inclusive breakdown)
  enum status: %w(status_new accepted declined pending)

  DUE_DATE_OPTIONS = {
    every_30_nights: "every 30 nights",
    first_next_month: "1st, next month (variable for small & big months)",
    first_next_next_month: "1st, next-next month (variable for small & big months)",
    same_day_every_month: "same date, every month",
    lumpsum: "lumpsum",
    first_next_month_same_rate: "1st, next month (same rate for small big & months)",
    first_next_next_month_same_rate: "1st, next-next month (same rate for small & big months)"
  }
  QUOTE_ROUND = 2
  QCENTER_QUOTE_ATTRS = %i(channel mid_clean_times request_heating customer_id customer_type)

  enum due_date_option: DUE_DATE_OPTIONS.keys
  attr_accessor :rate_table, :due_date_choice

  # before_save :sync_new_attrs

  QCENTER_QUOTE_ATTRS.each do |a|
    define_method(a) {quote_params[a]}
  end

  def start_date
    check_in
  end

  def end_date
    check_out
  end

  def new_rule?
    check_out >= "2023-05-01".to_date
  end

  def old_daily_owner_revenue
    read_attribute(:daily_owner_revenue).to_i > 0 and return read_attribute(:daily_owner_revenue)
    daily_revenue.to_f * booking.contract_ratio
  end

  def daily_owner_revenue
    if new_rule?
      (pure_rental_for_share / new_revenue_covered_days).to_f * booking.contract_ratio
    else
      super.to_i > 0 and return super
      daily_revenue.to_f * booking.contract_ratio
    end
  rescue => e
    super.to_i > 0 and return super
    daily_revenue.to_f * booking.contract_ratio
  end

  def lower_costs_records
    records.select{|a| a.internal_cost.to_f > a.amount && a.fee_key != 'deposit'}
  end

  def pure_rent
    if r = records.find{|a| a.fee_key == 'rent'}
      r.extra[:pure_amount] || 0
    end
    0
  end

  def pure_rental_for_share
    pure_rental_without_tax == 0 and return 0
    pure_rental_without_tax - modified_other_internal_costs
  end

  def rent_record
    @rent_record ||= get_record("rent") || init_record("rent", 0, 0)
  end

  def pure_rental
    (pure_rental_without_tax == 0 ? 0 : pure_rental_without_tax + rent_record.tax).round(2)
  end

  def pure_rental_without_tax
    rent_record ? (rent_record.extra[:pure_amount] || 0) : 0
  end

  def modified_other_internal_costs
    records.select do |a|
      a.amount < a.internal_cost && a.fee_key != 'deposit'
    end.sum{|a| a.internal_cost - a.amount}
  end

  def get_record(fee_name)
    records.find{|a| a.fee_key == fee_name.to_s}
  end

  def get_records(fee_names)
    records.select{|a| fee_names.map(&:to_s).index(a.fee_key)}
  end

  def new_revenue_covered_days
    return (extra[:revenue_details] || {}).dig(:entire_periods).to_i unless new_rule?
    sdate = revenue_start
    edate = revenue_end + booking.gap_days

    M2mTool.generate_month_ranges(sdate, edate).sum do |dates|
      _sdate, _edate = dates
      days = M2mTool.days_between(_sdate, _edate, last: edate == _edate)
      if (_sdate.year <= 2023 && _sdate.month < 5) && _sdate.month == _edate.month
        days
      else
        BookingRevenue::NEED_COVERT_TO_30_DAYS_MONTHS.include?(_sdate.month) && M2mTool.days_for_month(_sdate) == days ? 30 : days
      end
    end
  end

  def revenue_start
    check_in
  end

  def revenue_end
    check_out
  end

  def form_type
    quote_type
  end
end
