class QbObject < PmsRecord
  include ExtraObjectable
  has_many :qb_sync_records

  scope :for_invoice, -> {where(remote_type: "Invoice", local_type: "Payment")}
  scope :for_accounts, -> {where(remote_type: "Account")}
  scope :for_journal_entries, -> {where(remote_type: "JournalEntry")}
  scope :from_account_records, -> (ars){where(id: ars.map(&:record_source_id))}
 
  def qb_json
    extra[:remote_data] || {}
  end

  def last_sync
    last_sync_at&.strftime("%F %T")
  end

  def amountable_type
    remote_type.to_s.tableize.singularize
  end

  def is_invoice?
    remote_type == "Invoice" && local_type == "Payment"
  end

  def invoice_id
    is_invoice? and return local_value
  end

  def finance_date
    qb_json["TxnDate"]
  end

  def finance_amount
    _amount = if %w(refund_receipt).index(amountable_type)
      qb_json["Line"]&.select{|a| a["Amount"] > 0}.sum{|a| a["Amount"]} || 0
    elsif %w(journal_entry).index(amountable_type)
      qb_json["Line"].select{|c|c["JournalEntryLineDetail"]["AccountRef"]['name'].to_s !~ /Revenue Clearing Account/}.sum{|b|b["Amount"]}
    else
      qb_json["TotalAmt"] || 0
    end
    _amount.round(2)
  rescue => e
    0
  end

  def amount
    finance_amount
  end

  def balance_amount
    qb_json["Balance"] || 0
  rescue => e
    0
  end

  class << self
    def account_names
      where(remote_type: "Account").map(&:local_value).uniq.select{|a| a !~ /Clearing Account/}
    end

    def account_names_income
      _type = Rails.env.production? ? "Income" : "Accounts Receivable"
      where(remote_type: "Account").select{|a| a.qb_json["AccountType"] == _type}.map(&:local_value).uniq
    end

    def qb_class_names
      where(remote_type: "Class").map(&:local_value).uniq
    end
  end
end
