class ContractAmendment < PmsRecord
  include Dateable
  include ExtraObjectable
  include RevenueCommon

  belongs_to :booking
  scope :active, -> {where(status: "active")}
  scope :active_amendments, -> { active.order('start_date asc') }
  scope :need_revenues, -> { where.not(status: "voided").order('start_date asc') }

  delegate :quotation, to: :booking
  delegate :discount, :utility_fund, to: :quotation

  def active?
    status == "active"
  end

  %w(old_check_in old_check_out new_check_in new_check_out start_date end_date).each do |_date|
    define_method _date do
      read_attribute(_date).strftime("%F").to_date rescue nil
    end
  end

  def covered_days
    (end_date - start_date).to_i + booking.delt_cover
  end

  def gap_days
    booking.pay_by_days || end_date.to_date < booking.real_check_out_date ? 1 : 0
  end

  def real_covered_days
    edate = [booking.real_check_out_date, end_date.to_date].min
    days = (edate - self.start_date.to_date).to_i + gap_days rescue 0
    days < 0 ? 0 : days
  end

  def fees_with_detail(with_utility_fund: true)
    _days = real_covered_days.to_i
    fees = []
    fees << {amount: pet_rent * _days, title: "Pet Rent", covers: _days} if pet_rent.to_f > 0
    fees << {amount: extra_guest_rent * _days, title: "Extra Guest Rent", covers: _days} if extra_guest_rent.to_f > 0
    fees << {amount: utility_fund * _days, title: "Utility Fund", covers: _days} if utility_fund.to_f > 0 && with_utility_fund
    if quotation.rent_with_agency_labor? && relocation_agency_labor > 0
      amount = relocation_agency_labor
      fees << {amount: amount, title: "Agency Labor", covers: _days}
    end
    fees.select{|a| a[:amount].to_f != 0 }
  end

  def fees_with_detail_for_revenue
    base = extra.dig("revenue_details", "calculations", "extension_not_shared", "line_items") || {}
    base.map do |k,v| 
      [k.split(/\d+/)[1].gsub("(", "").strip, v]
    end.to_h
  rescue => e
    base = fees_with_detail
    Hash[base.map{|h| [h[:title], h[:amount]]}] rescue {}
  end

  def invoices
    booking.all_extension_invoices.select do |a| 
      a.effective? && a.start_date >= start_date.to_date && a.end_date < end_date.to_date + 1
    end
  end

  def booking_revenues
    quotation_new and return BookingRevenue.where(revenueable_type: 'QuotationNew', revenueable_id: quotation_new&.id)
    BookingRevenue.where(revenueable_type: 'ContractAmendment', revenueable_id: id).presence
  end

  def house
    booking.house
  end

  def user
    booking&.user
  end

  def data_for_robot_msg
    {
      deal_time: created_at.strftime("%m/%d"),
      address: [house.address2, house.city.to_s.titleize].join(","),
      guest_name: user&.full_name || "",
      lease_term: lease_term,
      quotation_version: booking.quotation_version,
      total_amount: Booking.to_currency(broadcast_total),
      referrer: booking.get_referrer&.full_name,
    }
  end

  def broadcast_total
    n = shareable_revenue
    n > 0 ? n : invoices.sum(&:amount)
  end

  def shareable_revenue
    extra.dig(:revenue_details, :shareable_revenue) || 0
  end

  def lease_term
    [start_date, end_date].map{|a| a.strftime("%m/%d/%Y")}.join("-")
  end

  # new rule start from 2022-12-27
  def can_broadcast?
    extra[:lark_broadcast].present? || created_at.to_date < '2022-12-27'.to_date and return false
    booking&.check_out == new_check_out rescue false
  end

  def data_for_robot_msg_content
    msg = data_for_robot_msg
    arr = ["**New Extension: #{LarkMsg.link_to_pms(booking)}**"]
    arr << "Extension Time: #{msg[:deal_time]}"
    arr << "Address: #{msg[:address]}"
    arr << "Guest Name: #{msg[:guest_name]}"
    arr << "Lease Term: #{msg[:lease_term]}"
    arr << "Quotation Version: #{msg[:quotation_version]}"
    arr << "Total Amount: #{msg[:total_amount]}"
    arr
  end

  def quotation_new
    QuotationNew.find_by(id: extra[:quotation_new_id])
  end

  def create_msg
    "New Extension For Booking ##{booking.id} from #{start_date.strftime("%m/%d/%Y")} to #{end_date.strftime("%m/%d/%Y")},House #{booking.house.short_address}"
  end
end
