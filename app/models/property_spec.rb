class PropertySpec < PmsRecord
  belongs_to :house, touch: true
  has_one :pool_heating, dependent: :destroy
end
