class JiraTask < ApplicationRecord
  self.table_name = "#{PublicConfigs.test_case_db_name}.jira_tasks"
  include ExtraObjectable
  validates :key, presence: true

  before_save :parse_fields

  scope :testing, -> {where(status: "Waiting for test")}
  scope :unfinished, -> {where(status: %w(Open In\ Development To\ Do))}
  scope :processing, -> {where(status: ["Open", "In Development", "To Do", "Waiting for test"])}
  scope :current_month, -> (month = Date.current) {
    where("sprints like '%#{month.to_s[0,7]}%'")
  }
  scope :label_for, -> (label) {
    where("labels like '%,#{label}'") + 
      where("labels like '#{label},%'") +
      where("labels = '#{label}'") +
      where("labels like '%,#{label},%'")
  }
  scope :labels_for, -> (labels) {
    labels.map{|a| label_for(a) }.flatten
  }

  ChineseStatus = {
    "Open" => "未开始",
    "To Do" => "未开始",
    "In Development" => "开发中",
    "Waiting for test" => "测试中",
    "PR Code Review" => "待上线",
    "Pre Online" => "待上线",
    "Done" => "已上线",
  }

  def label_for?(label)
    labels.to_s =~ /\,#{label}|#{label}\,/ 
  end

  def sprints_for?(sprint)
    sprints.to_s =~ /\,#{label}|#{label}\,/ 
  end

  def jira_id
    extra[:id]
  end

  def done?
    status == 'Done'
  end

  def pre_online?
    status.in? ['Pre Online', 'PR Code Review']
  end

  def basic_lark_title(_title = nil)
    _title ||= "#{key} #{title}"
    # if ak = FeishuObject.find_user_with(assignee_name)&.avatar_key
    #   return LarkMsg.tiny_image(_title, ak)
    # end
    _title
  end

  def release_date
    test_due || "待估"
  end

  def simple_report
    _title = "#{key} #{title}"
    basic = basic_lark_title(_title)
    msg = [basic]
    done? and return [basic_lark_title("#{_title} (已上线)")]
    pre_online? and return [basic_lark_title("#{_title} (等待上线)")]
    paused? and return [basic_lark_title("#{_title}<font color='red'> (已暂停)</font>")]

    if due_date
      msg = [basic_lark_title("#{_title} (上线时间: #{test_due ? test_due : "待估"})")]
      dp = dev_progress
      dev_ok = dp =~ /100/
      msg << "开发中: #{dev_progress}, Due: #{due_date ? due_date.to_s[0, 10] : "待估"}" if !dev_ok
      if dev_ok && test_due
        cps = cases_progress
        msg << "测试中#{cps == 'None' ? "" : ": #{cps}"}, Due: #{test_due ? test_due.to_s[0, 10] : "待估"}"
      end
      bisize = block_issues_size > 5 ? "多" : block_issues_size

      risk = []
      risk << "<font color='red'>**Delay 风险**：插入任务 #{bisize}个</font>" if has_blocked_issues?
      risk << "<font color='red'>需求变更 #{pd_changes_time}次</font>" if pd_changes?
      risk << "<font color='red'>暂停 #{paused_days} 天</font>" if paused_days.to_i > 0
      msg << risk.join(", ") if risk.presence
    else
      return [basic_lark_title("#{_title} (Dev待估)")]
    end
    msg
  end

  def has_blocked_issues?
    block_issues.present?
  end

  def labels_arr
    labels.to_s.split(",")
  end

  def paused?
    labels_arr.include?("paused")
  end

  def paused_days
    pure_description.split("\n").select{|a| a =~ /paused/}.last.to_s[/\d+/] || 0
  end

  def block_issues
    @block_issues ||= marked_line_contents("delay")
  end

  def reporters
    extra.dig("fields", "customfield_10911") || []
  end

  def block_issues_size
    block_issues.size
  end

  def pd_changes_time
    @pd_changes_time ||= marked_line_contents("update").size
  end

  def pd_changes?
    pd_changes_time > 0
  end

  def marked_line_contents(mark)
    pure_description.to_s.split("\n").select{|a| a.downcase =~ /#{mark}[:\：]+/}
  end

  %w(instruction video).each do |a|
    define_method a do
      marked_line_contents(a).first.to_s.scan(/\[(.*?)\|/).flatten.first.presence
    end
  end

  def dev_progress
    done = PmsRecord.get_work_days(start_date, [Date.current, due_date.to_date].min).size
    percentage(done - paused_days.to_i, dev_periods - paused_days.to_i)
  rescue => e
    "None"
  end

  def cases_progress
    !test_due and return 'None'
    ss = cases_status
    ss.empty? and return "None"

    passed = ss.select{|k,v| v == 'Passed'}
    percentage(passed.size, ss.size)
  rescue => e
    "None"
  end

  def dev_periods
    PmsRecord.get_work_days(start_date, due_date.to_date).size
  end

  def parse_fields
    self.labels = extra.dig(:fields, :labels)&.join(",")
    self.sprints = extra.dig(:fields, :customfield_10007)&.map{|a| a[:name]}&.join(",")
  rescue => e
    LarkMsg.enqueue(:parse_fields_error, "#{key} #{e.message}")
  end

  def assignee_name
    super.presence || extra.dig(:fields, :assignee, :displayName)
  end

  def assignee_avatar
    extra.dig(:fields, :assignee, :avatarUrls).first || "#{PublicConfigs.end_points.financial}/jira.jpg"
  end

  def short_assignee
    assignee_name.to_s.split(" ").first || 'Unassigned'
  end

  def cases_status
    _cases = extra.dig(:fields, :issuelinks).map do |a| 
      it = a.dig("outwardIssue","issuetype")
      if it == 'TestCase'
        [ a.dig("outwardIssue", "key"), a.dig("outwardIssue","status") ]
      end
    end.compact.to_h.compact
  end

  def cases_status_summary(consider_todo: true, format: true)
    ss = cases_status
    consider_todo && (ss.any?{|k,v| v == 'To Do' } || ss.empty?) and return
    ss.group_by{|k, v| v}.map{|k,v| [v.size, k].join(" ")}.join(", ")
  end

  def link
    Jira.link(key, title)
  end
  def msg_for_cases
    if desc = cases_status_summary
      data = [link]
      return data << "Test Cases Status: #{desc}"
    end
    []
  end

  def test_due
    extra.dig(:fields, :customfield_10921)&.to_date
  end

  def start_date
    extra.dig(:fields, :customfield_10900)&.to_date
  end

  def pure_description
    if json = JSON.parse(description.to_s.gsub("=>",": ")) rescue nil
      return json["content"].map{|a| a["content"].map{|aa| aa["text"]} }.flatten.join("\n") || description
    end
    description
  end

  class << self
    def check_task(issue)
      issue = issue.with_indifferent_access
      issue = clean_issue(issue)
      jt = where(key: issue[:key]).first_or_create
      fs = issue[:fields]
      fs.delete(:customfield_10919) # TestCase Description
      jt.update(
        key: issue[:key],
        title: issue.dig(:fields, :summary),
        issuetype: fs[:issuetype][:name],
        status: fs[:status][:name],
        description: fs[:description],
        assignee_id: fs.dig(:assignee, :accountId), # fs["assignee"]["displayName"]
        assignee_name: fs.dig(:assignee, :displayName),
        # tester_id: ,
        due_date: fs[:duedate],
        extra: issue
      )
      # fs[:issuelinks]&.each do |test_case|
      # end
    end

    def clean_issue(issue)
      if issuelinks = issue.dig("fields", "issuelinks").presence
        links = issuelinks.map do |link|
          outward_issue = link["outwardIssue"] || {}
          outward_issue_fields = outward_issue["fields"] || {}
          {
            id: link["id"], 
            outwardIssue: outward_issue.slice("id", "key").merge(
              title: outward_issue_fields["summary"],
              status: outward_issue_fields.dig("status", "name"),
              issuetype: outward_issue_fields.dig("issuetype", "name"),
            )
          }
        end
        issue["fields"]["issuelinks"] = links
      end
      issue
    end

    def testing
      where(status: 'waiting for test').select{|a| a.extra.dig(:fields, :issuelinks).presence}
    end

    def important_tasks_progress
      labels = %w(major us)
      # data = JiraTask.processing.current_month.labels_for(labels).map(&:simple_report)
      js = JiraTask.current_month.labels_for(labels).sort_by do |a| 
        a.test_due || a.due_date || '3000-01-01'.to_date
      end
      done_js = js.select(&:done?)
      data = (js - done_js).map(&:simple_report) 
      # data << done_js.map(&:simple_report).sum if done_js.present?
      ["**当前高优先级/重要任务进展**"] + MangoRobot.resp_multy_content(*data) +
        ["**最近上线的任务**"] + recent_online

    end

    def recent_online
      where(online_at: 3.days.ago..Time.current).map do |a|
        Jira.link_task(a)
      end
    end
  end
end
