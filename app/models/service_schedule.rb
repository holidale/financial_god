class ServiceSchedule < PmsRecord
  include ExtraObjectable
  include WorkorderLarkTaskConcern
  include TimeTrackable

  belongs_to :service
  belongs_to :booking
  belongs_to :house
  belongs_to :creator, :class_name => 'User', :foreign_key => 'creator_id'
  belongs_to :assignee, :class_name => 'User', :foreign_key => 'assignee_id'
  belongs_to :vendor_manager, :class_name => 'User', :foreign_key => 'vendor_manager_id'
  belongs_to :cleaner
  # has_many :scanned_files, as: :relatable, dependent: :destroy
  has_many :notes, as: :notable
  # has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :cost_items, dependent: :destroy
  # has_many :recurring_costs, dependent: :destroy
  # has_many :inspection_results, dependent: :destroy
  # has_one :house_service_image, dependent: :destroy
  has_one :feishu_task, foreign_key: :workorder_id
  has_one :token_reward, dependent: :destroy

  VALID_STATUSES = %w(pending scheduled submitted completed not_an_issue follow_up reviewed)
  enum status: VALID_STATUSES.map {|s| [s.to_sym, s]}.to_h
  VALID_PAYMENT_STATUSES = %w(pending closed)

  VALID_AUDIT_ATTRIBUTES = [:status, :note, :due, :booking_id, :house_id, :creator_id, :assignee_id, :start_time, :end_time]

  before_save :update_completion_time, if: :status_changed_to_completed?
  after_save :create_token_reward, if: :status_changed_to_completed?

  scope :by_ae, lambda { |id|
    where(house_id: House.native_listings.where(ae_user_id: id))
  }
  scope :inspections, -> { joins(:service).merge(Service.inspections) }
  scope :check_inspections, -> { joins(:service).merge(Service.check_inspections) }
  scope :work_orders, -> { joins(:service).merge(Service.work_orders) }

  scope :not_finished, -> { where(status: %w(pending scheduled submitted follow_up reviewed)) }
  scope :finished, -> { where(status: %w(completed not_an_issue)) }
  scope :overdue_days, -> (days) { where("due < ?", days.days.ago.to_date) }
  scope :past_notification_date_time, -> { where("service_schedules.notification_date_time < ? OR service_schedules.notification_date_time IS NULL", DateTime.current) }
  scope :recent_unfinished, -> {
    where(status: %w(pending scheduled submitted follow_up reviewed)).where("service_schedules.created_at > ?", "2022-07-10".to_date)
  }
  scope :need_addressing, -> { recent_unfinished.past_notification_date_time }

  scope :includes_tables, -> { includes({house: {location: :translations}}, :booking, :cleaner, :assignee, :service, :notes) }

  scope :has_cleaner, -> {
    where.not(cleaner_id: nil)
  }

  def status_changed_to_completed?
    self.status_was != "completed" && self.status == "completed" && self.token_reward.nil?
  end

  def create_token_reward
    token_reward.nil? and assignee.present? and assignee.is_employee? and TokenReward.create(
      user: assignee,
      service: service,
      service_schedule: self,
      token: service.token
    )
  end

  def related_region
    @related_region ||= Region.all.find { |region| region.zip_codes_arr.include?(house&.location&.zip) }
  end

  def maintenance_only?
    service.present? && Service.maintenance_only.exists?(service.id)
  end

  def id_name
    "#{self.id},#{self.service&.name},H##{self.house_id}"
  end

  # used to check whether service schedule is for cleaning service
  # return type should be boolean
  def is_cleaning?
    self.service&.category == "cleaning"
  end

  def set_default_attributes
    self.due ||= Date.current
    self.status ||= "pending"
  end

  def update_completion_time
    self.update_extra({ completion_time: Time.current })
  end

  def self.summary
    basic = where("created_at > '2022-07-10'")
    arr = ["Total Pending: #{basic.pending.count}"]
    arr << "Overdue Pending: #{basic.pending.overdue_days(0).count}"
    arr
  end

  def create_lark_task_related
    _assignee = assignee&.feishu_object
    _creator = creator&.feishu_object
    create_lark_task(
      {
        skip_send_lark_msg: true,
        task_payload: lark_task_payload
      }
    )
    create_lark_task_msg
  end
end
