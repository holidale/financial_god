class Organization < PmsRecord
  include ExtraObjectable
  has_and_belongs_to_many :fee_policies
  has_many :users
  belongs_to :default_fee_policy, class_name: "FeePolicy"
 
  def invoices_summary(period = nil)
    balance_dues = AccountRecord.invoices.for_org(self)
    # is = Invoice.unpaid.where(id: balance_dues.map(&:record_source_id))
    is = Invoice.where(id: balance_dues.map(&:record_source_id)).includes(booking: [:user, :house])
    period ? is.for_period_like(period, :due_date) : is
  end

  def self.find_by(params)
    params[:id].to_i == 0 and return new(name: 'airbnb', id: 0)
    super
  end
end
