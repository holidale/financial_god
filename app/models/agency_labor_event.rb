# frozen_string_literal: true

class AgencyLaborEvent < PmsRecord
  include AgencyLaborCommonConcern

  serialize :extra, Hash

  belongs_to :booking
  belongs_to :organization

  def agency_labor_monthly_cap
    extra[:agency_labor_monthly_cap].to_f
  end
end
