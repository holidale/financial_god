class ShortMessageLog < PmsRecord
  include ExtraObjectable
  has_many :workorder_messages, primary_key: :sid, foreign_key: :sid

  def lark_msg
     {
      id: id,
      sid: sid,
      from: from,
      to: to,
      body: body,
      direction: direction,
      segments: num_segments,
      api_version: api_version,
      status: status,
      date: created_at.strftime("%Y-%m-%d %H:%M:%S"),
      work_orders: workorder_messages.map{|wom| wom.workorder.id }
    }
  end
end
