module ColorsHelper

  def rand_colors(num = 6, opacity: 0.5)
    base_colors = rand_color_groups(num)
    {
      background_colors: base_colors.map{|c| "rgba(#{c.join(",")},#{opacity})" },
      border_colors: base_colors.map{|c| "rgba(#{c.join(",")},1)" }
    }
  end

  def rand_color_groups(num = 6)
    num.times.map{|i| 3.times.map {rand(256)}}
  end
end
