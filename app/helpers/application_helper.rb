module ApplicationHelper

  # to clean menus to code
  def finance_menus
    {
      home: "/",
      qb_compare: {
        compare_invoices: "",
        compare_journal_entries: "",
      },
      ar_overdue_summary: {
        organizations: "/ar_overdue_summary",
        individuals: "/ar_overdue_summary?source=user",
      },
      statistics: {
        daily_incomes_overview: "",
        deposits_overview: "",
        houses_overview: "",
        # bookings_overview: "",
        invoices_overview: "",
        journal_entries_overview: "",
      },
      auto_check: {
        extensions: ""
      },
      admin: {
        feedbacks: "/admin/feedbacks",
        lark_msg_subscription: "/admin/feishu_objects",
        approvals: "/admin/approvals"
      }
    }
  end

  def selectors_arr(k)
    k == "data_source" and return AccountRecord.data_sources
    k == "company" and return AccountRecord.companies
    k == "income_accounts" and return QbObject.account_names_income
    []
  end

  def year_month(year, month)
    "#{year}-#{format_low_number(month)}"
  end

  def format_low_number(a)
    a.to_i < 10 ? "0#{a}" : a.to_s
  end

  def data_to_chart(collection, ele_id: "invoices", label: "Finance Chart")
    base = {
      ele_id: ele_id, 
      label: label,
    }
    labels = parse_labels
    base.merge!(
      data: {
        labels: labels,
        data: labels.map{|m| collection.select{|a| a.finance_ensure_at.to_s =~ /#{m}/}.sum(&:amount) }
      }
    )
  end

  def parse_labels
    @period.to_s =~ /\,/ and return parse_range_labels
    end_label = @period.to_s.size == 4 ? 12 : last_day_for_month(@period)
    labels = 1.upto(end_label).map{|a| "#{@period}-#{format_low_number(a)}" }
  end

  def parse_range_labels
    range = (@start_date..@end_date).to_a
    (@end_date - @start_date).to_i > 30 and return range.map{|a| a.strftime("%Y-%m")}.uniq
    range.map{|a| a.strftime("%Y-%m-%d")}
  end

  def table_base_class
    "table left-table table-bordered"
  end

  def month_period?(s)
    s.to_s.size == 7
  end

  def supported_period?(s)
    s.to_s.size <= 7
  end

  def last_day_for_month(month)
    _year, n = month.to_s.split("-").map(&:to_i)
    [4,6,9,11].include?(n) and return 30
    if n == 2
      return _year % 4 == 0 ? 29 : 28
    end
    31
  end

  %w(booking house invoice).each do |type|
    define_method("#{type}_id_mark") do |id|
      "#{type[0].upcase}##{id}"
    end
  end

  def multi_check_box(name, value, checked)
    "<input type='checkbox' value='#{value}' name='#{name}[]' #{checked ? "checked" : ""}>".html_safe
  end

  def to_currency(amount, convert = true)
    !convert and return amount
    ApplicationRecord.to_currency(amount)
  end

  def range_desc(range)
    [range.first, range.last].join(",")
  end

  def puts_hash_amounts(h)
    html = ""
    h&.each do |__key, __val|
      if __val.to_f > 0
        html += [__key.titleize, to_currency(__val)].join(": ")
        html += "<br>"
      end
    end
    html.html_safe
  end
end
