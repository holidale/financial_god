module BootstrapHelper
  def nav_menu(sy, li = sy.to_s)
    link = li.to_s
    url = link.start_with?("http") || link.start_with?("/") ? link : "/#{sy}"
    link = link_to(t(sy), url)
    content_tag(:li, link)
  end

  def link_to_back(opts = {}, html_opts = {})
    link_to_btn title: t(:back), btn_style: 'primary', url: opts[:url]
  end

  def link_to_btn(opts = {}, html_opts = {})
    title = opts[:title] || 'title'
    size = opts[:size] || 'btn-big'
    btn_style = opts[:btn_style] || 'primary'
    style = {class: "btn btn-#{btn_style} ml10 " + size}.merge(html_opts)
    return link_to title, opts[:url], style if opts[:url]
    link_to_function(title, 'history.go(-1)', style)
  end

  def link_to_function(title, js, html_opts = {})
    link_to title, "#", html_opts.merge(onclick: js)
  end
end
