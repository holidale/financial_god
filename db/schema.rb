# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_05_22_012623) do
  create_table "account_records", charset: "utf8", force: :cascade do |t|
    t.string "record_source_type"
    t.string "record_source_id"
    t.string "finance_type"
    t.decimal "initial_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "received_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "overflow_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "rate_for_initial_amount", precision: 10, scale: 2, default: "0.0"
    t.integer "booking_id"
    t.string "responser_name"
    t.string "responser_type"
    t.integer "responser_id"
    t.string "finance_account"
    t.date "finance_create_at"
    t.date "finance_ensure_at"
    t.date "finance_end_at"
    t.integer "house_id"
    t.integer "data_source"
    t.integer "company"
    t.text "extra"
    t.string "sales_email"
    t.integer "sales_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uniq_key"
    t.string "status"
    t.index ["booking_id"], name: "index_account_records_on_booking_id"
    t.index ["finance_create_at"], name: "index_account_records_on_finance_create_at"
    t.index ["finance_end_at"], name: "index_account_records_on_finance_end_at"
    t.index ["finance_ensure_at"], name: "index_account_records_on_finance_ensure_at"
    t.index ["finance_type"], name: "index_account_records_on_finance_type"
    t.index ["house_id"], name: "index_account_records_on_house_id"
    t.index ["record_source_id"], name: "index_account_records_on_record_source_id"
    t.index ["record_source_type"], name: "index_account_records_on_record_source_type"
    t.index ["responser_id"], name: "index_account_records_on_responser_id"
    t.index ["responser_name"], name: "index_account_records_on_responser_name"
    t.index ["responser_type"], name: "index_account_records_on_responser_type"
    t.index ["sales_email"], name: "index_account_records_on_sales_email"
    t.index ["sales_id"], name: "index_account_records_on_sales_id"
  end

  create_table "approvals", charset: "utf8", force: :cascade do |t|
    t.integer "pms_user_id"
    t.string "key"
    t.string "approval_code"
    t.string "approval_name"
    t.string "department_id"
    t.string "end_time"
    t.string "open_id"
    t.boolean "reverted"
    t.string "serial_number"
    t.string "start_time"
    t.string "status"
    t.string "user_id"
    t.string "uuid"
    t.text "extra"
    t.text "form"
    t.text "timeline"
    t.text "task_list"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cost_item_id", default: 0
    t.integer "cost_id"
    t.index ["cost_id"], name: "index_approvals_on_cost_id"
    t.index ["cost_item_id"], name: "index_approvals_on_cost_item_id"
  end

  create_table "daily_incomes", charset: "utf8", force: :cascade do |t|
    t.string "day"
    t.string "account"
    t.integer "house_id", default: 0
    t.decimal "total", precision: 18, scale: 8, default: "0.0"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account"], name: "index_daily_incomes_on_account"
    t.index ["day"], name: "index_daily_incomes_on_day"
    t.index ["house_id"], name: "index_daily_incomes_on_house_id"
  end

  create_table "deposits", charset: "utf8", force: :cascade do |t|
    t.integer "account_record_id"
    t.date "finance_create_at"
    t.date "finance_ensure_at"
    t.date "finance_end_at"
    t.decimal "initial_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "received_amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "refunded_amount", precision: 10, scale: 2, default: "0.0"
    t.integer "house_id"
    t.integer "booking_id"
    t.integer "data_source"
    t.integer "company"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_record_id"], name: "index_deposits_on_account_record_id"
    t.index ["booking_id"], name: "index_deposits_on_booking_id"
    t.index ["finance_create_at"], name: "index_deposits_on_finance_create_at"
    t.index ["finance_end_at"], name: "index_deposits_on_finance_end_at"
    t.index ["finance_ensure_at"], name: "index_deposits_on_finance_ensure_at"
    t.index ["house_id"], name: "index_deposits_on_house_id"
  end

  create_table "diff_notes", charset: "utf8", force: :cascade do |t|
    t.integer "user_id"
    t.integer "platform", default: 0
    t.integer "local_id"
    t.string "local_type"
    t.string "remote_id"
    t.string "remote_type"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["local_type", "local_id"], name: "index_diff_notes_on_local_type_and_local_id"
    t.index ["remote_type", "remote_id"], name: "index_diff_notes_on_remote_type_and_remote_id"
    t.index ["user_id"], name: "index_diff_notes_on_user_id"
  end

  create_table "feishu_messages", charset: "utf8", force: :cascade do |t|
    t.string "message_id"
    t.text "content"
    t.string "sender_type"
    t.string "receivable_type"
    t.integer "receivable_id"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_feishu_messages_on_message_id"
    t.index ["receivable_id", "receivable_type"], name: "index_feishu_messages_on_receivable_id_and_receivable_type"
    t.index ["receivable_id"], name: "index_feishu_messages_on_receivable_id"
    t.index ["receivable_type"], name: "index_feishu_messages_on_receivable_type"
    t.index ["sender_type"], name: "index_feishu_messages_on_sender_type"
  end

  create_table "feishu_objects", charset: "utf8", force: :cascade do |t|
    t.string "app_id"
    t.string "key"
    t.string "name"
    t.integer "remote_type"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "outer_msg_keys"
    t.integer "user_id"
    t.string "alias_names"
    t.index ["key"], name: "index_feishu_objects_on_key"
    t.index ["remote_type"], name: "index_feishu_objects_on_remote_type"
  end

  create_table "feishu_objects_robot_tasks", charset: "utf8", force: :cascade do |t|
    t.integer "robot_task_id"
    t.integer "feishu_object_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feishu_object_id"], name: "index_feishu_objects_robot_tasks_on_feishu_object_id"
    t.index ["robot_task_id"], name: "index_feishu_objects_robot_tasks_on_robot_task_id"
  end

  create_table "feishu_task_comments", charset: "utf8", force: :cascade do |t|
    t.string "lark_task_id"
    t.string "lark_comment_id"
    t.string "commentable_type"
    t.integer "commentable_id"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["commentable_id"], name: "index_feishu_task_comments_on_commentable_id"
    t.index ["commentable_type", "commentable_id"], name: "index_feishu_task_comments_on_commentable"
    t.index ["commentable_type"], name: "index_feishu_task_comments_on_commentable_type"
    t.index ["lark_comment_id"], name: "index_feishu_task_comments_on_lark_comment_id"
    t.index ["lark_task_id", "lark_comment_id"], name: "index_feishu_task_comments_on_lark_task_id_and_lark_comment_id"
    t.index ["lark_task_id"], name: "index_feishu_task_comments_on_lark_task_id"
  end

  create_table "feishu_task_feishu_object_relationships", charset: "utf8", force: :cascade do |t|
    t.integer "feishu_task_id"
    t.integer "feishu_object_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feishu_object_id"], name: "index_relationship_on_object"
    t.index ["feishu_task_id", "feishu_object_id"], name: "index_relationship_on_task_id_object_id"
    t.index ["feishu_task_id"], name: "index_relationship_on_task_id"
  end

  create_table "feishu_tasks", charset: "utf8", force: :cascade do |t|
    t.string "remote_id"
    t.integer "creator_id"
    t.integer "workorder_id"
    t.string "creator_feishu_user"
    t.string "title"
    t.text "description"
    t.integer "due"
    t.integer "status"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_feishu_user"], name: "index_feishu_tasks_on_creator_feishu_user"
    t.index ["creator_id"], name: "index_feishu_tasks_on_creator_id"
    t.index ["due"], name: "index_feishu_tasks_on_due"
    t.index ["remote_id"], name: "index_feishu_tasks_on_remote_id"
    t.index ["workorder_id"], name: "index_feishu_tasks_on_workorder_id"
  end

  create_table "flat_costs", charset: "utf8", force: :cascade do |t|
    t.integer "house_id"
    t.integer "cost_id"
    t.integer "service_schedule_id"
    t.integer "booking_id"
    t.integer "financial_account_id"
    t.date "bill_date"
    t.date "service_date"
    t.string "customer"
    t.decimal "amount", precision: 10, scale: 2, default: "0.0"
    t.decimal "markup", precision: 10, scale: 2, default: "0.0"
    t.decimal "invoice_amount", precision: 10, scale: 2, default: "0.0"
    t.integer "source_id"
    t.string "source_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.integer "cleaner_id"
    t.integer "service_id"
    t.index ["bill_date"], name: "index_flat_costs_on_bill_date"
    t.index ["booking_id"], name: "index_flat_costs_on_booking_id"
    t.index ["cost_id"], name: "index_flat_costs_on_cost_id"
    t.index ["financial_account_id"], name: "index_flat_costs_on_financial_account_id"
    t.index ["house_id"], name: "index_flat_costs_on_house_id"
    t.index ["service_date"], name: "index_flat_costs_on_service_date"
    t.index ["service_schedule_id"], name: "index_flat_costs_on_service_schedule_id"
    t.index ["source_type", "source_id"], name: "index_flat_costs_on_source_type_and_source_id"
  end

  create_table "robot_task_records", charset: "utf8", force: :cascade do |t|
    t.integer "robot_task_id"
    t.text "extra"
    t.integer "result", default: 0
    t.datetime "finished_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["robot_task_id"], name: "index_robot_task_records_on_robot_task_id"
  end

  create_table "robot_tasks", charset: "utf8", force: :cascade do |t|
    t.string "name"
    t.string "worker"
    t.integer "interval"
    t.datetime "run_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "template"
  end

  create_table "robots", charset: "utf8", force: :cascade do |t|
    t.string "name"
    t.integer "platform", default: 0
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "outer_msg_keys"
  end

  create_table "robots_robots_tasks", charset: "utf8", force: :cascade do |t|
    t.integer "robot_id"
    t.integer "robot_task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_trackers", charset: "utf8", force: :cascade do |t|
    t.string "trackable_type"
    t.integer "trackable_id"
    t.string "action_name"
    t.string "status"
    t.integer "operator_id"
    t.string "uniq_source_key"
    t.text "extra"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["operator_id"], name: "index_time_trackers_on_operator_id"
    t.index ["trackable_id", "trackable_type"], name: "trackable_on_time_trackers"
    t.index ["uniq_source_key"], name: "index_time_trackers_on_uniq_source_key"
  end

end
