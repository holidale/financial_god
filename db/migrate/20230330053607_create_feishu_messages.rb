class CreateFeishuMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_messages do |t|
      t.string :message_id, index: true
      t.text :content
      t.string :sender_type
      t.string :receivable_type, index: true
      t.integer :receivable_id, index: true
      t.text :extra

      t.timestamps
    end

    add_index :feishu_messages, [:receivable_id, :receivable_type]
  end
end
