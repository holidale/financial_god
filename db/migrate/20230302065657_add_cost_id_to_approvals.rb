class AddCostIdToApprovals < ActiveRecord::Migration[7.0]
  def change
    add_column :approvals, :cost_id, :integer
    add_index :approvals, :cost_id
  end
end
