class AddServiceIdToFlatCosts < ActiveRecord::Migration[7.0]
  def change
    add_column :flat_costs, :service_id, :integer
  end
end
