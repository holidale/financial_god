class CreateDialpadRecords < ActiveRecord::Migration[7.0]
  def change
    create_table :dialpad_records do |t|
      t.datetime :record_time
      t.string :message_id
      t.string :name
      t.string :email
      t.string :target_type
      t.string :target_id
      t.string :direction
      t.string :sender_id
      t.string :to_phone
      t.string :from_phone
      t.text :encrypted_aes_text
      t.text :mms
      t.string :timezone

      t.timestamps
    end

    add_index :dialpad_records, :message_id
    add_index :dialpad_records, :record_time
  end
end
