class CreateFeishuTaskFeishuObjectRelationships < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_task_feishu_object_relationships do |t|
      t.integer :feishu_task_id
      t.integer :feishu_object_id

      t.index :feishu_task_id, name: :index_relationship_on_task_id
      t.index :feishu_object_id, name: :index_relationship_on_object
      t.index [:feishu_task_id, :feishu_object_id], name: :index_relationship_on_task_id_object_id

      t.timestamps
    end
  end
end
