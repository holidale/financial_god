class CreateRobotTasks < ActiveRecord::Migration[7.0]
  def change
    create_table :robot_tasks do |t|
      t.string :name
      t.string :worker
      t.integer :interval
      t.datetime :run_at

      t.timestamps
    end
  end
end
