class CreateFeishuTaskComments < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_task_comments do |t|
      t.string :lark_task_id, index: true
      t.string :lark_comment_id, index: true
      t.string :commentable_type, index: true
      t.integer :commentable_id, index: true
      t.text :extra

      t.timestamps
    end

    add_index :feishu_task_comments, [:lark_task_id, :lark_comment_id]
    add_index :feishu_task_comments, [:commentable_type, :commentable_id], name: :index_feishu_task_comments_on_commentable
  end
end
