class AddCleanerIdToFlatCosts < ActiveRecord::Migration[7.0]
  def change
    add_column :flat_costs, :cleaner_id, :integer
  end
end
