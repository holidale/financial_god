class CreateFeishuObjects < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_objects do |t|
      t.string :app_id
      t.string :key, index: true
      t.string :name
      t.string :remote_type, index: true
      t.text :extra

      t.timestamps
    end
  end
end
