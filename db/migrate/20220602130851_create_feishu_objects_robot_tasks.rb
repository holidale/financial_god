class CreateFeishuObjectsRobotTasks < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_objects_robot_tasks do |t|
      t.integer :robot_task_id, index: true
      t.integer :feishu_object_id, index: true

      t.timestamps
    end
  end
end
