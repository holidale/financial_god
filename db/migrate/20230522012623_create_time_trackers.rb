class CreateTimeTrackers < ActiveRecord::Migration[7.0]
  def change
    create_table :time_trackers do |t|
      t.string :trackable_type
      t.integer :trackable_id
      t.string :action_name
      t.string :status
      t.integer :operator_id, index: true
      t.string :uniq_source_key, index: true
      t.text :extra

      t.timestamps
    end

    add_index :time_trackers, [:trackable_id, :trackable_type], name: 'trackable_on_time_trackers'
  end
end
