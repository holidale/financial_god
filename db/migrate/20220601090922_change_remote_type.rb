class ChangeRemoteType < ActiveRecord::Migration[7.0]
  def change
    change_column :feishu_objects, :remote_type, :integer
  end
end
