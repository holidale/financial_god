class ChangeDecimalsOnFlatCosts < ActiveRecord::Migration[7.0]
  def change
    change_column :flat_costs, :amount, :decimal, default: 0, precision: 10, scale: 2 
    change_column :flat_costs, :markup, :decimal, default: 0, precision: 10, scale: 2
    change_column :flat_costs, :invoice_amount, :decimal, default: 0, precision: 10, scale: 2
  end
end
