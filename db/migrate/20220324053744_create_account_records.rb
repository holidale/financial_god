class CreateAccountRecords < ActiveRecord::Migration[7.0]
  def change
    create_table :account_records do |t|
      t.string :record_source_type, index: true   # like QbObject,Payment in PMS
      t.string :record_source_id, index: true    
      t.string :uniq_key, index: true    
      t.string :finance_type, index: true   # like Invoice, Cost for finance concept type
      t.decimal :initial_amount, default: 0, precision: 10, scale: 2
      t.decimal :received_amount, default: 0, precision: 10, scale: 2
      t.decimal :overflow_amount, default: 0, precision: 10, scale: 2
      t.decimal :rate_for_initial_amount, default: 0, precision: 10, scale: 2
      t.integer :booking_id, index: true
      t.string :responser_name, index: true
      t.string :responser_type, index: true
      t.string :finance_account
      t.integer :responser_id, index:true
      t.date :finance_create_at, index: true
      t.date :finance_ensure_at, index: true
      t.date :finance_end_at, index: true
      t.integer :house_id, index: true
      t.integer :data_source               # like PMS,QB
      t.integer :company                   # Holidale, Greenland
      t.text :extra
      t.string :sales_email, index: true
      t.integer :sales_id, index: true

      t.timestamps
    end
  end
end
