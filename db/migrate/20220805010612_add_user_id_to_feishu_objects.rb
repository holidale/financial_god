class AddUserIdToFeishuObjects < ActiveRecord::Migration[7.0]
  def change
    add_column :feishu_objects, :user_id, :integer
  end
end
