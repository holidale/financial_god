class AddAliasNamesToFeishuObjects < ActiveRecord::Migration[7.0]
  def change
    add_column :feishu_objects, :alias_names, :string
  end
end
