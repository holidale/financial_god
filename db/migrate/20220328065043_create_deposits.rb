class CreateDeposits < ActiveRecord::Migration[7.0]
  def change
    create_table :deposits do |t|
      t.integer :account_record_id, index: true
      t.date :finance_create_at, index: true
      t.date :finance_ensure_at, index: true
      t.date :finance_end_at, index: true
      t.decimal :initial_amount, default: 0, precision: 10, scale: 2
      t.decimal :received_amount, default: 0, precision: 10, scale: 2
      t.decimal :refunded_amount, default: 0, precision: 10, scale: 2
      t.integer :house_id, index: true
      t.integer :booking_id, index: true
      t.integer :data_source               # like PMS,QB
      t.integer :company                   # Holidale, Greenland
      t.text :extra

      t.timestamps
    end
  end
end
