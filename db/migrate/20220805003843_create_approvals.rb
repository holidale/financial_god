class CreateApprovals < ActiveRecord::Migration[7.0]
  def change
    create_table :approvals do |t|
      t.integer :pms_user_id
      t.string :key
      t.string :approval_code
      t.string :approval_name
      t.string :department_id
      t.string :end_time
      t.string :open_id
      t.boolean :reverted
      t.string :serial_number
      t.string :start_time
      t.string :status
      t.string :user_id
      t.string :uuid
      t.text :extra
      t.text :form
      t.text :timeline
      t.text :task_list

      t.timestamps
    end
  end
end
