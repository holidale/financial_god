class CreateDiffNotes < ActiveRecord::Migration[7.0]
  def change
    create_table :diff_notes do |t|
      t.integer :user_id, index: true
      t.integer :platform, default: 0
      t.integer :local_id
      t.string :local_type
      t.string :remote_id
      t.string :remote_type
      t.text :note

      t.timestamps
    end

    add_index :diff_notes, [:local_type, :local_id]
    add_index :diff_notes, [:remote_type, :remote_id]
  end
end