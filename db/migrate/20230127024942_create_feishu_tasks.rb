class CreateFeishuTasks < ActiveRecord::Migration[7.0]
  def change
    create_table :feishu_tasks do |t|
      t.string :remote_id, index: true
      t.integer :creator_id, index: true
      t.integer :workorder_id, index: true
      t.string :creator_feishu_user, index: true
      t.string :title
      t.text :description
      t.integer :due, index: true
      t.integer :status
      t.text :extra

      t.timestamps
    end
  end
end
