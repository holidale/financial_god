class AddOuterKeysToNotifyObjects < ActiveRecord::Migration[7.0]
  def change
    add_column :robots, :outer_msg_keys, :text
    add_column :feishu_objects, :outer_msg_keys, :text
  end
end
