class AddDescriptionToFlatCosts < ActiveRecord::Migration[7.0]
  def change
    add_column :flat_costs, :description, :text
  end
end
