class AddStatusToAccountRecords < ActiveRecord::Migration[7.0]
  def change
    add_column :account_records, :status, :string
  end
end
