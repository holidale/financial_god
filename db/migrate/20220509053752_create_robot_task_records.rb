class CreateRobotTaskRecords < ActiveRecord::Migration[7.0]
  def change
    create_table :robot_task_records do |t|
      t.integer :robot_task_id, index: true
      t.text :extra
      t.integer :result, default: 0
      t.datetime :finished_at

      t.timestamps
    end
  end
end
