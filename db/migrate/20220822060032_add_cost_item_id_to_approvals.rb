class AddCostItemIdToApprovals < ActiveRecord::Migration[7.0]
  def change
    add_column :approvals, :cost_item_id, :integer, default: 0
    add_index :approvals, :cost_item_id
  end
end
