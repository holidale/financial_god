class AddTemplateToTasks < ActiveRecord::Migration[7.0]
  def change
    add_column :robot_tasks, :template, :text
  end
end
