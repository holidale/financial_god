class CreateFlatCosts < ActiveRecord::Migration[7.0]
  def change
    create_table :flat_costs do |t|
      t.integer :house_id, index: true
      t.integer :cost_id, index: true
      t.integer :service_schedule_id, index: true
      t.integer :booking_id, index: true
      t.integer :financial_account_id, index: true
      t.date :bill_date, index: true
      t.date :service_date, index: true
      t.string :customer
      t.decimal :amount
      t.decimal :markup
      t.decimal :invoice_amount
      t.integer :source_id
      t.string :source_type

      t.timestamps
    end

    add_index :flat_costs, [:source_type, :source_id]
  end
end
