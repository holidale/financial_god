class CreateDailyIncomes < ActiveRecord::Migration[7.0]
  def change
    create_table :daily_incomes do |t|
      t.string :day, index: true
      t.string :account, index: true
      t.integer :house_id, index: true, default: 0
      t.decimal :total, default: 0, precision: 18, scale: 8
      t.text :extra

      t.timestamps
    end
  end
end
