class CreateRobotsRobotsTasks < ActiveRecord::Migration[7.0]
  def change
    create_table :robots_robots_tasks do |t|
      t.integer :robot_id
      t.integer :robot_task_id

      t.timestamps
    end
  end
end
