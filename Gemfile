source "https://rubygems.org"
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby "3.0.0"

# Bundle edge Rails instead: gem "rails", github: "rails/rails", branch: "main"
gem "rails", "~> 7.0.2", ">= 7.0.2.3"

# The original asset pipeline for Rails [https://github.com/rails/sprockets-rails]
gem "sprockets-rails"

# Use sqlite3 as the database for Active Record
# gem "sqlite3", "~> 1.4"

# Use the Puma web server [https://github.com/puma/puma]
gem "puma", "~> 5.0"
gem 'mysql2'
gem 'slim-rails'

# Use JavaScript with ESM import maps [https://github.com/rails/importmap-rails]
gem "importmap-rails"

# Hotwire's SPA-like page accelerator [https://turbo.hotwired.dev]
gem "turbo-rails"

# Hotwire's modest JavaScript framework [https://stimulus.hotwired.dev]
gem "stimulus-rails"

# Build JSON APIs with ease [https://github.com/rails/jbuilder]
gem "jbuilder"

# Use Redis adapter to run Action Cable in production
gem "redis", "~> 4.0"

# Use Kredis to get higher-level data types in Redis [https://github.com/rails/kredis]
# gem "kredis"

# Use Active Model has_secure_password [https://guides.rubyonrails.org/active_model_basics.html#securepassword]
# gem "bcrypt", "~> 3.1.7"

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: %i[ mingw mswin x64_mingw jruby ]

# Reduces boot times through caching; required in config/boot.rb
gem "bootsnap", require: false

# Use Sass to process CSS
# gem "sassc-rails"

# Use Active Storage variants [https://guides.rubyonrails.org/active_storage_overview.html#transforming-images]
# gem "image_processing", "~> 1.2"

# Open AI Ruby API Wrapper
gem "ruby-openai"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem "debug", platforms: %i[ mri mingw x64_mingw ]
end

group :development do
  # Use console on exceptions pages [https://github.com/rails/web-console]
  gem "web-console"

  # Add speed badges [https://github.com/MiniProfiler/rack-mini-profiler]
  # gem "rack-mini-profiler"

  # Speed up commands on slow machines / big apps [https://github.com/rails/spring]
  # gem "spring"
  # group :development, :development_with_cache do
  gem 'capistrano', '3.10'
  gem 'capistrano3-puma', '5.2.0', require: false
  gem 'capistrano-rails', '~>1.6', require: false
  gem 'capistrano-bundler', '~>1.6',  require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-multiconfig', '~>3.1.0', require: false
  gem 'capistrano-faster-assets', '1.1.0'
  gem 'execjs'       # for OSX
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem "capybara"
  gem "selenium-webdriver"
  gem "webdrivers"
end

gem 'pry'
gem 'settingslogic'
gem 'qbo_api'
gem 'intuit-oauth', '1.0.2'
gem 'sidekiq', '5.2.9'
gem "sidekiq-cron", '0.4.5'
gem 'capistrano-sidekiq', "~> 0.5.4"
gem 'rufus-scheduler', '~> 3.4.0'
gem 'sinatra', "~> 2.0", :require => nil
gem 'awesome_form_attributes'
gem 'kaminari'
gem 'stripe'
gem 'awesome_print'
gem 'imgkit'
gem 'wkhtmltoimage-binary'
gem 'rexml'
gem 'm2m_pms_shared_gem', git: 'git@bitbucket.org:holidale/m2m_pms_shared_gem.git'
gem 'net-ssh'
gem 'ed25519', '>= 1.2', '< 2.0'
gem 'bcrypt_pbkdf', '>= 1.0', '< 2.0'

