FROM ruby:3.0.0
RUN apt-get update -qq && apt-get install -y nodejs \
    curl zlib1g-dev build-essential \
    libssl-dev libreadline-dev

RUN apt-get install -y libyaml-dev \
    libxml2-dev \
    libxslt1-dev libcurl4-openssl-dev \
    libffi-dev
RUN git config --global url."https://github.com".insteadOf git://github.com

WORKDIR /usr/src/finance_god
COPY Gemfile /usr/src/finance_god/Gemfile
COPY Gemfile.lock /usr/src/finance_god/Gemfile.lock
RUN bundle install

EXPOSE 3001
CMD ["rails", "server", "-b", "0.0.0.0"]
