Sidekiq::Extensions.enable_delay!

Sidekiq.configure_server do |config|
  config.redis = CONFIG["finance_redis"] || {}
  config.error_handlers << proc {|ex,ctx_hash|
    LarkMsg.enqueue(:financial_sidekiq_error, "#{ex.message} -> #{ctx_hash[:jobstr]}")
    proj_file = ex.backtrace.select{|a| a.to_s =~ /releases/}
    LarkMsg.enqueue(:financial_sidekiq_error, proj_file.unshift(ex.message).join("\n"))
  }
end

Sidekiq.configure_client do |config|
  config.redis = CONFIG["finance_redis"] || {}
end
