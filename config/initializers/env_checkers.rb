def env_production?
  Rails.env.production? || Rails.env.production_web5? || Rails.env.production_cn?
end

def env_us_production?
  Rails.env.production? || Rails.env.production_web5?
end

def env_cn_production?
  Rails.env.production_cn?
end

def env_live_testing?
  Rails.env.live_testing? || Rails.env.live_testing_2?
end

def env_dev?
  Rails.env.dev1? || Rails.env.dev2? || Rails.env.dev3? || Rails.env.dev4?
end

def env_dev_and_live_testing?
  env_live_testing? || env_dev?
end

def env_local?
  Rails.env.development? || Rails.env.test? || Rails.env.test_with_mail?
end

def env_local_test?
  Rails.env.test? || Rails.env.test_with_mail?
end
