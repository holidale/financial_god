cron_file = "config/sidekiq_cron.yml"
Sidekiq::Cron::Job.load_from_hash YAML.load_file(cron_file) if File.exists?(cron_file)

