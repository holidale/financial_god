class Object
  def to_ejson
    to_json.gsub("\\u003c", "<").gsub("\\u003e", ">")
  end          
end
