#encoding: utf-8
AwesomeFormAttributes.configure do |config|
  # config.default_tag = "text_field"
  config.text_area_words += %w(Template Url url Extra extra Keys)
  config.select_words += %w(parent Worker)
  # config.boolean_words += []
  # config.file_field_words += []
end
