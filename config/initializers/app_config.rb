class AppConfig < Settingslogic
  source "#{Rails.root}/config/config.yml"
  namespace Rails.env
  suppress_errors true
  load!
end
