QboApi.production = Rails.env.production? || Rails.env.production_web5?                    
QboApi.log = true
QboApi.logger = Rails.logger
QboApi.request_id = true
QboApi.minor_version = 47

class QboApi
  module ApiMethods
    # new method for QboApi, Official code does not implement
    def void(entity, id:)
      path = add_params_to_path(path: entity_path(entity), params: { operation: :void})
      payload = set_update(entity, id) 
      request(:post, entity: entity, path: path, payload: payload)
    end 
  end 
end
