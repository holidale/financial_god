DBName = "holidale_#{Rails.env}"
RENTAL_INCOME_ACCOUNT = "Rental Income"
PMS_HOST = "month2month.com"
PMS_ENDPOINT = Rails.env.production? ? "https://internal.month2month.com" : "http://localhost:3000"
FINANCE_ENDPOINT = Rails.env.production? ? "http://104.237.152.83" : "http://127.0.0.1:3001"
DEBUGGER_EMAIL = "zmsun@gmail.com"
FEISHU_API_ADMIN_PHONE = "+8615810762491"
CONFIG = FinanceGod::Application::CONFIG
PMS_DEPLOY_PATH = "/opt/webapp/holidale/current"
APPROVAL_V2_START = Rails.env.production? ? '2023-03-08'.to_date : Date.current - 7.days

QB_CLEARING_ACCOUNT_VALUE = Rails.env.production? ? "372" : "93"

class PublicConfigs < Settingslogic
  source "#{Rails.root}/config/public_config.yml"
  namespace Rails.env
  suppress_errors true
  load!
end
