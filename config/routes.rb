Rails.application.routes.draw do
  root "home#index"

  resources :bookings do
    member do
      get :owner_revenue
    end
  end
  resources :invoices do
    collection do
      get :unpaid
    end
  end

  namespace :admin do
    resources :landing_users
    resources :feedbacks
    resources :robots do
      member do 
        get :config_tasks
      end
    end
    resources :robot_tasks
    resources :approvals
    resources :feishu_objects do
      member do 
        get :config_tasks
      end
    end
  end

  %w(
    forbidden
    compare_invoices 
    compare_journal_entries
    feishu_auth
    feishu_auth_from_external
    jira_dashboard
  ).each do |a|
    get a => "home##{a}"
  end
  
  %w(
    daily_incomes_overview
    deposits_overview
    houses_overview
    bookings_overview
    journal_entries_overview
    invoices_overview
    ar_overdue_summary
  ).each do |a|
    get a => "statistics##{a}"
  end

  %w(
    extensions
  ).each do |a|
    get a => "auto_checks##{a}"
  end

  post "/salesforce/:klass" => "home#salesforce_hook"

  get "houses_stas/:id" => "statistics#house_detail"
  get "ar_overdue_summary/:obj_id" => "statistics#ar_overdue_detail"
  get "month_overview" => "daily_incomes#month_overview"
  get "auth/:session_key" => "home#auth"
  post "feishu_events" => "home#feishu_events"
  post "ai_events" => "home#ai_events"
  post "jira_events" => "home#jira_events"
  get "jira_events" => "home#jira_events"
  get "big_screen_data" => "home#big_screen_data"
  post "bitbucket_events" => "home#bitbucket_events"
  post "rocket_chats" => "home#rocket_chats"
  get "houses/:id/coordinate" => "houses#coordinate"
  post "gmail" => "home#gmail_hook"

  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  require 'admin_constraint'
  mount Sidekiq::Web => '/sidekiq_finance', :constraints => AdminConstraint.new
end
