require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FinanceGod
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0
    CONFIG = YAML.load_file("#{Rails.root.to_s}/config/config.yml")[Rails.env].with_indifferent_access

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    config.time_zone = ENV['RAILS_CICD'] == "docker" ? 'UTC' : 'Pacific Time (US & Canada)'
    config.autoload_paths << "#{config.root}/lib"
    config.eager_load_paths << "#{Rails.root}/lib"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
